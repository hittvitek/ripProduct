package com.itCompany.ripProduct.common;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

public class Common {

    public static Format DD_MM_YYYY = new SimpleDateFormat("dd.MM.yyyy");
    public static DecimalFormat NUMBER_FORMAT_SIGN_THIRD_ROUND = new DecimalFormat("#,###,###,##0.000");
    public static DecimalFormat NUMBER_FORMAT_SIGN_SECOND_ROUND = new DecimalFormat("#,###,###,##0.00");

}
