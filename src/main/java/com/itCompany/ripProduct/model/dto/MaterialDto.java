package com.itCompany.ripProduct.model.dto;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@JsonAutoDetect
public class MaterialDto {
    Long id;
    @JsonIgnore
    @NotBlank(message = "Article is mandatory")
    String article;
    @NotBlank(message = "Name is mandatory")
    String name;
    @JsonIgnore
    float price;
    @JsonIgnore
    float mass;
    @JsonIgnore
    Float length;
    @JsonIgnore
    Float priceUanKg;
    @JsonIgnore
    boolean removed;
    @JsonIgnore
    String comment;
    Float cost;
    @JsonIgnore
    float quantity;

}
