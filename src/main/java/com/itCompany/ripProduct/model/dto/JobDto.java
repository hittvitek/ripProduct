package com.itCompany.ripProduct.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@JsonAutoDetect
public class JobDto {
    Long id;
    @JsonIgnore
    @NotBlank(message = "Article is mandatory")
    String article;
    @NotBlank(message = "Name is mandatory")
    String name;
    @Min(value = 0)
    //@NotBlank(message = "Price is a posetive value")
    float price;
    @JsonIgnore
    boolean removed;
    @JsonIgnore
    String comment;
}
