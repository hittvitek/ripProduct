package com.itCompany.ripProduct.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CurrencyDto {
    Long id;
    @NotBlank(message = "Article is mandatory")
    String article;
    @Min(value = 0)
    //@NotBlank(message = "Price is a posetive value")
    float price;
}
