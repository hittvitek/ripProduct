package com.itCompany.ripProduct.model.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@JsonAutoDetect
@Data
public class FoneDto {
    Long id;
    @JsonIgnore
    @NotBlank(message = "Article is mandatory")
    @Size(min = 1, max = 6, message = "Lenght text is a from 1 to 6 charaster.")
    String article;
    @NotBlank(message = "Name is mandatory")
    String name;
    float price;
    @JsonIgnore
    List<MaterialDto> materials;
    @JsonIgnore
    List<RuffDto> ruffs;
    @JsonIgnore
    JobDto jobBraid;
    @JsonIgnore
    CarcassDto carcass;
    @JsonIgnore
    boolean removed;
    @JsonIgnore
    String comment;
}
