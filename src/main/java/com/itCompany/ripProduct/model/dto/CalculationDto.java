package com.itCompany.ripProduct.model.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CalculationDto {
    Long id;
    @NotBlank(message = "Article is mandatory")
    @Size(min = 1, max = 6, message = "Lenght text is a from 1 to 6 charaster.")
    String article;
    @NotBlank(message = "Name is mandatory")
    String name;
    float price;
    FoneDto fone;
    List<MaterialDto> materials;
    List<FlowerDto> flowers;
    List<RuffDto> ruffs;
    JobDto jobProduct;
    boolean removed;
    String comment;
}
