package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "fones_materials")
public class FoneMaterial implements Serializable{

    private static final long serialVersionUID = -5526197395384022606L;

    @EmbeddedId
    FoneMaterialKey id;

    @ManyToOne()
    @MapsId("foneId")
    @JoinColumn(name = "fone_id")
    Fone fone;

    @ManyToOne()
    @MapsId("materialId")
    @JoinColumn(name = "materials_id")
    Material material;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    @Column(nullable = false)
    Float quantity;

    public FoneMaterial() {}

    public FoneMaterialKey getId() {
        return id;
    }

    public void setId(FoneMaterialKey id) {
        this.id = id;
    }

    public Fone getFone() {
        return fone;
    }

    public void setFone(Fone fone) {
        this.fone = fone;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "FoneMaterial [id=" + id + ", fone=" + fone + ", material=" + material + ", quantity=" + quantity + "]";
    }

}
