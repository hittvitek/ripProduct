package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FoneMaterialKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "fone_id")
    Long foneId;

    @Column(name = "materials_id")
    Long materialId;

    public FoneMaterialKey() {}

    public Long getFoneId() {
        return foneId;
    }

    public void setFoneId(Long foneId) {
        this.foneId = foneId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((foneId == null) ? 0 : foneId.hashCode());
        result = prime * result + ((materialId == null) ? 0 : materialId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FoneMaterialKey other = (FoneMaterialKey) obj;
        if (foneId == null) {
            if (other.foneId != null)
                return false;
        } else if (!foneId.equals(other.foneId))
            return false;
        if (materialId == null) {
            if (other.materialId != null)
                return false;
        } else if (!materialId.equals(other.materialId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FoneMaterialKey [foneId=" + foneId + ", materialId=" + materialId + "]";
    }

}
