package com.itCompany.ripProduct.model.po;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PROTECTED)
@Data
@MappedSuperclass
public class Entity {
//    @Temporal(TemporalType.DATE) //can use java.util.Date instead sql.Date
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
    @Size(min = 1, max = 50)
    String article;
    @Size(min = 1, max = 255)
    String name;
    Float price;

}

