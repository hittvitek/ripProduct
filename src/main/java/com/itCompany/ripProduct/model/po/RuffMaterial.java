package com.itCompany.ripProduct.model.po;

import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "ruffs_materials")
public class RuffMaterial {

    @EmbeddedId
    RuffMaterialKey id;

    @ManyToOne()
    @MapsId("ruffId")
    @JoinColumn(name = "ruff_id")
    Ruff ruff;

    @ManyToOne()
    @MapsId("materialId")
    @JoinColumn(name = "material_id")
    Material material;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    Float quantity;

    public RuffMaterial() {}

    public RuffMaterialKey getId() {
        return id;
    }

    public void setId(RuffMaterialKey id) {
        this.id = id;
    }

    public Ruff getRuff() {
        return ruff;
    }

    public void setRuff(Ruff ruff) {
        this.ruff = ruff;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((material == null) ? 0 : material.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((ruff == null) ? 0 : ruff.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RuffMaterial other = (RuffMaterial) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (material == null) {
            if (other.material != null)
                return false;
        } else if (!material.equals(other.material))
            return false;
        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;
        if (ruff == null) {
            if (other.ruff != null)
                return false;
        } else if (!ruff.equals(other.ruff))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "RuffMaterial [id=" + id + ", ruff=" + ruff + ", material=" + material + ", quantity=" + quantity + "]";
    }

}
