package com.itCompany.ripProduct.model.po;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "calculation")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@EqualsAndHashCode(callSuper = true)
public class Calculation extends Entity {

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "calculation", cascade = CascadeType.MERGE/*, orphanRemoval = true*/)
    List<CalculationRuff> calculationRuffs;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "calculation", cascade = { CascadeType.MERGE }/*, orphanRemoval = true*/)
    List<CalculationMaterial> calculationMaterials;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "calculation", cascade = { CascadeType.MERGE }/*, orphanRemoval = true*/)
    List<CalculationFlower> calculationFlowers;

    @ManyToOne()
    @JoinColumn(name = "fone_id", nullable = false)
    Fone fone;

    @ManyToOne()
    @JoinColumn(name = "jobproduct_id", nullable = false)
    JobProduct jobProduct;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
