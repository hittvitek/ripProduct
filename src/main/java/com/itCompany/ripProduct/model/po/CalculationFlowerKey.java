package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CalculationFlowerKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "calculations_id")
    Long calculationId;

    @Column(name = "flowers_id")
    Long flowerId;

    public CalculationFlowerKey() {}

    public Long getCalculationId() {
        return calculationId;
    }

    public void setCalculationId(Long calculationId) {
        this.calculationId = calculationId;
    }

    public Long getFlowerId() {
        return flowerId;
    }

    public void setFlowerId(Long flowerId) {
        this.flowerId = flowerId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((calculationId == null) ? 0 : calculationId.hashCode());
        result = prime * result + ((flowerId == null) ? 0 : flowerId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CalculationFlowerKey other = (CalculationFlowerKey) obj;
        if (calculationId == null) {
            if (other.calculationId != null)
                return false;
        } else if (!calculationId.equals(other.calculationId))
            return false;
        if (flowerId == null) {
            if (other.flowerId != null)
                return false;
        } else if (!flowerId.equals(other.flowerId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CalculationFlowerKey [calculationId=" + calculationId + ", flowerId=" + flowerId + "]";
    }

}
