package com.itCompany.ripProduct.model.po;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "carcass")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class Carcass extends Entity {

    @OneToMany(mappedBy = "carcass", cascade = {CascadeType.MERGE}, orphanRemoval = true)
    List<CarcassMaterial> carcassMaterials;

    @ManyToOne()
    @JoinColumn(name = "jobwelder_id", nullable=false)
    JobWelder jobWelder;

    @OneToMany(mappedBy = "carcass", fetch = FetchType.LAZY)
    Set<Fone> fone;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
