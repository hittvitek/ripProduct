package com.itCompany.ripProduct.model.po;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "jobflowers")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class JobFlower extends Entity {

    @OneToMany(mappedBy = "jobFlower")
    Set<Flower> flower;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
