package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FlowerMaterialKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "flower_id")
    Long flowerId;

    @Column(name = "material_id")
    Long materialId;

    public FlowerMaterialKey() {}

    public Long getFlowerId() {
        return flowerId;
    }

    public void setFlowerId(Long flowerId) {
        this.flowerId = flowerId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((flowerId == null) ? 0 : flowerId.hashCode());
        result = prime * result + ((materialId == null) ? 0 : materialId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FlowerMaterialKey other = (FlowerMaterialKey) obj;
        if (flowerId == null) {
            if (other.flowerId != null)
                return false;
        } else if (!flowerId.equals(other.flowerId))
            return false;
        if (materialId == null) {
            if (other.materialId != null)
                return false;
        } else if (!materialId.equals(other.materialId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FlowerMaterialKey [flowerId=" + flowerId + ", materialId=" + materialId + "]";
    }

}
