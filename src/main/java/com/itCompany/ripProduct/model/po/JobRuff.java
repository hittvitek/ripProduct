package com.itCompany.ripProduct.model.po;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "jobruffs")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class JobRuff extends Entity {

    @OneToOne(mappedBy = "jobRuff")
    Ruff ruff;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
