package com.itCompany.ripProduct.model.po;

import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "flowers_materials")
public class FlowerMaterial {

    @EmbeddedId
    FlowerMaterialKey id;

    @ManyToOne()
    @MapsId("flowerId")
    @JoinColumn(name = "flowers_id", referencedColumnName = "id")
    Flower flower;

    @ManyToOne()
    @MapsId("materialId")
    @JoinColumn(name = "materials_id", referencedColumnName = "id")
    Material material;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    Float quantity;

    public FlowerMaterial() {
    }

    public FlowerMaterialKey getId() {
        return id;
    }

    public void setId(FlowerMaterialKey id) {
        this.id = id;
    }

    public Flower getFlower() {
        return flower;
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((flower == null) ? 0 : flower.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((material == null) ? 0 : material.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FlowerMaterial other = (FlowerMaterial) obj;
        if (flower == null) {
            if (other.flower != null)
                return false;
        } else if (!flower.equals(other.flower))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (material == null) {
            if (other.material != null)
                return false;
        } else if (!material.equals(other.material))
            return false;
        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FlowerMaterial [id=" + id + ", flower=" + flower + ", material=" + material + ", quantity=" + quantity
                + "]";
    }

}
