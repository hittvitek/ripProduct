package com.itCompany.ripProduct.model.po;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "fone")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class Fone extends Entity {
    @OneToMany(mappedBy = "fone", cascade = CascadeType.MERGE, orphanRemoval = true)
    List<FoneRuff> foneRuffs;

    @OneToMany(mappedBy = "fone", cascade = {CascadeType.MERGE}, orphanRemoval = true)
    List<FoneMaterial> foneMaterials;

    @OneToMany(mappedBy = "fone")
    Set<Calculation> calculation;

    @ManyToOne()
    @JoinColumn(name = "carcass_id", nullable=false)
    Carcass carcass;

    @ManyToOne()
    @JoinColumn(name = "jobbraid_id", nullable=false)
    JobBraid jobBraid;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
