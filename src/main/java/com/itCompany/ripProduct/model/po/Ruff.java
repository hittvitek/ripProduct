package com.itCompany.ripProduct.model.po;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "ruffs")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class Ruff extends Entity {
    @OneToMany(mappedBy = "ruff", cascade = CascadeType.MERGE, orphanRemoval = true)
    List<RuffMaterial> ruffMaterials;

    @OneToOne
    @JoinColumn(name = "jobruff_id", referencedColumnName = "id", nullable = false)
    JobRuff jobRuff;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
