package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CarcassMaterialKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "carcas_id")
    Long carcassId;

    @Column(name = "material_id")
    Long materialId;

    public CarcassMaterialKey() {}

    public Long getCarcassId() {
        return carcassId;
    }

    public void setCarcassId(Long carcassId) {
        this.carcassId = carcassId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((carcassId == null) ? 0 : carcassId.hashCode());
        result = prime * result + ((materialId == null) ? 0 : materialId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CarcassMaterialKey other = (CarcassMaterialKey) obj;
        if (carcassId == null) {
            if (other.carcassId != null)
                return false;
        } else if (!carcassId.equals(other.carcassId))
            return false;
        if (materialId == null) {
            if (other.materialId != null)
                return false;
        } else if (!materialId.equals(other.materialId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CarcassMaterialKey [carcassId=" + carcassId + ", materialId=" + materialId + "]";
    }

}
