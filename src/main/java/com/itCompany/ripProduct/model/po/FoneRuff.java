package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "fones_ruffs")
public class FoneRuff implements Serializable{

    private static final long serialVersionUID = -5526197395384022606L;

    @EmbeddedId
    FoneRuffKey id;

    @ManyToOne()
    @MapsId("foneId")
    @JoinColumn(name = "fone_id")
    Fone fone;

    @ManyToOne()
    @MapsId("ruffId")
    @JoinColumn(name = "ruff_id")
    Ruff ruff;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    @Column(nullable = false)
    Float quantity;

    public FoneRuff() {}

    public FoneRuffKey getId() {
        return id;
    }

    public void setId(FoneRuffKey id) {
        this.id = id;
    }

    public Fone getFone() {
        return fone;
    }

    public void setFone(Fone fone) {
        this.fone = fone;
    }

    public Ruff getRuff() {
        return ruff;
    }

    public void setRuff(Ruff ruff) {
        this.ruff = ruff;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fone == null) ? 0 : fone.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((ruff == null) ? 0 : ruff.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FoneRuff other = (FoneRuff) obj;
        if (fone == null) {
            if (other.fone != null)
                return false;
        } else if (!fone.equals(other.fone))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;
        if (ruff == null) {
            if (other.ruff != null)
                return false;
        } else if (!ruff.equals(other.ruff))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FoneRuff [id=" + id + ", fone=" + fone + ", ruff=" + ruff + ", quantity=" + quantity + "]";
    }

}
