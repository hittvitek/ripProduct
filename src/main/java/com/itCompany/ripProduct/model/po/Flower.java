package com.itCompany.ripProduct.model.po;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "flowers")
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@Data
public class Flower extends Entity {
    @OneToMany(mappedBy = "flower", cascade = {CascadeType.MERGE}, orphanRemoval = true)
    List<FlowerMaterial> flowerMaterials;

    @ManyToOne()
    @JoinColumn(name = "jobflowers_id", nullable=false)
    JobFlower jobFlower;

    @Column(name = "removed")
    boolean removed;

    @Column(name = "comment")
    String comment;

}
