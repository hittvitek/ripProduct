package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CalculationMaterialKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "calculations_id")
    Long calculationId;

    @Column(name = "materials_id")
    Long materialId;

    public CalculationMaterialKey() {}

    public Long getCalculationId() {
        return calculationId;
    }

    public void setCalculationId(Long calculationId) {
        this.calculationId = calculationId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((calculationId == null) ? 0 : calculationId.hashCode());
        result = prime * result + ((materialId == null) ? 0 : materialId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CalculationMaterialKey other = (CalculationMaterialKey) obj;
        if (calculationId == null) {
            if (other.calculationId != null)
                return false;
        } else if (!calculationId.equals(other.calculationId))
            return false;
        if (materialId == null) {
            if (other.materialId != null)
                return false;
        } else if (!materialId.equals(other.materialId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CalculationMaterialKey [calculationId=" + calculationId + ", materialId=" + materialId + "]";
    }

}
