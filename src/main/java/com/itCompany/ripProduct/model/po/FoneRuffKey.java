package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FoneRuffKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "fone_id")
    Long foneId;

    @Column(name = "ruff_id")
    Long ruffId;

    public FoneRuffKey() {}

    public Long getFoneId() {
        return foneId;
    }

    public void setFoneId(Long foneId) {
        this.foneId = foneId;
    }

    public Long getRuffId() {
        return ruffId;
    }

    public void setRuffId(Long ruffId) {
        this.ruffId = ruffId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((foneId == null) ? 0 : foneId.hashCode());
        result = prime * result + ((ruffId == null) ? 0 : ruffId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FoneRuffKey other = (FoneRuffKey) obj;
        if (foneId == null) {
            if (other.foneId != null)
                return false;
        } else if (!foneId.equals(other.foneId))
            return false;
        if (ruffId == null) {
            if (other.ruffId != null)
                return false;
        } else if (!ruffId.equals(other.ruffId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FoneRuffKey [foneId=" + foneId + ", ruffId=" + ruffId + "]";
    }


}
