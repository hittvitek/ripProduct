package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "calculations_flowers")
public class CalculationFlower implements Serializable{

    private static final long serialVersionUID = -5526197395384022606L;

    @EmbeddedId
    CalculationFlowerKey id;

    @ManyToOne()
    @MapsId("calculationId")
    @JoinColumn(name = "calculations_id")
    Calculation calculation;

    @ManyToOne()
    @MapsId("flowerId")
    @JoinColumn(name = "flowers_id")
    Flower flower;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    @Column(nullable = false)
    Float quantity;

    public CalculationFlower() {}

    public CalculationFlowerKey getId() {
        return id;
    }

    public void setId(CalculationFlowerKey id) {
        this.id = id;
    }

    public Calculation getCalculation() {
        return calculation;
    }

    public void setCalculation(Calculation calculation) {
        this.calculation = calculation;
    }

    public Flower getFlower() {
        return flower;
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CalculationFlower [id=" + id + ", calculation=" + calculation + ", flower=" + flower + ", quantity="
                + quantity + "]";
    }

}
