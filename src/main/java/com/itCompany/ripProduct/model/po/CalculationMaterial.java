package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@javax.persistence.Entity
@Table(name = "calculations_materials")
public class CalculationMaterial implements Serializable{

    private static final long serialVersionUID = -5526197395384022606L;

    @EmbeddedId
    CalculationMaterialKey id;

    @ManyToOne()
    @MapsId("calculationId")
    @JoinColumn(name = "calculations_id")
    Calculation calculation;

    @ManyToOne()
    @MapsId("materialId")
    @JoinColumn(name = "materials_id")
    Material material;

    @Min(value = 0, message = "Must be greater than or equal to 0")
    @Column(nullable = false)
    Float quantity;

    public CalculationMaterial() {}

    public CalculationMaterialKey getId() {
        return id;
    }

    public void setId(CalculationMaterialKey id) {
        this.id = id;
    }

    public Calculation getCalculation() {
        return calculation;
    }

    public void setCalculation(Calculation calculation) {
        this.calculation = calculation;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CalculationMaterial [id=" + id + ", calculation=" + calculation + ", material=" + material
                + ", quantity=" + quantity + "]";
    }

}
