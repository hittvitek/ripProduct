package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CalculationRuffKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "calculations_id")
    Long calculationId;

    @Column(name = "ruffs_id")
    Long ruffId;

    public CalculationRuffKey() {}

    public Long getCalculationId() {
        return calculationId;
    }

    public void setCalculationId(Long calculationId) {
        this.calculationId = calculationId;
    }

    public Long getRuffId() {
        return ruffId;
    }

    public void setRuffId(Long ruffId) {
        this.ruffId = ruffId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((calculationId == null) ? 0 : calculationId.hashCode());
        result = prime * result + ((ruffId == null) ? 0 : ruffId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CalculationRuffKey other = (CalculationRuffKey) obj;
        if (calculationId == null) {
            if (other.calculationId != null)
                return false;
        } else if (!calculationId.equals(other.calculationId))
            return false;
        if (ruffId == null) {
            if (other.ruffId != null)
                return false;
        } else if (!ruffId.equals(other.ruffId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "CalculationRuffKey [calculationId=" + calculationId + ", ruffId=" + ruffId + "]";
    }

}
