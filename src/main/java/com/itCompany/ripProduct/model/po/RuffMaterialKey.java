package com.itCompany.ripProduct.model.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RuffMaterialKey implements Serializable {

    private static final long serialVersionUID = -8171282270640558931L;

    @Column(name = "ruff_id")
    Long ruffId;

    @Column(name = "material_id")
    Long materialId;

    public RuffMaterialKey() {}

    public Long getRuffId() {
        return ruffId;
    }

    public void setRuffId(Long ruffId) {
        this.ruffId = ruffId;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((materialId == null) ? 0 : materialId.hashCode());
        result = prime * result + ((ruffId == null) ? 0 : ruffId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RuffMaterialKey other = (RuffMaterialKey) obj;
        if (materialId == null) {
            if (other.materialId != null)
                return false;
        } else if (!materialId.equals(other.materialId))
            return false;
        if (ruffId == null) {
            if (other.ruffId != null)
                return false;
        } else if (!ruffId.equals(other.ruffId))
            return false;
        return true;
    }

}
