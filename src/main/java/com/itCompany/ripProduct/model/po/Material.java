package com.itCompany.ripProduct.model.po;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@javax.persistence.Entity
@Table(name = "materials")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Material extends Entity {
    float mass;
    Float length;
    @Column(name = "priceuankg")
    Float priceUanKg;
    @Column(name = "removed")
    boolean removed;
    @Column(name = "comment")
    String comment;
    Float cost;

}
