package com.itCompany.ripProduct.model.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.CarcassDto;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.Carcass;
import com.itCompany.ripProduct.model.po.CarcassMaterial;
import com.itCompany.ripProduct.model.po.CarcassMaterialKey;
import com.itCompany.ripProduct.model.po.JobWelder;
import com.itCompany.ripProduct.model.po.Material;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class CarcassConverter implements ObjectConverter {

    MaterialConverter materialConverter;

    @Override
    public Object convertToPersist(Object dto) {
        Carcass carcass = new Carcass();
        carcass.setId(((CarcassDto) dto).getId());
        carcass.setArticle(((CarcassDto) dto).getArticle());
        carcass.setName(((CarcassDto) dto).getName());
        carcass.setPrice(((CarcassDto) dto).getPrice());
        carcass.setRemoved(((CarcassDto) dto).isRemoved());
        carcass.setComment(((CarcassDto) dto).getComment());

        if (Objects.nonNull(((CarcassDto) dto).getJobWelder())) {
            JobWelder jobWelder = (JobWelder) JobWelderConverter.getInstanse().convertToPersist(((CarcassDto) dto).getJobWelder());
            carcass.setJobWelder(jobWelder);
        }

        if (Objects.nonNull(((CarcassDto) dto).getMaterials()) && !((CarcassDto) dto).getMaterials().isEmpty()) {
            Material material;
            List<CarcassMaterial> carcassMaterials = new ArrayList<CarcassMaterial>();
            CarcassMaterial carcassMaterial;
            CarcassMaterialKey carcassMaterialKey;
            for (MaterialDto rm : ((CarcassDto) dto).getMaterials()) {
                material = (Material) materialConverter.convertToPersist(rm);
                carcassMaterial = new CarcassMaterial();
                carcassMaterialKey = new CarcassMaterialKey();
                carcassMaterialKey.setMaterialId(material.getId());
                carcassMaterialKey.setCarcassId(carcass.getId());
                carcassMaterial.setId(carcassMaterialKey);
                carcassMaterial.setMaterial(material);
                carcassMaterial.setQuantity(rm.getQuantity());
                carcassMaterial.setCarcass(carcass);
                carcassMaterials.add(carcassMaterial);
            }
            carcass.setCarcassMaterials(carcassMaterials);
        }
        return carcass;
    }

    @Override
    public Object convertToDto(Object persist) {
        CarcassDto carcassDto = new CarcassDto();
        carcassDto.setId(((Carcass) persist).getId());
        carcassDto.setArticle(((Carcass) persist).getArticle());
        carcassDto.setName(((Carcass) persist).getName());
        carcassDto.setPrice(((Carcass) persist).getPrice());
        carcassDto.setRemoved(((Carcass) persist).isRemoved());
        carcassDto.setComment(((Carcass) persist).getComment());

        if (Objects.nonNull(((Carcass) persist).getJobWelder())) {
            JobDto jobCarcassDto = (JobDto) JobWelderConverter.getInstanse().convertToDto(((Carcass) persist).getJobWelder());
            carcassDto.setJobWelder(jobCarcassDto);
        }

        if (Objects.nonNull(((Carcass) persist).getCarcassMaterials()) && !((Carcass) persist).getCarcassMaterials().isEmpty()) {
            List<MaterialDto> materials = new ArrayList<MaterialDto>();
            MaterialDto materialDto;
            for (CarcassMaterial rm : ((Carcass) persist).getCarcassMaterials()) {
                materialDto = (MaterialDto) materialConverter.convertToDto(rm.getMaterial());
                materialDto.setQuantity(rm.getQuantity());
                materials.add(materialDto);
            }
            carcassDto.setMaterials(materials);
        }
        return carcassDto;
    }

}
