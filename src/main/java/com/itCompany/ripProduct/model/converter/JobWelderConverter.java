package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobWelder;

public class JobWelderConverter implements ObjectConverter {

    private volatile static JobWelderConverter instance; 

    private JobWelderConverter() {
    }

    public static JobWelderConverter getInstanse() {
        if (instance == null) {
            instance = new JobWelderConverter();
        }
        return instance;
    }

    @Override
    public Object convertToPersist(Object dto) {
        JobWelder model = new JobWelder();
        if (Objects.nonNull(((JobDto) dto).getId())) {
            model.setId(((JobDto) dto).getId());
        }
        model.setArticle(((JobDto) dto).getArticle());
        model.setName(((JobDto) dto).getName());
        model.setPrice(((JobDto) dto).getPrice());
        model.setRemoved(((JobDto) dto).isRemoved());
        model.setComment(((JobDto) dto).getComment());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        JobDto modelDto = new JobDto();
        modelDto.setId(((JobWelder) persist).getId());
        modelDto.setArticle(((JobWelder) persist).getArticle());
        modelDto.setName(((JobWelder) persist).getName());
        modelDto.setPrice(((JobWelder) persist).getPrice());
        modelDto.setRemoved(((JobWelder) persist).isRemoved());
        modelDto.setComment(((JobWelder) persist).getComment());
        return modelDto;
    }

}
