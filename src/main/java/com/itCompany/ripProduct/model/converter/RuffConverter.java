package com.itCompany.ripProduct.model.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.JobRuff;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.model.po.Ruff;
import com.itCompany.ripProduct.model.po.RuffMaterial;
import com.itCompany.ripProduct.model.po.RuffMaterialKey;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class RuffConverter implements ObjectConverter {

    MaterialConverter materialConverter;

    @Override
    public Object convertToPersist(Object dto) {
        Ruff ruff = new Ruff();
        ruff.setId(((RuffDto) dto).getId());
        ruff.setArticle(((RuffDto) dto).getArticle());
        ruff.setName(((RuffDto) dto).getName());
        ruff.setPrice(((RuffDto) dto).getPrice());
        ruff.setRemoved(((RuffDto) dto).isRemoved());
        ruff.setComment(((RuffDto) dto).getComment());

        if (((RuffDto) dto).getJobRuff() != null) {
            JobRuff jobRuff = (JobRuff) JobRuffConverter.getInstanse().convertToPersist(((RuffDto) dto).getJobRuff());
            ruff.setJobRuff(jobRuff);
        }

        if (((RuffDto) dto).getMaterials() != null && !((RuffDto) dto).getMaterials().isEmpty()) {
            Material material;
            List<RuffMaterial> ruffMaterials = new ArrayList<RuffMaterial>();
            RuffMaterial ruffMaterial;
            RuffMaterialKey ruffMaterialKey;
            for (MaterialDto rm : ((RuffDto) dto).getMaterials()) {
                material = (Material) materialConverter.convertToPersist(rm);
                ruffMaterial = new RuffMaterial();
                ruffMaterialKey = new RuffMaterialKey();
                ruffMaterialKey.setMaterialId(rm.getId());
                ruffMaterialKey.setRuffId(((RuffDto) dto).getId());
                ruffMaterial.setId(ruffMaterialKey);
                ruffMaterial.setMaterial(material);
                ruffMaterial.setQuantity(rm.getQuantity());
                ruffMaterial.setRuff(ruff);
                ruffMaterials.add(ruffMaterial);
            }
            ruff.setRuffMaterials(ruffMaterials);
        }
        return ruff;
    }

    @Override
    public Object convertToDto(Object persist) {
        RuffDto ruffDto = new RuffDto();
        ruffDto.setId(((Ruff) persist).getId());
        ruffDto.setArticle(((Ruff) persist).getArticle());
        ruffDto.setName(((Ruff) persist).getName());
        ruffDto.setPrice(((Ruff) persist).getPrice());
        ruffDto.setRemoved(((Ruff) persist).isRemoved());
        ruffDto.setComment(((Ruff) persist).getComment());

        if (((Ruff) persist).getJobRuff() != null) {
            JobDto jobRuffDto = (JobDto) JobRuffConverter.getInstanse().convertToDto(((Ruff) persist).getJobRuff());
            ruffDto.setJobRuff(jobRuffDto);
        }

        if (((Ruff) persist).getRuffMaterials() != null && !((Ruff) persist).getRuffMaterials().isEmpty()) {
            List<MaterialDto> materials = new ArrayList<MaterialDto>();
            MaterialDto materialDto;
            for (RuffMaterial rm : ((Ruff) persist).getRuffMaterials()) {
                materialDto = (MaterialDto) materialConverter.convertToDto(rm.getMaterial());
                materialDto.setQuantity(rm.getQuantity());
                materials.add(materialDto);
            }
            ruffDto.setMaterials(materials);
        }
        return ruffDto;
    }

}
