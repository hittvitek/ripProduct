package com.itCompany.ripProduct.model.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.CarcassDto;
import com.itCompany.ripProduct.model.dto.FoneDto;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.Carcass;
import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.model.po.FoneMaterial;
import com.itCompany.ripProduct.model.po.FoneMaterialKey;
import com.itCompany.ripProduct.model.po.FoneRuff;
import com.itCompany.ripProduct.model.po.FoneRuffKey;
import com.itCompany.ripProduct.model.po.JobBraid;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.model.po.Ruff;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class FoneConverter implements ObjectConverter {

    RuffConverter ruffConverter;
    CarcassConverter carcassConverter;
    MaterialConverter materialConverter;

    @Override
    public Object convertToPersist(Object dto) {
        Fone fone = new Fone();
        if (Objects.nonNull(fone.getId())) {
            fone.setId(((FoneDto) dto).getId());
        }
        fone.setId(((FoneDto) dto).getId());
        fone.setArticle(((FoneDto) dto).getArticle());
        fone.setName(((FoneDto) dto).getName());
        fone.setPrice(((FoneDto) dto).getPrice());
        fone.setRemoved(((FoneDto) dto).isRemoved());
        fone.setComment(((FoneDto) dto).getComment());

        if (Objects.nonNull(((FoneDto) dto).getJobBraid())) {
            JobBraid jobBraid = (JobBraid) JobBraidConverter.getInstanse().convertToPersist(((FoneDto) dto).getJobBraid());
            fone.setJobBraid(jobBraid);
        }
        if (Objects.nonNull(((FoneDto) dto).getCarcass())) {
            Carcass carcass = (Carcass) carcassConverter.convertToPersist(((FoneDto) dto).getCarcass());
            fone.setCarcass(carcass);
        }
        if (Objects.nonNull(((FoneDto) dto).getRuffs()) && !((FoneDto) dto).getRuffs().isEmpty()) {
            Ruff ruff;
            List<FoneRuff> foneRuffs = new ArrayList<FoneRuff>();
            FoneRuff foneRuff;
            FoneRuffKey foneRuffKey;
            for (RuffDto el : ((FoneDto) dto).getRuffs()) {
                ruff = (Ruff) ruffConverter.convertToPersist(el);
                foneRuff = new FoneRuff();
                foneRuffKey = new FoneRuffKey();
                foneRuffKey.setRuffId(ruff.getId());
                foneRuffKey.setFoneId(fone.getId());
                foneRuff.setId(foneRuffKey);
                foneRuff.setRuff(ruff);
                foneRuff.setQuantity(el.getQuantity());
                foneRuff.setFone(fone);
                foneRuffs.add(foneRuff);
            }
            fone.setFoneRuffs(foneRuffs);
        }
        if (Objects.nonNull(((FoneDto) dto).getMaterials()) && !((FoneDto) dto).getMaterials().isEmpty()) {
            Material material;
            List<FoneMaterial> foneMaterials = new ArrayList<FoneMaterial>();
            FoneMaterial foneMaterial;
            FoneMaterialKey foneMaterialKey;
            for (MaterialDto el : ((FoneDto) dto).getMaterials()) {
                material = (Material) materialConverter.convertToPersist(el);
                foneMaterial = new FoneMaterial();
                foneMaterialKey = new FoneMaterialKey();
                foneMaterialKey.setMaterialId(material.getId());
                foneMaterialKey.setFoneId(fone.getId());
                foneMaterial.setId(foneMaterialKey);
                foneMaterial.setMaterial(material);
                foneMaterial.setQuantity(el.getQuantity());
                foneMaterial.setFone(fone);
                foneMaterials.add(foneMaterial);
            }
            fone.setFoneMaterials(foneMaterials);
        }
        return fone;
    }

    @Override
    public Object convertToDto(Object persist) {
        FoneDto foneDto = new FoneDto();
        foneDto.setId(((Fone) persist).getId());
        foneDto.setArticle(((Fone) persist).getArticle());
        foneDto.setName(((Fone) persist).getName());
        foneDto.setPrice(((Fone) persist).getPrice());
        foneDto.setRemoved(((Fone) persist).isRemoved());
        foneDto.setComment(((Fone) persist).getComment());

        if (Objects.nonNull(((Fone) persist).getJobBraid())) {
            JobDto jobFoneDto = (JobDto) JobBraidConverter.getInstanse().convertToDto(((Fone) persist).getJobBraid());
            foneDto.setJobBraid(jobFoneDto);
        }

        if (Objects.nonNull(((Fone) persist).getCarcass())) {
            CarcassDto carcassDto = (CarcassDto) carcassConverter.convertToDto(((Fone) persist).getCarcass());
            foneDto.setCarcass(carcassDto);
        }

        if (Objects.nonNull(((Fone) persist).getFoneRuffs()) && !((Fone) persist).getFoneRuffs().isEmpty()) {
            List<RuffDto> ruffs = new ArrayList<RuffDto>();
            RuffDto ruffDto;
            for (FoneRuff el : ((Fone) persist).getFoneRuffs()) {
                ruffDto = (RuffDto) ruffConverter.convertToDto(el.getRuff());
                ruffDto.setQuantity(el.getQuantity());
                ruffs.add(ruffDto);
            }
            foneDto.setRuffs(ruffs);
        }

        if (Objects.nonNull(((Fone) persist).getFoneMaterials()) && !((Fone) persist).getFoneMaterials().isEmpty()) {
            List<MaterialDto> materials = new ArrayList<MaterialDto>();
            MaterialDto materialDto;
            for (FoneMaterial el : ((Fone) persist).getFoneMaterials()) {
                materialDto = (MaterialDto) materialConverter.convertToDto(el.getMaterial());
                materialDto.setQuantity(el.getQuantity());
                materials.add(materialDto);
            }
            foneDto.setMaterials(materials);
        }
        return foneDto;
    }

}
