package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import com.itCompany.ripProduct.model.dto.CurrencyDto;
import com.itCompany.ripProduct.model.po.Currency;

public class CurrencyConverter implements ObjectConverter {

    private volatile static CurrencyConverter instance; 

    private CurrencyConverter() {}

    public static CurrencyConverter getInstanse() {
        if (instance == null) {
            instance = new CurrencyConverter();
        }
        return instance;
    }

    @Override
    public Object convertToPersist(Object dto) {
        Currency model = new Currency();
        if (Objects.nonNull(((CurrencyDto) dto).getId())) {
            model.setId(((CurrencyDto) dto).getId());
        }
        model.setArticle(((CurrencyDto) dto).getArticle());
        model.setPrice(((CurrencyDto) dto).getPrice());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        CurrencyDto modelDto = new CurrencyDto();
        modelDto.setId(((Currency) persist).getId());
        modelDto.setArticle(((Currency) persist).getArticle());
        modelDto.setPrice(((Currency) persist).getPrice());
        return modelDto;
    }

}
