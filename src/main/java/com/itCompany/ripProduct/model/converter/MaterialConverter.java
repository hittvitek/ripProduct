package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.Material;

@Component
public class MaterialConverter implements ObjectConverter {

    @Override
    public Object convertToPersist(Object dto) {
        Material model = new Material();
        if (Objects.nonNull(((MaterialDto) dto).getId())) {
            model.setId(((MaterialDto) dto).getId());
        }
        model.setId(((MaterialDto) dto).getId());
        model.setArticle(((MaterialDto) dto).getArticle());
        model.setName(((MaterialDto) dto).getName());
        model.setMass(((MaterialDto) dto).getMass());
        model.setLength(((MaterialDto) dto).getLength());
        model.setPrice(((MaterialDto) dto).getPrice());
        model.setPriceUanKg(((MaterialDto) dto).getPriceUanKg());
        model.setRemoved(((MaterialDto) dto).isRemoved());
        model.setComment(((MaterialDto) dto).getComment());
        model.setCost(((MaterialDto) dto).getCost());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        MaterialDto materialDto = new MaterialDto();
        materialDto.setId(((Material) persist).getId());
        materialDto.setArticle(((Material) persist).getArticle());
        materialDto.setName(((Material) persist).getName());
        materialDto.setPrice(((Material) persist).getPrice());
        materialDto.setMass(((Material) persist).getMass());
        materialDto.setLength(((Material) persist).getLength());
        materialDto.setPriceUanKg(((Material) persist).getPriceUanKg());
        materialDto.setComment(((Material) persist).getComment());
        materialDto.setRemoved(((Material) persist).isRemoved());
        materialDto.setCost(((Material) persist).getCost());
        return materialDto;
    }

}
