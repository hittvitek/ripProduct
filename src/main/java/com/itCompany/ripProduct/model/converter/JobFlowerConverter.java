package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobFlower;

public class JobFlowerConverter implements ObjectConverter {

    private volatile static JobFlowerConverter instance; 

    private JobFlowerConverter() {
    }

    public static JobFlowerConverter getInstanse() {
        if (instance == null) {
            instance = new JobFlowerConverter();
        }
        return instance;
    }

    @Override
    public Object convertToPersist(Object dto) {
        JobFlower model = new JobFlower();
        if (Objects.nonNull(((JobDto) dto).getId())) {
            model.setId(((JobDto) dto).getId());
        }
        model.setArticle(((JobDto) dto).getArticle());
        model.setName(((JobDto) dto).getName());
        model.setPrice(((JobDto) dto).getPrice());
        model.setRemoved(((JobDto) dto).isRemoved());
        model.setComment(((JobDto) dto).getComment());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        JobDto modelDto = new JobDto();
        modelDto.setId(((JobFlower) persist).getId());
        modelDto.setArticle(((JobFlower) persist).getArticle());
        modelDto.setName(((JobFlower) persist).getName());
        modelDto.setPrice(((JobFlower) persist).getPrice());
        modelDto.setRemoved(((JobFlower) persist).isRemoved());
        modelDto.setComment(((JobFlower) persist).getComment());
        return modelDto;
    }

}
