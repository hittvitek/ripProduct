package com.itCompany.ripProduct.model.converter;

public interface ObjectConverter {

    Object convertToPersist(Object dto);

    Object convertToDto(Object persist);

}
