package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobRuff;

public class JobRuffConverter implements ObjectConverter {

    private volatile static JobRuffConverter instance; 

    private JobRuffConverter() {
    }

    public static JobRuffConverter getInstanse() {
        if (instance == null) {
            instance = new JobRuffConverter();
        }
        return instance;
    }

    @Override
    public Object convertToPersist(Object dto) {
        JobRuff model = new JobRuff();
        if (Objects.nonNull(((JobDto) dto).getId())) {
            model.setId(((JobDto) dto).getId());
        }
        model.setArticle(((JobDto) dto).getArticle());
        model.setName(((JobDto) dto).getName());
        model.setPrice(((JobDto) dto).getPrice());
        model.setRemoved(((JobDto) dto).isRemoved());
        model.setComment(((JobDto) dto).getComment());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        JobDto modelDto = new JobDto();
        modelDto.setId(((JobRuff) persist).getId());
        modelDto.setArticle(((JobRuff) persist).getArticle());
        modelDto.setName(((JobRuff) persist).getName());
        modelDto.setPrice(((JobRuff) persist).getPrice());
        modelDto.setRemoved(((JobRuff) persist).isRemoved());
        modelDto.setComment(((JobRuff) persist).getComment());
        return modelDto;
    }

}
