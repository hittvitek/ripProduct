package com.itCompany.ripProduct.model.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.FlowerDto;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.model.po.FlowerMaterial;
import com.itCompany.ripProduct.model.po.FlowerMaterialKey;
import com.itCompany.ripProduct.model.po.JobFlower;
import com.itCompany.ripProduct.model.po.Material;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FlowerConverter implements ObjectConverter {

    MaterialConverter materialConverter;

    @Override
    public Object convertToPersist(Object dto) {
        Flower flower = new Flower();
        flower.setId(((FlowerDto) dto).getId());
        flower.setArticle(((FlowerDto) dto).getArticle());
        flower.setName(((FlowerDto) dto).getName());
        flower.setPrice(((FlowerDto) dto).getPrice());
        flower.setRemoved(((FlowerDto) dto).isRemoved());
        flower.setComment(((FlowerDto) dto).getComment());

        if (Objects.nonNull(((FlowerDto) dto).getJobFlower())) {
            JobFlower jobFlower = (JobFlower) JobFlowerConverter.getInstanse().convertToPersist(((FlowerDto) dto).getJobFlower());
            flower.setJobFlower(jobFlower);
        }

        if (Objects.nonNull(((FlowerDto) dto).getMaterials()) && !((FlowerDto) dto).getMaterials().isEmpty()) {
            Material material;
            List<FlowerMaterial> flowerMaterials = new ArrayList<FlowerMaterial>();
            FlowerMaterial flowerMaterial;
            FlowerMaterialKey flowerMaterialKey;
            for (MaterialDto rm : ((FlowerDto) dto).getMaterials()) {
                material = (Material) materialConverter.convertToPersist(rm);
                flowerMaterial = new FlowerMaterial();
                flowerMaterialKey = new FlowerMaterialKey();
                flowerMaterialKey.setMaterialId(material.getId());
                flowerMaterialKey.setFlowerId(flower.getId());
                flowerMaterial.setId(flowerMaterialKey);
                flowerMaterial.setMaterial(material);
                flowerMaterial.setQuantity(rm.getQuantity());
                flowerMaterial.setFlower(flower);
                flowerMaterials.add(flowerMaterial);
            }
            flower.setFlowerMaterials(flowerMaterials);
        }
        return flower;
    }

    @Override
    public Object convertToDto(Object persist) {
        FlowerDto flowerDto = new FlowerDto();
        flowerDto.setId(((Flower) persist).getId());
        flowerDto.setArticle(((Flower) persist).getArticle());
        flowerDto.setName(((Flower) persist).getName());
        flowerDto.setPrice(((Flower) persist).getPrice());
        flowerDto.setRemoved(((Flower) persist).isRemoved());
        flowerDto.setComment(((Flower) persist).getComment());

        if (Objects.nonNull(((Flower) persist).getJobFlower())) {
            JobDto jobFlowerDto = (JobDto) JobFlowerConverter.getInstanse().convertToDto(((Flower) persist).getJobFlower());
            flowerDto.setJobFlower(jobFlowerDto);
        }

        if (Objects.nonNull(((Flower) persist).getFlowerMaterials()) && !((Flower) persist).getFlowerMaterials().isEmpty()) {
            List<MaterialDto> materials = new ArrayList<MaterialDto>();
            MaterialDto materialDto;
            for (FlowerMaterial rm : ((Flower) persist).getFlowerMaterials()) {
                materialDto = (MaterialDto) materialConverter.convertToDto(rm.getMaterial());
                materialDto.setQuantity(rm.getQuantity());
                materials.add(materialDto);
            }
            flowerDto.setMaterials(materials);
        }
        return flowerDto;
    }

}
