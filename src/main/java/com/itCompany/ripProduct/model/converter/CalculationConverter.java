package com.itCompany.ripProduct.model.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.CalculationDto;
import com.itCompany.ripProduct.model.dto.FlowerDto;
import com.itCompany.ripProduct.model.dto.FoneDto;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.Calculation;
import com.itCompany.ripProduct.model.po.CalculationFlower;
import com.itCompany.ripProduct.model.po.CalculationFlowerKey;
import com.itCompany.ripProduct.model.po.CalculationMaterial;
import com.itCompany.ripProduct.model.po.CalculationMaterialKey;
import com.itCompany.ripProduct.model.po.CalculationRuff;
import com.itCompany.ripProduct.model.po.CalculationRuffKey;
import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.model.po.JobProduct;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.model.po.Ruff;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class CalculationConverter implements ObjectConverter {

    MaterialConverter materialConverter;
    RuffConverter ruffConverter;
    FlowerConverter flowerConverter;
    FoneConverter foneConverter;

    JobProductConverter jobProductConverter;

    @Override
    public Object convertToPersist(Object dto) {
        Calculation calculation = new Calculation();
        calculation.setId(((CalculationDto) dto).getId());
        calculation.setArticle(((CalculationDto) dto).getArticle());
        calculation.setName(((CalculationDto) dto).getName());
        calculation.setPrice(((CalculationDto) dto).getPrice());
        calculation.setRemoved(((CalculationDto) dto).isRemoved());
        calculation.setComment(((CalculationDto) dto).getComment());
        if (Objects.nonNull(((CalculationDto) dto).getJobProduct())) {
            JobProduct jobProduct = (JobProduct) jobProductConverter.convertToPersist(((CalculationDto) dto).getJobProduct());
            calculation.setJobProduct(jobProduct);
        }
        if (Objects.nonNull(((CalculationDto) dto).getFone())) {
            Fone fone = (Fone) foneConverter.convertToPersist(((CalculationDto) dto).getFone());
            calculation.setFone(fone);
        }
        if (Objects.nonNull(((CalculationDto) dto).getFlowers()) && !((CalculationDto) dto).getFlowers().isEmpty()) {
            Flower flower;
            List<CalculationFlower> calculationFlowers = new ArrayList<CalculationFlower>();
            CalculationFlower calculationFlower;
            CalculationFlowerKey calculationFlowerKey;
            for (FlowerDto el : ((CalculationDto) dto).getFlowers()) {
                flower = (Flower) flowerConverter.convertToPersist(el);
                calculationFlower = new CalculationFlower();
                calculationFlowerKey = new CalculationFlowerKey();
                calculationFlowerKey.setFlowerId(flower.getId());
                calculationFlowerKey.setCalculationId(calculation.getId());
                calculationFlower.setId(calculationFlowerKey);
                calculationFlower.setFlower(flower);
                calculationFlower.setQuantity(el.getQuantity());
                calculationFlower.setCalculation(calculation);
                calculationFlowers.add(calculationFlower);
            }
            calculation.setCalculationFlowers(calculationFlowers);
        }
        if (Objects.nonNull(((CalculationDto) dto).getRuffs()) && !((CalculationDto) dto).getRuffs().isEmpty()) {
            Ruff ruff;
            List<CalculationRuff> calculationRuffs = new ArrayList<CalculationRuff>();
            CalculationRuff calculationRuff;
            CalculationRuffKey calculationRuffKey;
            for (RuffDto el : ((CalculationDto) dto).getRuffs()) {
                ruff = (Ruff) ruffConverter.convertToPersist(el);
                calculationRuff = new CalculationRuff();
                calculationRuffKey = new CalculationRuffKey();
                calculationRuffKey.setRuffId(ruff.getId());
                calculationRuffKey.setCalculationId(calculation.getId());
                calculationRuff.setId(calculationRuffKey);
                calculationRuff.setRuff(ruff);
                calculationRuff.setQuantity(el.getQuantity());
                calculationRuff.setCalculation(calculation);
                calculationRuffs.add(calculationRuff);
            }
            calculation.setCalculationRuffs(calculationRuffs);
        }
        if (Objects.nonNull(((CalculationDto) dto).getMaterials()) && !((CalculationDto) dto).getMaterials().isEmpty()) {
            Material material;
            List<CalculationMaterial> calculationMaterials = new ArrayList<CalculationMaterial>();
            CalculationMaterial calculationMaterial;
            CalculationMaterialKey calculationMaterialKey;
            for (MaterialDto el : ((CalculationDto) dto).getMaterials()) {
                material = (Material) materialConverter.convertToPersist(el);
                calculationMaterial = new CalculationMaterial();
                calculationMaterialKey = new CalculationMaterialKey();
                calculationMaterialKey.setMaterialId(material.getId());
                calculationMaterialKey.setCalculationId(calculation.getId());
                calculationMaterial.setId(calculationMaterialKey);
                calculationMaterial.setMaterial(material);
                calculationMaterial.setQuantity(el.getQuantity());
                calculationMaterial.setCalculation(calculation);
                calculationMaterials.add(calculationMaterial);
            }
            calculation.setCalculationMaterials(calculationMaterials);
        }
        return calculation;
    }

    @Override
    public Object convertToDto(Object persist) {
        CalculationDto calculationDto = new CalculationDto();
        calculationDto.setId(((Calculation) persist).getId());
        calculationDto.setArticle(((Calculation) persist).getArticle());
        calculationDto.setName(((Calculation) persist).getName());
        calculationDto.setPrice(((Calculation) persist).getPrice());
        calculationDto.setRemoved(((Calculation) persist).isRemoved());
        calculationDto.setComment(((Calculation) persist).getComment());
        if (Objects.nonNull(((Calculation) persist).getJobProduct())) {
            JobDto job = (JobDto) jobProductConverter.convertToDto(((Calculation) persist).getJobProduct());
            calculationDto.setJobProduct(job);
        }
        if (Objects.nonNull(((Calculation) persist).getFone())) {
            FoneDto fone = (FoneDto) foneConverter.convertToDto(((Calculation) persist).getFone());
            calculationDto.setFone(fone);
        }
        if (Objects.nonNull(((Calculation) persist).getCalculationFlowers()) && !((Calculation) persist).getCalculationFlowers().isEmpty()) {
            List<FlowerDto> flowers = new ArrayList<FlowerDto>();
            FlowerDto flowerDto;
            for (CalculationFlower el : ((Calculation) persist).getCalculationFlowers()) {
                flowerDto = (FlowerDto) flowerConverter.convertToDto(el.getFlower());
                flowerDto.setQuantity(el.getQuantity());
                flowers.add(flowerDto);
            }
            calculationDto.setFlowers(flowers);
        }
        if (Objects.nonNull(((Calculation) persist).getCalculationRuffs()) && !((Calculation) persist).getCalculationRuffs().isEmpty()) {
            List<RuffDto> ruffs = new ArrayList<RuffDto>();
            RuffDto ruffDto;
            for (CalculationRuff el : ((Calculation) persist).getCalculationRuffs()) {
                ruffDto = (RuffDto) ruffConverter.convertToDto(el.getRuff());
                ruffDto.setQuantity(el.getQuantity());
                ruffs.add(ruffDto);
            }
            calculationDto.setRuffs(ruffs);
        }
        if (Objects.nonNull(((Calculation) persist).getCalculationMaterials()) && !((Calculation) persist).getCalculationMaterials().isEmpty()) {
            List<MaterialDto> materials = new ArrayList<MaterialDto>();
            MaterialDto materialDto;
            for (CalculationMaterial el : ((Calculation) persist).getCalculationMaterials()) {
                materialDto = (MaterialDto) materialConverter.convertToDto(el.getMaterial());
                materialDto.setQuantity(el.getQuantity());
                materials.add(materialDto);
            }
            calculationDto.setMaterials(materials);
        }
        return calculationDto;
    }

}
