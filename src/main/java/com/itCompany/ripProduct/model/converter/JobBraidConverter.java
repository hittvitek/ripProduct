package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobBraid;

public class JobBraidConverter implements ObjectConverter {

    private volatile static JobBraidConverter instance; 

    private JobBraidConverter() {
    }

    public static JobBraidConverter getInstanse() {
        if (instance == null) {
            instance = new JobBraidConverter();
        }
        return instance;
    }

    @Override
    public Object convertToPersist(Object dto) {
        JobBraid model = new JobBraid();
        if (Objects.nonNull(((JobDto) dto).getId())) {
            model.setId(((JobDto) dto).getId());
        }
        model.setArticle(((JobDto) dto).getArticle());
        model.setName(((JobDto) dto).getName());
        model.setPrice(((JobDto) dto).getPrice());
        model.setRemoved(((JobDto) dto).isRemoved());
        model.setComment(((JobDto) dto).getComment());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        JobDto modelDto = new JobDto();
        modelDto.setId(((JobBraid) persist).getId());
        modelDto.setArticle(((JobBraid) persist).getArticle());
        modelDto.setName(((JobBraid) persist).getName());
        modelDto.setPrice(((JobBraid) persist).getPrice());
        modelDto.setRemoved(((JobBraid) persist).isRemoved());
        modelDto.setComment(((JobBraid) persist).getComment());
        return modelDto;
    }

}
