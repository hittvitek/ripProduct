package com.itCompany.ripProduct.model.converter;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobProduct;

@Component
public class JobProductConverter implements ObjectConverter {

    @Override
    public Object convertToPersist(Object dto) {
        JobProduct model = new JobProduct();
        if (Objects.nonNull(((JobDto) dto).getId())) {
            model.setId(((JobDto) dto).getId());
        }
        model.setArticle(((JobDto) dto).getArticle());
        model.setName(((JobDto) dto).getName());
        model.setPrice(((JobDto) dto).getPrice());
        model.setRemoved(((JobDto) dto).isRemoved());
        model.setComment(((JobDto) dto).getComment());
        return model;
    }

    @Override
    public Object convertToDto(Object persist) {
        JobDto modelDto = new JobDto();
        modelDto.setId(((JobProduct) persist).getId());
        modelDto.setArticle(((JobProduct) persist).getArticle());
        modelDto.setName(((JobProduct) persist).getName());
        modelDto.setPrice(((JobProduct) persist).getPrice());
        modelDto.setRemoved(((JobProduct) persist).isRemoved());
        modelDto.setComment(((JobProduct) persist).getComment());
        return modelDto;
    }

}
