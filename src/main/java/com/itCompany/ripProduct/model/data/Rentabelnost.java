package com.itCompany.ripProduct.model.data;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Data
public class Rentabelnost {

    String calculationArticle;
    Double calculationPrice;
    Integer productPrice;
    Double rentabelnost;
    Integer quantity;

}
