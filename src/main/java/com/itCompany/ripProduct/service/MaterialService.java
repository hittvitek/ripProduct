package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Material;

public interface MaterialService {

    Material getMaterial(final Long id);

    Material getMaterialByName(final String name);

    Material getMaterialByArticle(final String article);

    List<Material> getMaterials();

    void saveMaterial(final Material model);

    void deleteMaterial(final Long id);

	void recalculate();
}
