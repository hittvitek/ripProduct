package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.CalculationMaterial;

public interface CalculationMaterialService {

    void saveCalculationMaterial(final List<CalculationMaterial> calculationMaterial);

}
