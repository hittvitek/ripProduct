package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Fone;

public interface FoneService {

    Fone getFone(final Long id);

    Fone getFoneByName(final String name);

    Fone getFoneByArticle(final String article);

    List<Fone> getFones();

    void saveFone(final Fone model);

    void deleteFone(final Long id);

    void deleteFoneMaterial(Long foneId, Long materialId);

    void deleteFoneRuff(Long foneId, Long ruffId);

}
