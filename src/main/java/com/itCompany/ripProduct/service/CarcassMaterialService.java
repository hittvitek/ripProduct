package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.CarcassMaterial;

public interface CarcassMaterialService {

    void saveCarcassMaterial(final List<CarcassMaterial> carcassMaterial);

}
