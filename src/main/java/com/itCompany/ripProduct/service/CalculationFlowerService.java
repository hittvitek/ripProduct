package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.CalculationFlower;

public interface CalculationFlowerService {

    void saveCalculationFlower(final List<CalculationFlower> calculationFlower);

}
