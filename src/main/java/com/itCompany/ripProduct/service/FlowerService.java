package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Flower;

public interface FlowerService {
    Flower getFlower(final Long id);

    Flower getFlowerByName(final String name);

    Flower getFlowerByArticle(final String article);

    List<Flower> getFlowers();

    void saveFlower(final Flower model);

    void deleteFlower(final Long id);

    void deleteFlowerMaterial(Long flowerId, Long materialId);

}
