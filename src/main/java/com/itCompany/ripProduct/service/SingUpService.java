package com.itCompany.ripProduct.service;

import com.itCompany.ripProduct.model.dto.UserDto;

public interface SingUpService {

    void singUp(UserDto userDto);

}
