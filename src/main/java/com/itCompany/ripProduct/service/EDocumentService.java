package com.itCompany.ripProduct.service;

import java.io.IOException;
import java.util.Map;

import com.itCompany.ripProduct.enums.Composite;

public interface EDocumentService {

    void reportTableSold(final java.sql.Date dateFrom, final java.sql.Date dateTo) throws IOException;

    void reportTableListCalculation(final boolean printPrice, Map<Composite, Map<String, Object>> maps, final long... ids);

}
