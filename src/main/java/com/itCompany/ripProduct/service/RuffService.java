package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Ruff;

public interface RuffService {
    Ruff getRuff(final Long id);

    Ruff getRuffByName(final String name);

    Ruff getRuffByArticle(final String article);

    List<Ruff> getRuffs();

    void saveRuff(final Ruff model);

    void deleteRuff(final Long id);

    void deleteRuffMaterial(Long ruffId, Long materialId);

}
