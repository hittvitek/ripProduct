package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.JobRuff;

public interface JobRuffService {

    JobRuff getJobRuff(final Long id);

    JobRuff getJobRuffByName(final String name);

    JobRuff getJobRuffByArticle(final String article);

    List<JobRuff> getJobRuffs();

    void saveJobRuff(final JobRuff model);

    void deleteJobRuff(final Long id);
}
