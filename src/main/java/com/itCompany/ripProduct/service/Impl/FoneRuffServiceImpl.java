package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.FoneRuff;
import com.itCompany.ripProduct.repository.jpa.FoneRuffRepository;
import com.itCompany.ripProduct.service.FoneRuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class FoneRuffServiceImpl implements FoneRuffService{

    FoneRuffRepository foneRuffRepository;

    @Transactional
    @Override
    public void saveFoneRuff(List<FoneRuff> foneRuff) {
        foneRuffRepository.saveAll(foneRuff);
    }

}
