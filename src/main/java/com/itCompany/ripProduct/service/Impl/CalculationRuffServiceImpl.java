package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.CalculationRuff;
import com.itCompany.ripProduct.repository.jpa.CalculationRuffRepository;
import com.itCompany.ripProduct.service.CalculationRuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CalculationRuffServiceImpl implements CalculationRuffService{

    CalculationRuffRepository calculationRuffRepository;

    @Transactional
    @Override
    public void saveCalculationRuff(List<CalculationRuff> calculationRuff) {
        calculationRuffRepository.saveAll(calculationRuff);
    }

}
