package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.CalculationFlower;
import com.itCompany.ripProduct.repository.jpa.CalculationFlowerRepository;
import com.itCompany.ripProduct.service.CalculationFlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CalculationFlowerServiceImpl implements CalculationFlowerService{

    CalculationFlowerRepository calculationFlowerRepository;

    @Transactional
    @Override
    public void saveCalculationFlower(List<CalculationFlower> calculationFlower) {
        calculationFlowerRepository.saveAll(calculationFlower);
    }

}
