package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.JobFlower;
import com.itCompany.ripProduct.repository.jpa.JobFlowerRepository;
import com.itCompany.ripProduct.service.JobFlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class JobFlowerServiceImpl implements JobFlowerService {

    JobFlowerRepository jobFlowerRepository;

    @Override
    public JobFlower getJobFlower(final Long id) {
        return jobFlowerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found jobFlower id = " + id));
    }

    @Override
    public JobFlower getJobFlowerByName(final String name) {
        return jobFlowerRepository.findByName(name);
    }

    @Override
    public JobFlower getJobFlowerByArticle(final String article) {
        return jobFlowerRepository.findByArticle(article);
    }

    @Override
    public List<JobFlower> getJobFlowers() {
        return (List<JobFlower>)jobFlowerRepository.findAll();
    }

    @Override
    public void saveJobFlower(final JobFlower model) {
        jobFlowerRepository.save(model);
    }

    @Override
    public void deleteJobFlower(final Long id) {
        jobFlowerRepository.deleteById(id);
    }

}
