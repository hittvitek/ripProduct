package com.itCompany.ripProduct.service.Impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.enums.Role;
import com.itCompany.ripProduct.enums.State;
import com.itCompany.ripProduct.model.dto.UserDto;
import com.itCompany.ripProduct.model.po.User;
import com.itCompany.ripProduct.repository.jpa.UserRepository;
import com.itCompany.ripProduct.service.SingUpService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class SingUpServiceImpl implements SingUpService {

    UserRepository userRepository;
    PasswordEncoder passwordEncoder;

    @Override
    public void singUp(UserDto userDto) {
        String hashPassword = passwordEncoder.encode(userDto.getPassword());
        User user = User.builder()
            .id(userDto.getId())
            .login(userDto.getLogin())
            .password(hashPassword)
            .role(Role.USER)
            .state(State.ACTIVE)
            .build();
        userRepository.save(user);
    }

}
