package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.Carcass;
import com.itCompany.ripProduct.repository.jpa.CarcassRepository;
import com.itCompany.ripProduct.service.CarcassService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CarcassServiceImpl implements CarcassService {

    CarcassRepository carcassRepository;

    @Override
    public Carcass getCarcass(final Long id) {
        return carcassRepository.findByIdView(id).orElseThrow(() -> new EntityNotFoundException("Not found carcass id = " + id));
    }

    @Override
    public Carcass getCarcassByName(final String name) {
        return carcassRepository.findByNameView(name);
    }

    @Override
    public Carcass getCarcassByArticle(final String article) {
        return carcassRepository.findByArticleView(article);
    }

    @Override
    public List<Carcass> getCarcass() {
        return (List<Carcass>)carcassRepository.findCarcassView();
    }

    @Override
    public void saveCarcass(final Carcass carcass) {
        carcassRepository.save(carcass);
    }

    @Override
    public void deleteCarcass(final Long id) {
        carcassRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteCarcassMaterial(final Long carcassId, final Long materialId) {
        carcassRepository.deleteCarcassMaterial(carcassId, materialId);
    }

}
