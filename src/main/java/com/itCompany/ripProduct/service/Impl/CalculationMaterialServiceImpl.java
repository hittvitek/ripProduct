package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.CalculationMaterial;
import com.itCompany.ripProduct.repository.jpa.CalculationMaterialRepository;
import com.itCompany.ripProduct.service.CalculationMaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CalculationMaterialServiceImpl implements CalculationMaterialService{

    CalculationMaterialRepository calculationMaterialRepository;

    @Transactional
    @Override
    public void saveCalculationMaterial(List<CalculationMaterial> calculationMaterial) {
        calculationMaterialRepository.saveAll(calculationMaterial);
    }

}
