package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.repository.jpa.MaterialRepository;
import com.itCompany.ripProduct.service.MaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class MaterialServiceImpl implements MaterialService {

	MaterialRepository materialRepository;

    @Override
    public Material getMaterial(final Long id) {
        Material persist = materialRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found material id = " + id));
        return persist;
    }

    @Override
    public Material getMaterialByName(final String name) {
        return materialRepository.findByName(name);
    }

    @Override
    public Material getMaterialByArticle(final String article) {
        return materialRepository.findByArticle(article);
    }

    @Override
    public List<Material> getMaterials() {
        return (List<Material>)materialRepository.findAll();
    }

    @Override
    public void saveMaterial(final Material model) {
        materialRepository.save(model);
    }

    @Override
    public void deleteMaterial(final Long id) {
        materialRepository.deleteById(id);
    }

    @Override
    public void recalculate() {
        materialRepository.recalculateCalculationAll();
    }

}
