package com.itCompany.ripProduct.service.Impl;

import java.util.Objects;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.User;
import com.itCompany.ripProduct.repository.jpa.UserRepository;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@Log4j2
public class UserDetailsServiceImpl implements UserDetailsService {

    UserRepository userRepository; 

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        final User userCondidate = userRepository.findOneByLogin(login);
        if (Objects.isNull(userCondidate)) {
            throw new UsernameNotFoundException("User not found: " + login);
        } else {
            UserDetails userDetails = org.springframework.security.core.userdetails.User
                .withUsername(userCondidate.getLogin())
                .password(userCondidate.getPassword())
                .authorities(userCondidate.getRole().name()).build();
            log.info(new StringBuilder("Sing in : ").append(userCondidate.toString()));
            return userDetails;
        }
//       return new UserDetailsImpl(userRepository.findByLogin(login).orElseThrow(IllegalArgumentException::new));
    }

}
