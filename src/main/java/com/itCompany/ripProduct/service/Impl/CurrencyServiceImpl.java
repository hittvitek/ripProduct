package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.Currency;
import com.itCompany.ripProduct.repository.jpa.CurrencyRepository;
import com.itCompany.ripProduct.service.CurrencyService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CurrencyServiceImpl implements CurrencyService {

    CurrencyRepository currencyRepository;

    @Override
    public Currency getCurrency(final Long id) {
        return currencyRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found currency id = " + id));
    }

    @Override
    public Currency getCurrencyByArticle(final String article) {
        return currencyRepository.findByArticle(article);
    }

    @Override
    public List<Currency> getCurrencys() {
        return (List<Currency>)currencyRepository.findAll();
    }

    @Override
    public void saveCurrency(final Currency model) {
        currencyRepository.save(model);
    }

    @Override
    public void deleteCurrency(final Long id) {
        currencyRepository.deleteById(id);
    }

}
