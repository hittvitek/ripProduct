package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.repository.jpa.FlowerRepository;
import com.itCompany.ripProduct.service.FlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class FlowerServiceImpl implements FlowerService {

    FlowerRepository flowerRepository;

    @Override
    public Flower getFlower(final Long id) {
        return flowerRepository.findByIdView(id).orElseThrow(() -> new EntityNotFoundException("Not found flower id = " + id));
    }

    @Override
    public Flower getFlowerByName(final String name) {
        return flowerRepository.findByNameView(name);
    }

    @Override
    public Flower getFlowerByArticle(final String article) {
        return flowerRepository.findByArticleView(article);
    }

    @Override
    public List<Flower> getFlowers() {
        return (List<Flower>)flowerRepository.findFlowerView();
    }

    @Override
    public void saveFlower(final Flower flower) {
        flowerRepository.save(flower);
    }

    @Override
    public void deleteFlower(final Long id) {
        flowerRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteFlowerMaterial(final Long flowerId, final Long materialId) {
        flowerRepository.deleteFlowerMaterial(flowerId, materialId);
    }

}
