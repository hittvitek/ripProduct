package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.JobWelder;
import com.itCompany.ripProduct.repository.jpa.JobWelderRepository;
import com.itCompany.ripProduct.service.JobWelderService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class JobWelderServiceImpl implements JobWelderService {

    JobWelderRepository jobWelderRepository;

    @Override
    public JobWelder getJobWelder(final Long id) {
        return jobWelderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found jobWelder id = " + id));
    }

    @Override
    public JobWelder getJobWelderByName(final String name) {
        return jobWelderRepository.findByName(name);
    }

    @Override
    public JobWelder getJobWelderByArticle(final String article) {
        return jobWelderRepository.findByArticle(article);
    }

    @Override
    public List<JobWelder> getJobWelders() {
        return (List<JobWelder>)jobWelderRepository.findAll();
    }

    @Override
    public void saveJobWelder(final JobWelder model) {
        jobWelderRepository.save(model);
    }

    @Override
    public void deleteJobWelder(final Long id) {
        jobWelderRepository.deleteById(id);
    }

}
