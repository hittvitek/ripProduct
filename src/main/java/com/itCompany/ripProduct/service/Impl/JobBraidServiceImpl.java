package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.JobBraid;
import com.itCompany.ripProduct.repository.jpa.JobBraidRepository;
import com.itCompany.ripProduct.service.JobBraidService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class JobBraidServiceImpl implements JobBraidService {

    JobBraidRepository jobBraidRepository;

    @Override
    public JobBraid getJobBraid(final Long id) {
        return jobBraidRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found jobBraid id = " + id));
    }

    @Override
    public JobBraid getJobBraidByName(final String name) {
        return jobBraidRepository.findByName(name);
    }

    @Override
    public JobBraid getJobBraidByArticle(final String article) {
        return jobBraidRepository.findByArticle(article);
    }

    @Override
    public List<JobBraid> getJobBraids() {
        return (List<JobBraid>)jobBraidRepository.findAll();
    }

    @Override
    public void saveJobBraid(final JobBraid model) {
        jobBraidRepository.save(model);
    }

    @Override
    public void deleteJobBraid(final Long id) {
        jobBraidRepository.deleteById(id);
    }

}
