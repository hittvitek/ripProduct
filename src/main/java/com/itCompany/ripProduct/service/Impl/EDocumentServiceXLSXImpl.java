package com.itCompany.ripProduct.service.Impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.enums.Composite;
import com.itCompany.ripProduct.service.EDocumentService;
import com.itCompany.ripProduct.util.eDocument.EDocument;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@Log4j2
public class EDocumentServiceXLSXImpl implements EDocumentService {

    EDocument eDocument;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH.mm.ss - dd.MM.yyyy");

    @Override
    public void reportTableSold(final java.sql.Date dateFrom, final java.sql.Date dateTo) throws IOException {
        Runnable task = () -> {
            String PATH = "c:\\template\\Calculation\\Calculation_sold_"
                + LocalDateTime.now().format(FORMATTER) + ".xlsx";
            try (Workbook workbook = eDocument.getWorkbook(PATH);
                    OutputStream outputStream = new FileOutputStream(PATH);) {
                Sheet sheet = workbook.createSheet("Report_SOLD");
                eDocument.createXLSXTableSold(workbook, sheet, dateFrom, dateTo);
                workbook.write(outputStream);
            } catch (IOException e) {
                log.info("IOException ".concat(e.getMessage()));
            } catch (Exception e) {
                log.info("Exception : reportTableSold"/*.concat(e.getMessage())*/);
            }
        };
        CompletableFuture<Void> future = CompletableFuture.runAsync(task)
            .exceptionally( ex -> {
                log.info("Exception: reportTableSold future ".concat(ex.getStackTrace().toString()));
                return null;
            });
        future.join();
    }

    @Transactional
    @Override
    public void reportTableListCalculation(boolean printPrice, Map<Composite, Map<String, Object>> maps, long... ids) {
        Runnable task = () -> {
            String PATH = "c:\\template\\Calculation\\Calculation_list_"
                + LocalDateTime.now().format(FORMATTER) + ".xlsx";
            try (Workbook workbook = eDocument.getWorkbook(PATH);
                    OutputStream outputStream = new FileOutputStream(PATH);) {
                Sheet sheet = workbook.createSheet("Report_LIST");
                eDocument.createXLSXTableList(workbook, sheet, printPrice, maps, ids);
                workbook.write(outputStream);
            } catch (IOException e) {
                log.info("IOException: reportTableListCalculation ".concat(e.getMessage()));
            } catch (Exception e) {
                log.info("Exception: reportTableListCalculation ".concat(e.getStackTrace().toString()));
            }
        };
        CompletableFuture<Void> future = CompletableFuture.runAsync(task)
            .exceptionally( ex -> {
                log.info("Exception: reportTableListCalculation future ".concat(ex.getStackTrace().toString()));
                return null;
            });
        future.join();
    }

}
