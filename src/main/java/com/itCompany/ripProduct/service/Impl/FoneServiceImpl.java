package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.repository.jpa.FoneRepository;
import com.itCompany.ripProduct.service.FoneService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class FoneServiceImpl implements FoneService {

    FoneRepository foneRepository;

    @Override
    public Fone getFone(final Long id) {
        return foneRepository.findByIdView(id).orElseThrow(() -> new EntityNotFoundException("Not found fone id = " + id));
    }

    @Override
    public Fone getFoneByName(final String name) {
        return foneRepository.findByNameView(name);
    }

    @Override
    public Fone getFoneByArticle(final String article) {
        return foneRepository.findByArticleView(article);
    }

    @Override
    public List<Fone> getFones() {
        return (List<Fone>)foneRepository.findFoneView();
    }

    @Override
    public void saveFone(final Fone fone) {
        foneRepository.save(fone);
    }

    @Override
    public void deleteFone(final Long id) {
        foneRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteFoneMaterial(final Long foneId, final Long materialId) {
        foneRepository.deleteFoneMaterial(foneId, materialId);
    }

    @Transactional
    @Override
    public void deleteFoneRuff(Long foneId, Long ruffId) {
        foneRepository.deleteFoneRuff(foneId, ruffId);
    }

}
