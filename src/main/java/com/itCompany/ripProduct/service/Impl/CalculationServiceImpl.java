package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.Calculation;
import com.itCompany.ripProduct.repository.jpa.CalculationRepository;
import com.itCompany.ripProduct.service.CalculationService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CalculationServiceImpl implements CalculationService {

    CalculationRepository calculationRepository;

    @Override
    public Calculation getCalculation(final Long id) {
        return calculationRepository.findByIdView(id).orElseThrow(() -> new EntityNotFoundException("Not found calculation id = " + id));
    }

    @Override
    public Calculation getCalculationByName(final String name) {
        return calculationRepository.findByNameView(name);
    }

    @Override
    public Calculation getCalculationByArticle(final String article) {
        return calculationRepository.findByArticleView(article);
    }

    @Override
    public List<Calculation> getCalculations() {
        return (List<Calculation>)calculationRepository.findAllCalculationView();
    }

    @Override
    public void saveCalculation(final Calculation calculation) {
        calculationRepository.save(calculation);
    }

    @Override
    public void deleteCalculation(final Long id) {
        calculationRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteCalculationMaterial(final Long calculationId, final Long materialId) {
        calculationRepository.deleteCalculationMaterial(calculationId, materialId);
    }

    @Transactional
    @Override
    public void deleteCalculationRuff(Long calculationId, Long ruffId) {
        calculationRepository.deleteCalculationRuff(calculationId, ruffId);
    }

    @Transactional
    @Override
    public void deleteCalculationFlower(Long calculationId, Long flowerId) {
        calculationRepository.deleteCalculationFlower(calculationId, flowerId);
    }

}
