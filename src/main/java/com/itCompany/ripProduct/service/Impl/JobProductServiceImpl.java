package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.JobProduct;
import com.itCompany.ripProduct.repository.jpa.JobProductRepository;
import com.itCompany.ripProduct.service.JobProductService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class JobProductServiceImpl implements JobProductService {

    JobProductRepository jobProductRepository;

    @Override
    public JobProduct getJobProduct(final Long id) {
        return jobProductRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found jobProduct id = " + id));
    }

    @Override
    public JobProduct getJobProductByName(final String name) {
        return jobProductRepository.findByName(name);
    }

    @Override
    public JobProduct getJobProductByArticle(final String article) {
        return jobProductRepository.findByArticle(article);
    }

    @Override
    public List<JobProduct> getJobProducts() {
        return (List<JobProduct>)jobProductRepository.findAll();
    }

    @Override
    public void saveJobProduct(final JobProduct model) {
        jobProductRepository.save(model);
    }

    @Override
    public void deleteJobProduct(final Long id) {
        jobProductRepository.deleteById(id);
    }

}
