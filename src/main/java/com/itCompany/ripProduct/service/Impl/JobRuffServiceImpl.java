package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.itCompany.ripProduct.model.po.JobRuff;
import com.itCompany.ripProduct.repository.jpa.JobRuffRepository;
import com.itCompany.ripProduct.service.JobRuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class JobRuffServiceImpl implements JobRuffService {

    JobRuffRepository jobRuffRepository;

    @Override
    public JobRuff getJobRuff(final Long id) {
        return jobRuffRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found jobRuff id = " + id));
    }

    @Override
    public JobRuff getJobRuffByName(final String name) {
        return jobRuffRepository.findByName(name);
    }

    @Override
    public JobRuff getJobRuffByArticle(final String article) {
        return jobRuffRepository.findByArticle(article);
    }

    @Override
    public List<JobRuff> getJobRuffs() {
        return (List<JobRuff>)jobRuffRepository.findAll();
    }

    @Override
    public void saveJobRuff(final JobRuff model) {
        jobRuffRepository.save(model);
    }

    @Override
    public void deleteJobRuff(final Long id) {
        jobRuffRepository.deleteById(id);
    }

}
