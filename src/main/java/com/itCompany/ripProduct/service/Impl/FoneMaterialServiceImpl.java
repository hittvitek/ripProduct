package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.FoneMaterial;
import com.itCompany.ripProduct.repository.jpa.FoneMaterialRepository;
import com.itCompany.ripProduct.service.FoneMaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class FoneMaterialServiceImpl implements FoneMaterialService{

    FoneMaterialRepository foneMaterialRepository;

    @Transactional
    @Override
    public void saveFoneMaterial(List<FoneMaterial> foneMaterial) {
        foneMaterialRepository.saveAll(foneMaterial);
    }

}
