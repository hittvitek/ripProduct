package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.FlowerMaterial;
import com.itCompany.ripProduct.repository.jpa.FlowerMaterialRepository;
import com.itCompany.ripProduct.service.FlowerMaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class FlowerMaterialServiceImpl implements FlowerMaterialService{

    FlowerMaterialRepository flowerMaterialRepository;

    @Transactional
    @Override
    public void saveFlowerMaterial(List<FlowerMaterial> flowerMaterial) {
        flowerMaterialRepository.saveAll(flowerMaterial);
    }

}
