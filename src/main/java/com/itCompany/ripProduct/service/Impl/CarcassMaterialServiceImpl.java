package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.CarcassMaterial;
import com.itCompany.ripProduct.repository.jpa.CarcassMaterialRepository;
import com.itCompany.ripProduct.service.CarcassMaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class CarcassMaterialServiceImpl implements CarcassMaterialService{

    CarcassMaterialRepository carcassMaterialRepository;

    @Transactional
    @Override
    public void saveCarcassMaterial(List<CarcassMaterial> carcassMaterial) {
        carcassMaterialRepository.saveAll(carcassMaterial);
    }

}
