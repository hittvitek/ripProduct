package com.itCompany.ripProduct.service.Impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.model.po.Ruff;
import com.itCompany.ripProduct.repository.jpa.RuffRepository;
import com.itCompany.ripProduct.service.RuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
public class RuffServiceImpl implements RuffService {

    RuffRepository ruffRepository;

    @Override
    public Ruff getRuff(final Long id) {
        return ruffRepository.findByIdView(id).orElseThrow(() -> new EntityNotFoundException("Not found ruff id = " + id));
    }

    @Override
    public Ruff getRuffByName(final String name) {
        return ruffRepository.findByNameView(name);
    }

    @Override
    public Ruff getRuffByArticle(final String article) {
        return ruffRepository.findByArticleView(article);
    }

    @Override
    public List<Ruff> getRuffs() {
        return (List<Ruff>)ruffRepository.findRuffView();
    }

    @Override
    public void saveRuff(final Ruff ruff) {
        ruffRepository.save(ruff);
    }

    @Override
    public void deleteRuff(final Long id) {
        ruffRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteRuffMaterial(final Long ruffId, final Long materialId) {
        ruffRepository.deleteRuffMaterial(ruffId, materialId);
    }

}
