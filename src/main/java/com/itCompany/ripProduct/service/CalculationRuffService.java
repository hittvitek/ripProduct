package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.CalculationRuff;

public interface CalculationRuffService {

    void saveCalculationRuff(final List<CalculationRuff> calculationRuff);

}
