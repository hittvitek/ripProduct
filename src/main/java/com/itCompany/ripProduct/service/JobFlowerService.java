package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.JobFlower;

public interface JobFlowerService {

    JobFlower getJobFlower(final Long id);

    JobFlower getJobFlowerByName(final String name);

    JobFlower getJobFlowerByArticle(final String article);

    List<JobFlower> getJobFlowers();

    void saveJobFlower(final JobFlower model);

    void deleteJobFlower(final Long id);
}
