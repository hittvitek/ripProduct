package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Currency;

public interface CurrencyService {

    Currency getCurrency(final Long id);

    Currency getCurrencyByArticle(final String article);

    List<Currency> getCurrencys();

    void saveCurrency(final Currency model);

    void deleteCurrency(final Long id);
}
