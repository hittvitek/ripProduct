package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.JobWelder;

public interface JobWelderService {

    JobWelder getJobWelder(final Long id);

    JobWelder getJobWelderByName(final String name);

    JobWelder getJobWelderByArticle(final String article);

    List<JobWelder> getJobWelders();

    void saveJobWelder(final JobWelder model);

    void deleteJobWelder(final Long id);
}
