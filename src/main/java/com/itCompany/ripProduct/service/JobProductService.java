package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.JobProduct;

public interface JobProductService {

    JobProduct getJobProduct(final Long id);

    JobProduct getJobProductByName(final String name);

    JobProduct getJobProductByArticle(final String article);

    List<JobProduct> getJobProducts();

    void saveJobProduct(final JobProduct model);

    void deleteJobProduct(final Long id);
}
