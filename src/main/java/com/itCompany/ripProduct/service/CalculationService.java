package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Calculation;

public interface CalculationService {

    Calculation getCalculation(final Long id);

    Calculation getCalculationByName(final String name);

    Calculation getCalculationByArticle(final String article);

    List<Calculation> getCalculations();

    void saveCalculation(final Calculation model);

    void deleteCalculation(final Long id);

    void deleteCalculationMaterial(Long calculationId, Long materialId);

    void deleteCalculationRuff(Long calculationId, Long ruffId);

    void deleteCalculationFlower(Long calculationId, Long flowerId);

//    long getCountNotRemoved();

//    List<Rentabelnost> getRentabelnostNotRemoved(Date with, Date by);

}
