package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.JobBraid;

public interface JobBraidService {

    JobBraid getJobBraid(final Long id);

    JobBraid getJobBraidByName(final String name);

    JobBraid getJobBraidByArticle(final String article);

    List<JobBraid> getJobBraids();

    void saveJobBraid(final JobBraid model);

    void deleteJobBraid(final Long id);
}
