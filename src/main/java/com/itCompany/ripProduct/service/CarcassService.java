package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.Carcass;

public interface CarcassService {
    Carcass getCarcass(final Long id);

    Carcass getCarcassByName(final String name);

    Carcass getCarcassByArticle(final String article);

    List<Carcass> getCarcass();

    void saveCarcass(final Carcass model);

    void deleteCarcass(final Long id);

    void deleteCarcassMaterial(Long carcassId, Long materialId);

}
