package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.FoneMaterial;

public interface FoneMaterialService {

    void saveFoneMaterial(final List<FoneMaterial> foneMaterial);

}
