package com.itCompany.ripProduct.service;

import java.util.List;

import com.itCompany.ripProduct.model.po.FlowerMaterial;

public interface FlowerMaterialService {

    void saveFlowerMaterial(final List<FlowerMaterial> flowerMaterial);

}
