package com.itCompany.ripProduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EntityScan(basePackages = "com.itCompany.model.po")
//@EnableJpaRepositories(basePackages = "com.itCompany.repository")
public class RipProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(RipProductApplication.class, args);
    }

}
