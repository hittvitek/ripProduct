package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.JobRuff;

@Repository
public interface JobRuffRepository extends CrudRepository<JobRuff, Long>{

    JobRuff findByName(final String name);

    JobRuff findByArticle(final String article);

}
