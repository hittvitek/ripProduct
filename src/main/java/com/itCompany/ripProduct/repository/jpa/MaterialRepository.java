package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Material;

@Repository
public interface MaterialRepository extends CrudRepository<Material, Long> {

    Material findByName(final String name);

    Material findByArticle(final String article);

    @Query(nativeQuery = true, value = "UPDATE MATERIALS SET PRICE = PRICE")
    void recalculateCalculationAll();

}
