package com.itCompany.ripProduct.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Carcass;

@Repository
public interface CarcassRepository extends CrudRepository<Carcass, Long> {

    @Query(value = "SELECT * FROM CARCASS_V WHERE ID = ?1", nativeQuery = true)
    Optional<Carcass> findByIdView(final Long id);

    @Query(value = "SELECT * FROM CARCASS_V WHERE NAME = ?1", nativeQuery = true)
    Carcass findByNameView(final String name);

    @Query(value = "SELECT * FROM CARCASS_V WHERE ARTICLE = ?1", nativeQuery = true)
    Carcass findByArticleView(final String article);

    @Query(value = "SELECT * FROM CARCASS_V", nativeQuery = true)
    List<Carcass> findCarcassView();

    @Modifying
    @Query(value = "DELETE FROM FLOWERS_MATERIALS WHERE CARCAS_ID = ?1 AND MATERIAL_ID = ?2", nativeQuery = true)
    void deleteCarcassMaterial(final Long carcassId, final Long materialId);

}
