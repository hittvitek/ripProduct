package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.FoneRuff;

public interface FoneRuffRepository extends CrudRepository<FoneRuff, Long>{

}
