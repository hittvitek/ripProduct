package com.itCompany.ripProduct.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Ruff;

@Repository
public interface RuffRepository extends CrudRepository<Ruff, Long> {

    @Query(value = "SELECT * FROM RUFF_V WHERE ID = ?1", nativeQuery = true)
    Optional<Ruff> findByIdView(final Long id);

    @Query(value = "SELECT * FROM RUFF_V WHERE NAME = ?1", nativeQuery = true)
    Ruff findByNameView(final String name);

    @Query(value = "SELECT * FROM RUFF_V WHERE ARTICLE = ?1", nativeQuery = true)
    Ruff findByArticleView(final String article);

    @Query(value = "SELECT * FROM RUFF_V", nativeQuery = true)
    List<Ruff> findRuffView();

    @Modifying
    @Query(value = "DELETE FROM RUFFS_MATERIALS WHERE RUFF_ID = ?1 AND MATERIAL_ID = ?2", nativeQuery = true)
    void deleteRuffMaterial(final Long ruffId, final Long materialId);

}
