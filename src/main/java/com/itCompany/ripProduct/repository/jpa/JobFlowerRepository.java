package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.JobFlower;

@Repository
public interface JobFlowerRepository extends CrudRepository<JobFlower, Long>{

    JobFlower findByName(final String name);

    JobFlower findByArticle(final String article);

}
