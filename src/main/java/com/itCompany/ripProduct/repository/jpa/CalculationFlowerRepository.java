package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.CalculationFlower;

public interface CalculationFlowerRepository extends CrudRepository<CalculationFlower, Long>{

}
