package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.CalculationRuff;

public interface CalculationRuffRepository extends CrudRepository<CalculationRuff, Long>{

}
