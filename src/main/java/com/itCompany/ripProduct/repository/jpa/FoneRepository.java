package com.itCompany.ripProduct.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Fone;

@Repository
public interface FoneRepository extends CrudRepository<Fone, Long> {

    @Query(value = "SELECT * FROM FONE_V WHERE ID = ?1", nativeQuery = true)
    Optional<Fone> findByIdView(final Long id);

    @Query(value = "SELECT * FROM FONE_V WHERE NAME = ?1", nativeQuery = true)
    Fone findByNameView(final String name);

    @Query(value = "SELECT * FROM FONE_V WHERE ARTICLE = ?1", nativeQuery = true)
    Fone findByArticleView(final String article);

    @Query(value = "SELECT * FROM FONE_V", nativeQuery = true)
    List<Fone> findFoneView();

    @Modifying
    @Query(value = "DELETE FROM FONES_MATERIALS WHERE FONE_ID = ?1 AND MATERIALS_ID = ?2", nativeQuery = true)
    void deleteFoneMaterial(final Long foneId, final Long materialId);

    @Modifying
    @Query(value = "DELETE FROM FONES_RUFFS WHERE FONE_ID = ?1 AND RUFF_ID = ?2", nativeQuery = true)
    void deleteFoneRuff(final Long foneId, final Long ruffId);

}
