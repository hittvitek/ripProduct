package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.JobBraid;

@Repository
public interface JobBraidRepository extends CrudRepository<JobBraid, Long>{

    JobBraid findByName(final String name);

    JobBraid findByArticle(final String article);

}
