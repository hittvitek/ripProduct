package com.itCompany.ripProduct.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Calculation;

@Repository
public interface CalculationRepository extends CrudRepository<Calculation, Long> {

    @Query(value = "SELECT * FROM CALCULATION_V WHERE ID = ?1", nativeQuery = true)
    Optional<Calculation> findByIdView(final Long id);

    @Query(value = "SELECT * FROM CALCULATION_V WHERE NAME = ?1", nativeQuery = true)
    Calculation findByNameView(final String name);

    @Query(value = "SELECT * FROM CALCULATION_V WHERE ARTICLE = ?1", nativeQuery = true)
    Calculation findByArticleView(final String article);

    @Query(value = "SELECT * FROM CALCULATION_V", nativeQuery = true)
    List<Calculation> findAllCalculationView();

    @Modifying
    @Query(value = "DELETE FROM CALCULATIONS_MATERIALS WHERE CALCULATIONS_ID = ?1 AND MATERIALS_ID = ?2", nativeQuery = true)
    void deleteCalculationMaterial(final Long calculationId, final Long materialId);

    @Modifying
    @Query(value = "DELETE FROM CALCULATIONS_FLOWERS WHERE CALCULATIONS_ID = ?1 AND FLOWERS_ID = ?2", nativeQuery = true)
    void deleteCalculationFlower(final Long calculationId, final Long flowerId);

    @Modifying
    @Query(value = "DELETE FROM CALCULATIONS_RUFFS WHERE CALCULATIONS_ID = ?1 AND RUFFS_ID = ?2", nativeQuery = true)
    void deleteCalculationRuff(final Long calculationId, final Long ruffId);

//    long countNotRemoved();

//    @Query(value = RENTABELNOST_NOT_REMOVED , nativeQuery = true )
//    List<Rentabelnost> rentabelnostNotRemoved(final Date with, final Date by);

}
