package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.FlowerMaterial;

public interface FlowerMaterialRepository extends CrudRepository<FlowerMaterial, Long>{

}
