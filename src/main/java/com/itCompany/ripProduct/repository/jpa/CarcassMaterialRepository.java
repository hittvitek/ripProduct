package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.CarcassMaterial;

public interface CarcassMaterialRepository extends CrudRepository<CarcassMaterial, Long>{

}
