package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Currency;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Long>{

    Currency findByArticle(final String article);

}
