package com.itCompany.ripProduct.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.Flower;

@Repository
public interface FlowerRepository extends CrudRepository<Flower, Long> {

    @Query(value = "SELECT * FROM FLOWERS_V WHERE ID = ?1", nativeQuery = true)
    Optional<Flower> findByIdView(final Long id);

    @Query(value = "SELECT * FROM FLOWERS_V WHERE NAME = ?1", nativeQuery = true)
    Flower findByNameView(final String name);

    @Query(value = "SELECT * FROM FLOWERS_V WHERE ARTICLE = ?1", nativeQuery = true)
    Flower findByArticleView(final String article);

    @Query(value = "SELECT * FROM FLOWERS_V", nativeQuery = true)
    List<Flower> findFlowerView();

    @Modifying
    @Query(value = "DELETE FROM FLOWERS_MATERIALS WHERE FLOWERS_ID = ?1 AND MATERIALS_ID = ?2", nativeQuery = true)
    void deleteFlowerMaterial(final Long flowerId, final Long materialId);

}
