package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.CalculationMaterial;

public interface CalculationMaterialRepository extends CrudRepository<CalculationMaterial, Long>{

}
