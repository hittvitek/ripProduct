package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;

import com.itCompany.ripProduct.model.po.FoneMaterial;

public interface FoneMaterialRepository extends CrudRepository<FoneMaterial, Long>{

}
