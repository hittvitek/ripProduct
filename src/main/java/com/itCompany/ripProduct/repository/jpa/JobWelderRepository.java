package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.JobWelder;

@Repository
public interface JobWelderRepository extends CrudRepository<JobWelder, Long>{

    JobWelder findByName(final String name);

    JobWelder findByArticle(final String article);

}
