package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.JobProduct;

@Repository
public interface JobProductRepository extends CrudRepository<JobProduct, Long>{

    JobProduct findByName(final String name);

    JobProduct findByArticle(final String article);

}
