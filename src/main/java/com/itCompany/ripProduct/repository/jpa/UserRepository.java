package com.itCompany.ripProduct.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.po.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByLogin(String login);

}
