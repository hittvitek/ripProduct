package com.itCompany.ripProduct.repository.dao;

import java.sql.Date;
import java.util.List;

import com.itCompany.ripProduct.model.data.Rentabelnost;

public interface ReportDao {

    List<Rentabelnost> rentabelnostNotRemoved(final Date with, final Date by);

}
