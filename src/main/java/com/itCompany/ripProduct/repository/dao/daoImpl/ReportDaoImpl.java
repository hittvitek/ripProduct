package com.itCompany.ripProduct.repository.dao.daoImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.itCompany.ripProduct.model.data.Rentabelnost;
import com.itCompany.ripProduct.repository.dao.ReportDao;

@Repository
public class ReportDaoImpl extends JdbcDaoSupport implements ReportDao {

    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    final static String RENTABELNOST_NOT_REMOVED = "SELECT A.calculationArticle AS calculationArticle, A.calculationPrice AS calculationPrice, A.productPrice AS productPrice, A.RENTABELNOST AS RENTABELNOST, sum(A.QUANTITY) AS QUANTITY FROM ( " +
            "SELECT P.NAME AS calculationArticle, C.PRICE AS calculationPrice, P.PRICE AS productPrice, ROUND(((P.PRICE * 100) / C.PRICE - 100)::NUMERIC, 2) AS RENTABELNOST, SUM(OI.QUANTITY) AS QUANTITY " +
            "FROM PRODUCTS P " +
            "JOIN ORDERITEMS OI ON OI.PRODUCT_ID = P.ID " +
            "JOIN ORDERS O ON O.ID = OI.ORDER_ID " +
            "JOIN CALCULATION_V C ON C.ID = P.ID " +
            "WHERE REMOVED = 'FALSE' AND O.DATE_INV BETWEEN ? AND ? " + 
            "GROUP BY calculationArticle, calculationPrice, productPrice " +
        "UNION " +
            "SELECT P.NAME AS calculationArticle, C.PRICE AS calculationPrice, P.PRICE AS productPrice, ROUND(((P.PRICE * 100) / C.PRICE - 100)::NUMERIC, 2) AS RENTABELNOST, 0 AS QUANTITY " +
            "FROM PRODUCTS P " +
            "JOIN CALCULATION_V C ON C.ID = P.ID " +
            "WHERE REMOVED = 'FALSE' " +
            "GROUP BY calculationArticle, calculationPrice, productPrice " +
        ") A " +
        "GROUP BY A.calculationArticle, A.calculationPrice, A.productPrice, A.RENTABELNOST " +
        "ORDER BY A.calculationArticle";

    public List<Rentabelnost> rentabelnostNotRemoved(final Date from, final Date to) {
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(RENTABELNOST_NOT_REMOVED, new Object[] { from , to});
        logger.info(RENTABELNOST_NOT_REMOVED);
        List<Rentabelnost> result = new ArrayList<Rentabelnost>();
        Rentabelnost rentabelnost = null;
        for (Map<String, Object> row : rows) {
            rentabelnost = new Rentabelnost((String)row.get("calculationArticle"),
                Double.parseDouble(row.get("calculationPrice").toString()),
                Integer.parseInt(row.get("productPrice").toString()),
                Double.parseDouble(row.get("rentabelnost").toString()),
                Integer.parseInt(row.get("quantity").toString()));
            result.add(rentabelnost);
            rentabelnost = null;
        }
        return result;
    }

}
