package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.JobProductFace;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.service.JobProductService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class JobProductController {

    private static final Logger log = LoggerFactory.getLogger(JobProductController.class);
    JobProductFace jobProductFace;
    JobProductService jobProductService;

    @GetMapping("/jobproducts")
    public String showJobJobProducts(Model model) {
        model.addAttribute("jobproducts", jobProductService.getJobProducts());
        log.info("-= showJobProductList =-");
        return "jobproducts";
    }

    @GetMapping("/addjobproduct")
    public String createJobProduct(Model model) {
        model.addAttribute("jobproduct", new JobDto());
        return "add-jobproduct";
    }

    @PostMapping(value = "/addjobproduct")
    public String createJobProduct(@ModelAttribute("jobproduct") @Valid JobDto jobDto,
                                BindingResult errors) {
        log.info("begin jobproduct add");
        if (errors.hasErrors()) {
            return "add-jobproduct";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobProductFace.save(jobDto);
        log.info(jobDto.toString());
        log.info("end jobproduct add");
        return "redirect:/jobproducts";
    }

    @GetMapping("/editjobproduct/{id}")
    public String showJobProduct(@PathVariable("id") long id, Model model) {
        JobDto jobDto = jobProductFace.getJobDtoById(id);
        model.addAttribute("jobproduct", jobDto);
        return "/update-jobproduct";
    }

    @PostMapping("updatejobproduct/{id}")
    public String updateJobProduct(@PathVariable("id") long id,
                                @ModelAttribute("jobproduct") @Valid final JobDto jobDto,
                                BindingResult result) {
        if (result.hasErrors()) {
            //productDto.setId(id);
            return "update-jobproduct";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobProductFace.save(jobDto);
        return "redirect:/jobproducts";
    }

    @GetMapping("/deletejobproduct/{id}")
    public String deleteJobProduct(@PathVariable("id") long id, Model model) {
        jobProductService.deleteJobProduct(id);
        return "redirect:/jobproducts";
    }

}
