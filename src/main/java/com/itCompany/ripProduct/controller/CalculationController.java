package com.itCompany.ripProduct.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.itCompany.ripProduct.enums.Composite;
import com.itCompany.ripProduct.face.CalculationFace;
import com.itCompany.ripProduct.face.FlowerFace;
import com.itCompany.ripProduct.face.FoneFace;
import com.itCompany.ripProduct.face.JobProductFace;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.face.RuffFace;
import com.itCompany.ripProduct.model.dto.CalculationDto;
import com.itCompany.ripProduct.model.dto.FlowerDto;
import com.itCompany.ripProduct.model.dto.FoneDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.JobProduct;
import com.itCompany.ripProduct.service.CalculationService;
import com.itCompany.ripProduct.service.EDocumentService;
import com.itCompany.ripProduct.service.JobProductService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
@RequestMapping("/calculation")
public class CalculationController {

    CalculationService calculationService;
    JobProductService jobProductService;
    JobProductFace jobProductFace;
    CalculationFace calculationFace;
    FoneFace foneFace;
    FlowerFace flowerFace;
    RuffFace ruffFace;
    MaterialFace materialFace;
    EDocumentService eDocumentService;

    @ModelAttribute("allFone")
    public List<FoneDto> populateFone() {
        return foneFace.getAllFoneDtoList();
    }

    @ModelAttribute("allJob")
    public List<JobProduct> populateJobProduct() {
        return jobProductService.getJobProducts();
    }

    @ModelAttribute("allFlower")
    public List<FlowerDto> populateFlowers() {
        return flowerFace.getAllFlowerDtoList();
    }

    @ModelAttribute("allRuff")
    public List<RuffDto> populateRuffs() {
        return ruffFace.getAllRuffDtoList();
    }

    @ModelAttribute("allMaterial")
    public List<MaterialDto> populateMaterials() {
        return materialFace.getAllMaterialDtoList();
    }

    @GetMapping("/list")
    public String showCalculation(final Model model) {
        model.addAttribute("calculations", calculationFace.getAllCalculationDtoListView());
        log.info("-= showCalculationList =-");
        return "/calculation/list";
    }

    @GetMapping("/new")
    public String createCalculation(final Model model) {
        model.addAttribute("calculation", new CalculationDto());
        return "/calculation/new";
    }

    @PostMapping(value = "/new")
    public String createCalculation(@ModelAttribute("calculation") @Valid final CalculationDto calculationDto,
            final BindingResult errors, final ModelMap model) {
        log.info("begin calculation add");
        if (errors.hasErrors()) {
            return "/calculation/new";
        }
        // generic throwable errors.rejectValue("field", "", "Field not ..");
        calculationFace.save(calculationDto);
        model.clear();
        log.info(calculationDto.toString());
        log.info("end calculation add");
        return "redirect:/calculation/list";
    }

    @GetMapping(value = "/edit/{id}")
    public String showCalculation(@PathVariable("id") final Long id, Model model) {
        model.addAttribute("calculation", calculationFace.getCalculationDto(id));
        return "/calculation/edit";
    }

    @PostMapping("/edit")
    public String updateCalculation(@ModelAttribute("calculation") @Valid final CalculationDto calculationDto,
            BindingResult result) {
        if (result.hasErrors()) {
            return "/calculation/edit";
        }
        // generic throwable errors.rejectValue("field", "", "Field not ..");
        calculationFace.save(calculationDto);
        return "redirect:/calculation/list";
    }

    @GetMapping("/delete/{id}")
    public String deleteCalculation(@PathVariable("id") long id, Model model) {
        calculationService.deleteCalculation(id);
        return "redirect:/calculation/list";
    }

    @GetMapping(value = "/reportXLSX/listSold")
    public ResponseEntity<?> reportXLSXListSold(
            @RequestParam(name = "dateFrom", required = true) final Date from,
            @RequestParam(name = "dateTo", required = true) final Date to,
            Model model,
            HttpServletRequest http) throws IOException {
        //TODO DateFrom and DateTo validate dateTo>=dateFrom 
//        if (http.getParameterMap().containsKey("errorСollisionDate")) {
//            model.addAttribute("errorСollisionDate", true);
//            return "/calculation/list";
//        }
        eDocumentService.reportTableSold(from, to);
        log.info(new StringBuilder("printTableSold [dateFrom = ").append(from).append(" dateTo = ").append(to)
            .append("]").toString());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Transactional
    @PostMapping(value = "/reportXLSX/listCalculation")
    public ResponseEntity<?> reportXLSXListCalculation(
            @RequestParam(name = "ids[]", required = true) final long[] ids,
            @RequestParam(name = "printPrice", required = true, defaultValue = "true") final boolean printPrice) throws IOException {
        Map<Composite, Map<String, Object>> maps = calculationFace.getAllCalculationView();
        eDocumentService.reportTableListCalculation(printPrice, maps, ids);
        log.info(new StringBuilder("listCalculation list = { listIdentity = ").append(" }")
                .toString());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/recalculatePrice")
    public void recalculatePricies() {
        materialFace.recalculate();
    }

    @GetMapping(value = "/fone/{article}")
    public ResponseEntity<?> foneArticle(@PathVariable String article) {
        return new ResponseEntity<>(foneFace.getFoneDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/flower/{article}")
    public ResponseEntity<?> flowerArticle(@PathVariable String article) {
        return new ResponseEntity<>(flowerFace.getJsonFlowerDto(article), HttpStatus.OK);
    }

    @GetMapping(value = "/ruff/{article}")
    public ResponseEntity<?> ruffArticle(@PathVariable String article) {
        return new ResponseEntity<>(ruffFace.getRuffDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/material/{article}")
    public ResponseEntity<?> materialArticle(@PathVariable String article) {
        return new ResponseEntity<>(materialFace.getMaterialDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/job/{article}")
    public ResponseEntity<?> jobCalculationArticle(@PathVariable String article) {
        return new ResponseEntity<>(jobProductFace.getJsonJobProductDto(article), HttpStatus.OK);
    }

    @GetMapping("/{calculationId}/delete/material/{materialId}")
    public ResponseEntity<?> deleteCalculationMaterial(@PathVariable("calculationId") Long calculationId,
            @PathVariable("materialId") Long materialId, Model model) {
        calculationService.deleteCalculationMaterial(calculationId, materialId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{calculationId}/delete/ruff/{ruffId}")
    public ResponseEntity<?> deleteCalculationRuff(@PathVariable("calculationId") Long calculationId,
            @PathVariable("ruffId") Long ruffId, Model model) {
        calculationService.deleteCalculationRuff(calculationId, ruffId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{calculationId}/delete/flower/{flowerId}")
    public ResponseEntity<?> deleteCalculationFlower(@PathVariable("calculationId") Long calculationId,
            @PathVariable("flowerId") Long flowerId, Model model) {
        calculationService.deleteCalculationFlower(calculationId, flowerId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
