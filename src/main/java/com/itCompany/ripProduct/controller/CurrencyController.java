package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.CurrencyFace;
import com.itCompany.ripProduct.model.dto.CurrencyDto;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
public class CurrencyController {

    CurrencyFace currencyFace;
//    CurrencyService currencyService;

    @GetMapping("/currencies")
    public String showCurrencys(Model model) {
        model.addAttribute("currencies", currencyFace.getAllCurrencyDto());
        log.info("-= showCurrenciesList =-");
        return "currencies";
    }

    @GetMapping("/addcurrency")
    public String createCurrency(Model model) {
        model.addAttribute("currency", new CurrencyDto());
        return "add-currency";
    }

    @PostMapping(value = "/addcurrency")
    public String createCurrency(@ModelAttribute("currency") @Valid CurrencyDto currencyDto,
                                BindingResult errors) {
        log.info("begin currency add");
        if (errors.hasErrors()) {
            return "add-currency";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        currencyFace.save(currencyDto);
        log.info(currencyDto.toString());
        log.info("end currency add");
        return "redirect:/currencies";
    }

    @GetMapping("/editcurrency/{id}")
    public String showCurrency(@PathVariable("id") long id, Model model) {
        CurrencyDto currencyDto = currencyFace.getCurrencyDtoById(id);
        model.addAttribute("currency", currencyDto);
        return "/update-currency";
    }

    @PostMapping("updatecurrency/{id}")
    public String updateCurrency(@PathVariable("id") long id,
                                @ModelAttribute("currency") @Valid final CurrencyDto currencyDto,
                                BindingResult result) {
        if (result.hasErrors()) {
            return "update-currency";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        currencyFace.save(currencyDto);
        return "redirect:/currencies";
    }

    @GetMapping("/deletecurrency/{id}")
    public String deleteCurrency(@PathVariable("id") Long id, Model model) {
        currencyFace.delete(id);
        return "redirect:/currencies";
    }

}
