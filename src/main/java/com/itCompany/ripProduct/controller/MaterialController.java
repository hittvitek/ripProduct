package com.itCompany.ripProduct.controller;

import java.io.IOException;
import java.io.StringWriter;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.service.MaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
public class MaterialController {

    MaterialFace materialFace; 
    MaterialService materialService;

    @GetMapping("/materials")
    public String showMaterials(Model model) {
        model.addAttribute("materials", materialService.getMaterials());
        log.info("-= showMaterialList =-");
        return "materials";
    }

    @GetMapping("/addmaterial")
    public String createMaterial(Model model) {
        model.addAttribute("material", new MaterialDto());
        return "add-material";
    }

    @PostMapping(value = "/addmaterial")
    public String createMaterial(@ModelAttribute("material") @Valid MaterialDto materialDto, BindingResult errors) {
        log.info("begin material add");
        if (errors.hasErrors()) {
            return "add-material";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        materialFace.save(materialDto);
        log.info(materialDto.toString());
        log.info("end material add");
        return "redirect:/materials";
    }

    @GetMapping("/editmaterial/{id}")
    public String showMaterial(@PathVariable("id") long id, Model model) {
        model.addAttribute("material", materialFace.getMaterialDtoById(id));
        return "/update-material";
    }

    @PostMapping("/updatematerial")
    public String updateMaterial(@ModelAttribute("material") @Valid final MaterialDto materialDto, BindingResult result) {
        if (result.hasErrors()) {
            //materialDto.setId(id);
            return "update-material";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        materialFace.save(materialDto);
        return "redirect:/materials";
    }

    @GetMapping("/deletematerial/{id}")
    public String deleteMaterial(@PathVariable("id") long id, Model model) {
        materialFace.deleteMaterial(id);
        return "redirect:/materials";
    }

    @GetMapping("/material/fill")
    @ResponseBody
    public String fillOptionMaterials() throws JsonGenerationException, JsonMappingException, IOException {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(writer, materialFace.getAllMaterialDtoMap());
        return writer.toString();
    }

}
