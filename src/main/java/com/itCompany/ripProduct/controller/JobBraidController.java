package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.JobBraidFace;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.service.JobBraidService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
//@RequestMapping(name = "/jobBraid")
public class JobBraidController {

    private static final Logger log = LoggerFactory.getLogger(JobBraidController.class);
    JobBraidFace jobBraidFace;
    JobBraidService jobBraidService;

    @GetMapping("/jobbraids")
    public String showJobJobBraids(Model model) {
        model.addAttribute("jobbraids", jobBraidService.getJobBraids());
        log.info("-= showJobBraidList =-");
        return "jobbraids";
    }

    @GetMapping("/addjobbraid")
    public String createJobBraid(Model model) {
        model.addAttribute("jobbraid", new JobDto());
        return "add-jobbraid";
    }

    @PostMapping(value = "/addjobbraid")
    public String createJobBraid(@ModelAttribute("jobbraid") @Valid JobDto jobDto,
                                  BindingResult errors) {
        log.info("begin jobbraid add");
        if (errors.hasErrors()) {
            return "add-jobbraid";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobBraidFace.save(jobDto);
        log.info(jobDto.toString());
        log.info("end jobbraid add");
        return "redirect:/jobbraids";
    }

    @GetMapping("/editjobbraid/{id}")
    public String showJobBraid(@PathVariable("id") long id, Model model) {
        JobDto jobDto = jobBraidFace.getJobDtoById(id);
        model.addAttribute("jobbraid", jobDto);
        return "/update-jobbraid";
    }

    @PostMapping("updatejobbraid/{id}")
    public String updateJobBraid(@PathVariable("id") long id,
                                  @ModelAttribute("jobbraid") @Valid final JobDto jobDto,
                                  BindingResult result) {
        if (result.hasErrors()) {
            //braidDto.setId(id);
            return "update-jobbraid";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobBraidFace.save(jobDto);
        return "redirect:/jobbraids";
    }

    @GetMapping("/deletejobbraid/{id}")
    public String deleteJobBraid(@PathVariable("id") long id, Model model) {
        jobBraidService.deleteJobBraid(id);
        return "redirect:/jobbraids";
    }

}
