package com.itCompany.ripProduct.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itCompany.ripProduct.face.FlowerFace;
import com.itCompany.ripProduct.face.JobFlowerFace;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.model.dto.FlowerDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.JobFlower;
import com.itCompany.ripProduct.service.FlowerService;
import com.itCompany.ripProduct.service.JobFlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class FlowerController {

    private static final Logger log = LoggerFactory.getLogger(FlowerController.class);
    JobFlowerService jobFlowerService;
    JobFlowerFace jobFlowerFace;
    MaterialFace materialFace;
    FlowerService flowerService;
    FlowerFace flowerFace;

    @ModelAttribute("allJobflower")
    public List<JobFlower> populateJobFlowers(){
        return jobFlowerService.getJobFlowers();
    }

    @ModelAttribute("allMaterial")
    public List<MaterialDto> populateMaterials(){
        return materialFace.getAllMaterialDtoList();
    }

    @GetMapping("/flowers")
    public String showFlowers(final Model model) {
        model.addAttribute("flowers", flowerService.getFlowers());
        log.info("-= showFlowerList =-");
        return "flowers";
    }
    @GetMapping("/addflower")
    public String createFlower(final Model model) {
        model.addAttribute("flower", new FlowerDto());
        return "add-flower";
    }

    @PostMapping(value = "/addflower")
    public String createFlower(@ModelAttribute("flower") @Valid final FlowerDto flowerDto, final BindingResult errors, final ModelMap model) {
        log.info("begin flower add");
        if (errors.hasErrors()) {
            return "add-flower";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        flowerFace.save(flowerDto);
        model.clear();
        log.info(flowerDto.toString());
        log.info("end flower add");
        return "redirect:/flowers";
    }

    @GetMapping(value = "/editflower/{id}")
    public String showFlower(@PathVariable("id") final Long id, Model model) {
        model.addAttribute("flower", flowerFace.getFlowerDto(id));
        return "/update-flower";
    }

    @PostMapping("/updateflower")
    public String updateFlower(@ModelAttribute("flower") @Valid final FlowerDto flowerDto, BindingResult result) {
        if (result.hasErrors()) {
            return "update-flower";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        flowerFace.save(flowerDto);
        return "redirect:/flowers";
    }

    @GetMapping("/deleteflower/{id}")
    public String deleteFlower(@PathVariable("id") long id, Model model) {
        flowerService.deleteFlower(id);
        return "redirect:/flowers";
    }

    @GetMapping(value = "/flower/material/{article}")
    public ResponseEntity<?> materialArticle(@PathVariable String article) {
        return new ResponseEntity<>(materialFace.getMaterialDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/flower/jobFlower/{article}")
    public ResponseEntity<?> jobFlowerArticle(@PathVariable String article) {
        return new ResponseEntity<>(jobFlowerFace.getJsonJobFlower(article), HttpStatus.OK);
    }

    @GetMapping("/flower/{flowerId}/delete/material/{materialId}")
    public ResponseEntity<?> deleteFlowerMaterial(@PathVariable("flowerId") Long flowerId, @PathVariable("materialId") Long materialId, Model model) {
        flowerService.deleteFlowerMaterial(flowerId, materialId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/flower/fill")
    @ResponseBody
    public String fillOptionFlowers() throws JsonGenerationException, JsonMappingException, IOException {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(writer, flowerFace.getAllFlowerDtoMap());
        return writer.toString();
    }

}
