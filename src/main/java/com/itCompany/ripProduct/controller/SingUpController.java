package com.itCompany.ripProduct.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.model.dto.UserDto;
import com.itCompany.ripProduct.service.SingUpService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
public class SingUpController {

    SingUpService singUpService;

    @GetMapping("/singUp")
    public String getSingUp(final Model model) {
        model.addAttribute("userDto", new UserDto());
        return "singUp";
    }
    
    @PostMapping("/singUp")
    public String singUp(UserDto userDto) {
        singUpService.singUp(userDto);
        return "redirect:/login";
    }
}
