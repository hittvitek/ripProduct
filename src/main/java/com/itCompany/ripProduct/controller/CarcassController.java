package com.itCompany.ripProduct.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.CarcassFace;
import com.itCompany.ripProduct.face.JobWelderFace;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.model.dto.CarcassDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.JobWelder;
import com.itCompany.ripProduct.service.CarcassService;
import com.itCompany.ripProduct.service.JobWelderService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class CarcassController {

    private static final Logger log = LoggerFactory.getLogger(CarcassController.class);
    JobWelderService jobWelderService;
    JobWelderFace jobWelderFace;
    MaterialFace materialFace;
    CarcassService carcassService;
    CarcassFace carcassFace;

    @ModelAttribute("allJobwelder")
    public List<JobWelder> populateJobWelder(){
        return jobWelderService.getJobWelders();
    }

    @ModelAttribute("allMaterial")
    public List<MaterialDto> populateMaterials(){
        return materialFace.getAllMaterialDtoList();
    }

    @GetMapping("/carcass")
    public String showCarcass(final Model model) {
        model.addAttribute("carcass", carcassService.getCarcass());
        log.info("-= showCarcassList =-");
        return "carcass";
    }
    @GetMapping("/addcarcass")
    public String createCarcass(final Model model) {
        model.addAttribute("carcass", new CarcassDto());
        return "add-carcass";
    }

    @PostMapping(value = "/addcarcass")
    public String createCarcass(@ModelAttribute("carcass") @Valid final CarcassDto carcassDto, final BindingResult errors, final ModelMap model) {
        log.info("begin carcass add");
        if (errors.hasErrors()) {
            return "add-carcass";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        carcassFace.save(carcassDto);
        model.clear();
        log.info(carcassDto.toString());
        log.info("end carcass add");
        return "redirect:/carcass";
    }

    @GetMapping(value = "/editcarcass/{id}")
    public String showCarcass(@PathVariable("id") final Long id, Model model) {
        model.addAttribute("carcass", carcassFace.getCarcassDto(id));
        return "/update-carcass";
    }

    @PostMapping("/updatecarcass")
    public String updateCarcass(@ModelAttribute("carcass") @Valid final CarcassDto carcassDto, BindingResult result) {
        if (result.hasErrors()) {
            return "update-carcass";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        carcassFace.save(carcassDto);
        return "redirect:/carcass";
    }

    @GetMapping("/deletecarcass/{id}")
    public String deleteCarcass(@PathVariable("id") long id, Model model) {
        carcassService.deleteCarcass(id);
        return "redirect:/carcass";
    }

    @GetMapping(value = "/carcass/material/{article}")
    public ResponseEntity<?> materialArticle(@PathVariable String article) {
        return new ResponseEntity<>(materialFace.getMaterialDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/carcass/jobWelder/{article}")
    public ResponseEntity<?> jobCarcassArticle(@PathVariable String article) {
        return new ResponseEntity<>(jobWelderFace.getJsonJobWelder(article), HttpStatus.OK);
    }

    @GetMapping("/carcass/{carcassId}/delete/material/{materialId}")
    public ResponseEntity<?> deleteCarcassMaterial(@PathVariable("carcassId") Long carcassId, @PathVariable("materialId") Long materialId, Model model) {
        carcassService.deleteCarcassMaterial(carcassId, materialId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
