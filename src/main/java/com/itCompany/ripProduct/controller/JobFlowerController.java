package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.JobFlowerFace;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.service.JobFlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
//@RequestMapping(name = "/jobFlower")
public class JobFlowerController {

    private static final Logger log = LoggerFactory.getLogger(JobFlowerController.class);
    JobFlowerFace jobFlowerFace;
    JobFlowerService jobFlowerService;

    @GetMapping("/jobflowers")
    public String showJobJobFlowers(Model model) {
        model.addAttribute("jobflowers", jobFlowerService.getJobFlowers());
        log.info("-= showJobFlowerList =-");
        return "jobflowers";
    }

    @GetMapping("/addjobflower")
    public String createJobFlower(Model model) {
        model.addAttribute("jobflower", new JobDto());
        return "add-jobflower";
    }

    @PostMapping(value = "/addjobflower")
    public String createJobFlower(@ModelAttribute("jobflower") @Valid JobDto jobDto,
                                  BindingResult errors) {
        log.info("begin jobflower add");
        if (errors.hasErrors()) {
            return "add-jobflower";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobFlowerFace.save(jobDto);
        log.info(jobDto.toString());
        log.info("end jobflower add");
        return "redirect:/jobflowers";
    }

    @GetMapping("/editjobflower/{id}")
    public String showJobFlower(@PathVariable("id") long id, Model model) {
        JobDto jobDto = jobFlowerFace.getJobDtoById(id);
        model.addAttribute("jobflower", jobDto);
        return "/update-jobflower";
    }

    @PostMapping("updatejobflower/{id}")
    public String updateJobFlower(@PathVariable("id") long id,
                                  @ModelAttribute("jobflower") @Valid final JobDto jobDto,
                                  BindingResult result) {
        if (result.hasErrors()) {
            //flowerDto.setId(id);
            return "update-jobflower";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobFlowerFace.save(jobDto);
        return "redirect:/jobflowers";
    }

    @GetMapping("/deletejobflower/{id}")
    public String deleteJobFlower(@PathVariable("id") long id, Model model) {
        jobFlowerService.deleteJobFlower(id);
        return "redirect:/jobflowers";
    }

}
