package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.JobWelderFace;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.service.JobWelderService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
//@RequestMapping(name = "/jobWelder")
public class JobWelderController {

    private static final Logger log = LoggerFactory.getLogger(JobWelderController.class);
    JobWelderFace jobWelderFace;
    JobWelderService jobWelderService;

    @GetMapping("/jobwelders")
    public String showJobJobWelders(Model model) {
        model.addAttribute("jobwelders", jobWelderService.getJobWelders());
        log.info("-= showJobWelderList =-");
        return "jobwelders";
    }

    @GetMapping("/addjobwelder")
    public String createJobWelder(Model model) {
        model.addAttribute("jobwelder", new JobDto());
        return "add-jobwelder";
    }

    @PostMapping(value = "/addjobwelder")
    public String createJobWelder(@ModelAttribute("jobwelder") @Valid JobDto jobDto,
                                  BindingResult errors) {
        log.info("begin jobwelder add");
        if (errors.hasErrors()) {
            return "add-jobwelder";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobWelderFace.save(jobDto);
        log.info(jobDto.toString());
        log.info("end jobwelder add");
        return "redirect:/jobwelders";
    }

    @GetMapping("/editjobwelder/{id}")
    public String showJobWelder(@PathVariable("id") long id, Model model) {
        JobDto jobDto = jobWelderFace.getJobDtoById(id);
        model.addAttribute("jobwelder", jobDto);
        return "/update-jobwelder";
    }

    @PostMapping("updatejobwelder/{id}")
    public String updateJobWelder(@PathVariable("id") long id,
                                  @ModelAttribute("jobwelder") @Valid final JobDto jobDto,
                                  BindingResult result) {
        if (result.hasErrors()) {
            //welderDto.setId(id);
            return "update-jobwelder";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobWelderFace.save(jobDto);
        return "redirect:/jobwelders";
    }

    @GetMapping("/deletejobwelder/{id}")
    public String deleteJobWelder(@PathVariable("id") long id, Model model) {
        jobWelderService.deleteJobWelder(id);
        return "redirect:/jobwelders";
    }

}
