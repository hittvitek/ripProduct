package com.itCompany.ripProduct.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itCompany.ripProduct.face.JobRuffFace;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.face.RuffFace;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.JobRuff;
import com.itCompany.ripProduct.service.JobRuffService;
import com.itCompany.ripProduct.service.RuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
public class RuffController {

//    private static final Logger log = LoggerFactory.getLogger(RuffController.class);
    JobRuffService jobRuffService;
    JobRuffFace jobRuffFace;
    MaterialFace materialFace;
    RuffService ruffService;
    RuffFace ruffFace;

    @ModelAttribute("allJobruff")
    public List<JobRuff> populateJobRuffs(){
        return jobRuffService.getJobRuffs();
    }

    @ModelAttribute("allMaterial")
    public List<MaterialDto> populateMaterials(){
        return materialFace.getAllMaterialDtoList();
    }

    @GetMapping("/ruffs")
    public String showRuffs(final Model model) {
        model.addAttribute("ruffs", ruffService.getRuffs());
        log.info("-= showRuffList =-");
        return "ruffs";
    }
    @GetMapping("/addruff")
    public String createRuff(final Model model) {
        model.addAttribute("ruff", new RuffDto());
        return "add-ruff";
    }

    @PostMapping(value = "/addruff")
    public String createRuff(@ModelAttribute("ruff") @Valid final RuffDto ruffDto, final BindingResult errors, final ModelMap model) {
        log.info("begin ruff add");
        if (errors.hasErrors()) {
            return "add-ruff";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        ruffFace.save(ruffDto);
        model.clear();
        log.info(ruffDto.toString());
        log.info("end ruff add");
        return "redirect:/ruffs";
    }

    @GetMapping(value = "/editruff/{id}")
    public String showRuff(@PathVariable("id") final Long id, Model model) {
        model.addAttribute("ruff", ruffFace.getRuffDto(id));
        return "/update-ruff";
    }

    @PostMapping("/updateruff")
    public String updateRuff(@ModelAttribute("ruff") @Valid final RuffDto ruffDto, BindingResult result) {
        if (result.hasErrors()) {
            return "update-ruff";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        ruffFace.save(ruffDto);
        return "redirect:/ruffs";
    }

    @GetMapping("/deleteruff/{id}")
    public String deleteRuff(@PathVariable("id") long id, Model model) {
        ruffService.deleteRuff(id);
        return "redirect:/ruffs";
    }

    @GetMapping(value = "/ruff/material/{article}")
    public ResponseEntity<?> materialArticle(@PathVariable String article) {
        return new ResponseEntity<>(materialFace.getMaterialDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/ruff/jobRuff/{article}")
    public ResponseEntity<?> jobRuffArticle(@PathVariable String article) {
        return new ResponseEntity<>(jobRuffFace.getJsonJobRuff(article), HttpStatus.OK);
    }

    @GetMapping("/ruff/{ruffId}/delete/material/{materialId}")
    public ResponseEntity<?> deleteRuffMaterial(@PathVariable("ruffId") Long ruffId, @PathVariable("materialId") Long materialId, Model model) {
        ruffService.deleteRuffMaterial(ruffId, materialId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/ruff/fill")
    @ResponseBody
    public String fillOptionRuffs() throws JsonGenerationException, JsonMappingException, IOException {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(writer, ruffFace.getAllRuffDtoMap());
        return writer.toString();
    }

}
