package com.itCompany.ripProduct.controller;

import java.util.Objects;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping({"/", "/index"})
    public String index(Authentication authentication) {
        if (Objects.isNull(authentication)) {
            return "redirect:/login";
        }
        return "index";
    }

}
