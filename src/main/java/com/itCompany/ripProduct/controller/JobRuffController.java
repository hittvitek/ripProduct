package com.itCompany.ripProduct.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.JobRuffFace;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.service.JobRuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class JobRuffController {

    private static final Logger log = LoggerFactory.getLogger(JobRuffController.class);
    JobRuffFace jobRuffFace;
    JobRuffService jobRuffService;

    @GetMapping("/jobruffs")
    public String showJobJobRuffs(Model model) {
        model.addAttribute("jobruffs", jobRuffService.getJobRuffs());
        log.info("-= showJobRuffList =-");
        return "jobruffs";
    }

    @GetMapping("/addjobruff")
    public String createJobRuff(Model model) {
        model.addAttribute("jobruff", new JobDto());
        return "add-jobruff";
    }

    @PostMapping(value = "/addjobruff")
    public String createJobRuff(@ModelAttribute("jobruff") @Valid JobDto jobDto,
                                BindingResult errors) {
        log.info("begin jobruff add");
        if (errors.hasErrors()) {
            return "add-jobruff";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobRuffFace.save(jobDto);
        log.info(jobDto.toString());
        log.info("end jobruff add");
        return "redirect:/jobruffs";
    }

    @GetMapping("/editjobruff/{id}")
    public String showJobRuff(@PathVariable("id") long id, Model model) {
        JobDto jobDto = jobRuffFace.getJobDtoById(id);
        model.addAttribute("jobruff", jobDto);
        return "/update-jobruff";
    }

    @PostMapping("updatejobruff/{id}")
    public String updateJobRuff(@PathVariable("id") long id,
                                @ModelAttribute("jobruff") @Valid final JobDto jobDto,
                                BindingResult result) {
        if (result.hasErrors()) {
            //ruffDto.setId(id);
            return "update-jobruff";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        jobRuffFace.save(jobDto);
        return "redirect:/jobruffs";
    }

    @GetMapping("/deletejobruff/{id}")
    public String deleteJobRuff(@PathVariable("id") long id, Model model) {
        jobRuffService.deleteJobRuff(id);
        return "redirect:/jobruffs";
    }

}
