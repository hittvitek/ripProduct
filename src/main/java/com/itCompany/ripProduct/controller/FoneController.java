package com.itCompany.ripProduct.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.itCompany.ripProduct.face.CarcassFace;
import com.itCompany.ripProduct.face.FoneFace;
import com.itCompany.ripProduct.face.JobBraidFace;
import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.face.RuffFace;
import com.itCompany.ripProduct.model.dto.CarcassDto;
import com.itCompany.ripProduct.model.dto.FoneDto;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.JobBraid;
import com.itCompany.ripProduct.service.FoneService;
import com.itCompany.ripProduct.service.JobBraidService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
public class FoneController {

    CarcassFace carcassFace;
    JobBraidService jobBraidService;
    JobBraidFace jobBraidFace;
    MaterialFace materialFace;
    FoneService foneService;
    FoneFace foneFace;
    RuffFace ruffFace;

    @ModelAttribute("allCarcass")
    public List<CarcassDto> populateCarcass(){
        return carcassFace.getAllCarcassDtoList();
    }

    @ModelAttribute("allJobbraid")
    public List<JobBraid> populateJobBraid(){
        return jobBraidService.getJobBraids();
    }

    @ModelAttribute("allRuff")
    public List<RuffDto> populateRuffs(){
        return ruffFace.getAllRuffDtoList();
    }

    @ModelAttribute("allMaterial")
    public List<MaterialDto> populateMaterials(){
        return materialFace.getAllMaterialDtoList();
    }

    @GetMapping("/fones")
    public String showFone(final Model model) {
        model.addAttribute("fones", foneService.getFones());
        log.info("-= showFoneList =-");
        return "fones";
    }

    @GetMapping("/addfone")
    public String createFone(final Model model) {
        model.addAttribute("fone", new FoneDto());
        return "add-fone";
    }

    @PostMapping(value = "/addfone")
    public String createFone(@ModelAttribute("fone") @Valid final FoneDto foneDto, final BindingResult errors, final ModelMap model) {
        log.info("begin fone add");
        if (errors.hasErrors()) {
            return "add-fone";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        foneFace.save(foneDto);
        model.clear();
        log.info(foneDto.toString());
        log.info("end fone add");
        return "redirect:/fones";
    }

    @GetMapping(value = "/editfone/{id}")
    public String showFone(@PathVariable("id") final Long id, Model model) {
        model.addAttribute("fone", foneFace.getFoneDto(id));
        return "/update-fone";
    }

    @PostMapping("/updatefone")
    public String updateFone(@ModelAttribute("fone") @Valid final FoneDto foneDto, BindingResult result) {
        if (result.hasErrors()) {
            return "update-fone";
        }
        //generic throwable errors.rejectValue("field", "", "Field not ..");
        foneFace.save(foneDto);
        return "redirect:/fones";
    }

    @GetMapping("/deletefone/{id}")
    public String deleteFone(@PathVariable("id") long id, Model model) {
        foneService.deleteFone(id);
        return "redirect:/fones";
    }

    @GetMapping(value = "/fone/carcass/{article}")
    public ResponseEntity<?> jobCarcassArticle(@PathVariable String article) {
        return new ResponseEntity<>(carcassFace.getJsonCarcassDto(article), HttpStatus.OK);
    }

    @GetMapping(value = "/fone/ruff/{article}")
    public ResponseEntity<?> ruffArticle(@PathVariable String article) {
        return new ResponseEntity<>(ruffFace.getRuffDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/fone/material/{article}")
    public ResponseEntity<?> materialArticle(@PathVariable String article) {
        return new ResponseEntity<>(materialFace.getMaterialDtoByArticle(article), HttpStatus.OK);
    }

    @GetMapping(value = "/fone/job/{article}")
    public ResponseEntity<?> jobFoneArticle(@PathVariable String article) {
        return new ResponseEntity<>(jobBraidFace.getJsonJobBraidDto(article), HttpStatus.OK);
    }

    @GetMapping("/fone/{foneId}/delete/material/{materialId}")
    public ResponseEntity<?> deleteFoneMaterial(@PathVariable("foneId") Long foneId, @PathVariable("materialId") Long materialId, Model model) {
        foneService.deleteFoneMaterial(foneId, materialId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/fone/{foneId}/delete/ruff/{ruffId}")
    public ResponseEntity<?> deleteFoneRuff(@PathVariable("foneId") Long foneId, @PathVariable("ruffId") Long ruffId, Model model) {
        foneService.deleteFoneRuff(foneId, ruffId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
