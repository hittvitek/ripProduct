package com.itCompany.ripProduct.util.eDocument;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Log4j2
@Component
public class TreeEDocument {

    /*private void tree(Workbook workbook, Sheet sheet, long idCalculate) {
        int rowCount = 2;
        calculationService = new CalculationServiceImpl();
        CarcassService carcassService = new runner.service.CarcassServiceImpl();
        RuffService ruffService = new RuffServiceImpl();
        FoneService foneService = new FoneServiceImpl();
        BouquetService bouquetService = new BouquetServiceImpl();
        FlowersService flowersService = new FlowersServiceImpl();
        JobProductService jobProductService = new JobProductServiceImpl();
        CommonService commonService = new CommonServiceImpl();
        System.out.println("Start");
        Date start = new Date();
        CalculationModel objects = calculationService.getCalculation(Long.valueOf(idCalculate));
        objects.setBouquets(calculationService.getCalculationBouquets(Long.valueOf(idCalculate)));
        objects.setFlowers(calculationService.getCalculationFlowers(Long.valueOf(idCalculate)));
        objects.setRuffs(calculationService.getCalculationRuffs(Long.valueOf(idCalculate)));
        objects.setMaterials(calculationService.getCalculationMaterials(Long.valueOf(idCalculate)));
        objects.setFone(foneService.getFone(objects.getFone().getId()));
        objects.setJobProduct(jobProductService.getJob(objects.getJobProduct().getId()));

        Row rowTable = sheet.createRow(rowCount);
        rowTable.setHeightInPoints(17.0F);

        rowTable = sheet.createRow(rowCount++);
        Cell node = rowTable.createCell(3);
        node.setCellValue("Артикул");
        node = rowTable.createCell(4);
        node.setCellValue("Количество");
        node = rowTable.createCell(5);
        node.setCellValue("Цена");
        node = rowTable.createCell(6);
        node.setCellValue("Метры");
        rowTable = sheet.createRow(rowCount++);
        Cell prdt = rowTable.createCell(0);
        prdt.setCellValue(objects.getArticle());
        prdt.setCellStyle(cellStyle(true, true, true));
        prdt = rowTable.createCell(1);
        prdt.setCellValue(calculationService.calculate(objects.getName()));

        rowTable = sheet.createRow(rowCount++);
        Cell fn = rowTable.createCell(0);
        fn.setCellValue("Фон");
        fn = rowTable.createCell(1);
        fn.setCellValue(objects.getFone().getArticle());
        fn.setCellStyle(cellStyle(true, true, true));
        fn = rowTable.createCell(2);
        fn.setCellValue(foneService.calculate(objects.getFone().getArticle()));

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        Cell crcss = rowTable.createCell(2);
        crcss.setCellValue(objects.getFone().getCarcass().getArticle());
        crcss.setCellStyle(cellStyle(true, true, true));
        crcss = rowTable.createCell(3);
        crcss.setCellValue(carcassService.calculate(objects.getFone().getCarcass().getArticle()));

        Cell mtrl = null;

        int size = objects.getFone().getCarcass().getMaterial().size();
        float rez = 0.0F;
        float quantityObj = 0.0F;
        for (int j = 0; j < size; j++) {
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, true);
            String value = ((MaterialModel) objects.getFone().getCarcass().getMaterial().get(j)).getArticle();
            mtrl = rowTable.createCell(3);
            mtrl.setCellStyle(cellStyle(true, true, false));
            mtrl.setCellValue(value);
            quantityObj = ((MaterialModel) objects.getFone().getCarcass().getMaterial().get(j)).getQuantity();

            mtrl = rowTable.createCell(4);
            mtrl.setCellStyle(cellStyle(false, true, false));
            mtrl.setCellValue(quantityObj);
            rez = quantityObj * commonService.calculate(value);

            mtrl = rowTable.createCell(5);
            mtrl.setCellStyle(cellStyle(false, true, true));
            mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
        }
        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, true);
        Cell job = rowTable.createCell(3);
        job.setCellStyle(cellStyle(true, true, false));
        job.setCellValue(objects.getFone().getCarcass().getJobWelder().getArticle());
        job = rowTable.createCell(4);
        job.setCellStyle(cellStyle(false, true, false));
        job = rowTable.createCell(5);
        job.setCellStyle(cellStyle(false, true, true));
        job.setCellValue(objects.getFone().getCarcass().getJobWelder().getPrice());

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, true);

        size = objects.getFone().getRuffs().size();
        for (int j = 0; j < size; j++) {
            rowTable = sheet.createRow(rowCount++);
            node = rowTable.createCell(1);
            node.setCellStyle(cellStyle(true, false, false));
            Cell rff = rowTable.createCell(2);
            rff.setCellValue(((RuffModel) objects.getFone().getRuffs().get(j)).getArticle());
            rff.setCellStyle(cellStyle(true, true, true));

            rff = rowTable.createCell(6);
            quantityObj = ((RuffModel) objects.getFone().getRuffs().get(j)).getQuantity();
            rff.setCellValue(quantityObj);

            rff = rowTable.createCell(3);
            rez = ruffService.calculate(((RuffModel) objects.getFone().getRuffs().get(j)).getArticle());
            rff.setCellValue(Float.parseFloat(commonService.realRub(rez * quantityObj)));

            int sizeFRM = ((RuffModel) objects.getFone().getRuffs().get(j)).getMaterial().size();
            for (int i = 0; i < sizeFRM; i++) {
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, true);
                String value = ((MaterialModel) ((RuffModel) objects.getFone().getRuffs().get(j)).getMaterial().get(i))
                        .getArticle();
                rff = rowTable.createCell(3);
                rff.setCellStyle(cellStyle(true, true, false));
                rff.setCellValue(value);
                quantityObj = ((MaterialModel) ((RuffModel) objects.getFone().getRuffs().get(j)).getMaterial().get(i))
                        .getQuantity();

                rff = rowTable.createCell(4);
                rff.setCellStyle(cellStyle(false, true, false));
                rff.setCellValue(quantityObj);
                rez = quantityObj * commonService.calculate(value);

                rff = rowTable.createCell(5);
                rff.setCellStyle(cellStyle(false, true, true));
                rff.setCellValue(Float.parseFloat(commonService.realRub(rez)));
            }
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, true);
            job = rowTable.createCell(3);
            job.setCellStyle(cellStyle(true, true, false));
            job.setCellValue(((RuffModel) objects.getFone().getRuffs().get(j)).getJobRuff().getArticle());
            job = rowTable.createCell(4);
            job.setCellStyle(cellStyle(false, true, false));
            job = rowTable.createCell(5);
            job.setCellStyle(cellStyle(false, true, true));
            job.setCellValue(((RuffModel) objects.getFone().getRuffs().get(j)).getJobRuff().getPrice());

            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, true);
        }

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        mtrl = rowTable.createCell(2);
        mtrl.setCellStyle(cellStyle(true, true, true));
        mtrl.setCellValue("Материалы");
        int sizeFMM = objects.getFone().getMaterials().size();
        for (int i = 0; i < sizeFMM; i++) {
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, true);
            String value = ((MaterialModel) objects.getFone().getMaterials().get(i)).getArticle();
            mtrl = rowTable.createCell(3);
            mtrl.setCellStyle(cellStyle(true, true, false));
            mtrl.setCellValue(value);
            quantityObj = ((MaterialModel) objects.getFone().getMaterials().get(i)).getQuantity();

            mtrl = rowTable.createCell(4);
            mtrl.setCellStyle(cellStyle(false, true, false));
            mtrl.setCellValue(quantityObj);
            rez = quantityObj * commonService.calculate(value);

            mtrl = rowTable.createCell(5);
            mtrl.setCellStyle(cellStyle(false, true, true));
            mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
        }
        rowTable = sheet.createRow(rowCount++);
        node = rowTable.createCell(1);
        node.setCellStyle(cellStyle(true, false, true));
        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        job = rowTable.createCell(2);
        job.setCellStyle(cellStyle(true, true, true));
        job.setCellValue("Работа");
        job = rowTable.createCell(3);
        float cost = objects.getFone().getJobBraid().getPrice();
        job.setCellValue(cost);

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        job = rowTable.createCell(3);
        job.setCellValue(objects.getFone().getJobBraid().getArticle());
        job.setCellStyle(cellStyle(true, true, false));
        job = rowTable.createCell(4);
        job.setCellStyle(cellStyle(false, true, false));
        job = rowTable.createCell(5);
        job.setCellStyle(cellStyle(false, true, true));
        job.setCellValue(cost);
        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);

        boolean titleBouquets = true;
        if ((objects.getBouquets() != null) && (!objects.getBouquets().isEmpty())) {
            int bouquetsSize = objects.getBouquets().size();
            boolean titleFlowers = true;
            float price = 0.0F;

            for (int i = 0; i < bouquetsSize; i++) {
                BouquetModel bouquetModel = bouquetService
                        .getBouquet(((BouquetModel) objects.getBouquets().get(i)).getName());
                rowTable = sheet.createRow(rowCount++);
                Cell bqt = rowTable.createCell(0);
                if (titleBouquets) {
                    bqt.setCellValue("Букеты");
                    titleBouquets = false;
                }
                bqt = rowTable.createCell(1);
                bqt.setCellValue(bouquetModel.getArticle());
                bqt.setCellStyle(cellStyle(true, true, true));
                price = bouquetService.calculate(bouquetModel.getName());
                if (((BouquetModel) objects.getBouquets().get(i)).getQuantity() > 1.0F) {
                    bqt = rowTable.createCell(4);
                    bqt.setCellValue(((BouquetModel) objects.getBouquets().get(i)).getQuantity());
                }
                bqt = rowTable.createCell(2);

                bqt.setCellValue("Не правильно считает");

                int flowersSize = bouquetModel.getFlowers().size();
                for (int j = 0; j < flowersSize; j++) {
                    FlowersModel flowersModel = flowersService
                            .getFlowersByArticle(((FlowersModel) bouquetModel.getFlowers().get(j)).getArticle());
                    rowTable = sheet.createRow(rowCount++);
                    Cell flwrs = rowTable.createCell(1);
                    nodeBorder(rowTable, false);
                    if (titleFlowers) {
                        flwrs.setCellValue("Цветок");
                        titleFlowers = false;
                    }
                    flwrs = rowTable.createCell(2);
                    flwrs.setCellValue(flowersModel.getArticle());
                    flwrs.setCellStyle(cellStyle(true, true, true));
                    flwrs = rowTable.createCell(3);
                    flwrs.setCellValue(flowersService.calculate(flowersModel.getArticle()));
                    if (((FlowersModel) bouquetModel.getFlowers().get(j)).getQuantity() > 1.0F) {
                        flwrs = rowTable.createCell(4);

                        flwrs.setCellValue("Вставить количество");
                    }
                    int materialsSize = flowersModel.getMaterial().size();
                    for (int k = 0; k < materialsSize; k++) {
                        MaterialModel materialModel = (MaterialModel) flowersModel.getMaterial().get(k);
                        rowTable = sheet.createRow(rowCount++);
                        nodeBorder(rowTable, true);
                        mtrl = rowTable.createCell(3);
                        mtrl.setCellStyle(cellStyle(true, true, false));
                        mtrl.setCellValue(materialModel.getArticle());

                        mtrl = rowTable.createCell(4);
                        mtrl.setCellStyle(cellStyle(false, true, false));

                        rez = commonService.calculate(materialModel.getArticle());
                        mtrl = rowTable.createCell(5);
                        mtrl.setCellStyle(cellStyle(false, true, true));
                        mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
                    }
                    rowTable = sheet.createRow(rowCount++);
                    nodeBorder(rowTable, true);
                    job = rowTable.createCell(3);
                    job.setCellStyle(cellStyle(true, true, false));
                    job.setCellValue(flowersModel.getJobFlower().getArticle());
                    job = rowTable.createCell(4);
                    job.setCellStyle(cellStyle(false, true, false));
                    job = rowTable.createCell(5);
                    job.setCellStyle(cellStyle(false, true, true));
                    job.setCellValue(flowersModel.getJobFlower().getPrice());

                    rowTable = sheet.createRow(rowCount++);
                    nodeBorder(rowTable, true);
                }

                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
                mtrl = rowTable.createCell(2);
                mtrl.setCellStyle(cellStyle(true, true, true));
                mtrl.setCellValue("Материалы");
                int materialsSize = bouquetModel.getMaterial().size();
                for (int k = 0; k < materialsSize; k++) {
                    MaterialModel materialModel = (MaterialModel) bouquetModel.getMaterial().get(k);
                    rowTable = sheet.createRow(rowCount++);
                    nodeBorder(rowTable, true);
                    mtrl = rowTable.createCell(3);
                    mtrl.setCellStyle(cellStyle(true, true, false));
                    mtrl.setCellValue(materialModel.getArticle());

                    mtrl = rowTable.createCell(4);
                    mtrl.setCellStyle(cellStyle(false, true, false));

                    rez = commonService.calculate(materialModel.getArticle());
                    mtrl = rowTable.createCell(5);
                    mtrl.setCellStyle(cellStyle(false, true, true));
                    mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
                }

                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, true);
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
                job = rowTable.createCell(2);
                job.setCellStyle(cellStyle(true, true, true));
                job.setCellValue("Работа");
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, true);
                job = rowTable.createCell(3);
                job.setCellStyle(cellStyle(true, true, false));
                job.setCellValue(bouquetModel.getJobFlower().getArticle());
                job = rowTable.createCell(4);
                job.setCellStyle(cellStyle(false, true, false));
                job = rowTable.createCell(5);
                job.setCellStyle(cellStyle(false, true, true));
                job.setCellValue(bouquetModel.getJobFlower().getPrice());

                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, true);
            }
        }

        int flowersSize = objects.getFlowers().size();
        boolean titleFlowers = true;
        for (int j = 0; j < flowersSize; j++) {
            FlowersModel flowersModel = flowersService
                    .getFlowersByArticle(((FlowersModel) objects.getFlowers().get(j)).getArticle());
            rowTable = sheet.createRow(rowCount++);
            if (titleFlowers) {
                Cell flwrs = rowTable.createCell(0);
                flwrs.setCellValue("Цветы");
                titleFlowers = false;
            }
            nodeBorder(rowTable, false);
            Cell flwrs = rowTable.createCell(1);
            flwrs.setCellValue(flowersModel.getArticle());
            flwrs.setCellStyle(cellStyle(true, true, true));
            flwrs = rowTable.createCell(2);
            flwrs.setCellValue(flowersService.calculate(flowersModel.getArticle()));
            float quantity = ((FlowersModel) objects.getFlowers().get(j)).getQuantity();
            if (quantity > 1.0F) {
                flwrs = rowTable.createCell(4);
                flwrs.setCellValue(quantity);
            }
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, false);
            mtrl = rowTable.createCell(2);
            mtrl.setCellValue("Материалы");
            mtrl.setCellStyle(cellStyle(true, true, true));

            int materialsSize = flowersModel.getMaterial().size();
            for (int k = 0; k < materialsSize; k++) {
                MaterialModel materialModel = (MaterialModel) flowersModel.getMaterial().get(k);
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
                mtrl = rowTable.createCell(3);
                mtrl.setCellStyle(cellStyle(true, true, false));
                mtrl.setCellValue(materialModel.getArticle());

                mtrl = rowTable.createCell(4);
                mtrl.setCellStyle(cellStyle(false, true, false));

                rez = commonService.calculate(materialModel.getArticle());
                mtrl = rowTable.createCell(5);
                mtrl.setCellStyle(cellStyle(false, true, true));
                mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
            }
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, false);
            job = rowTable.createCell(3);
            job.setCellStyle(cellStyle(true, true, false));
            job.setCellValue(flowersModel.getJobFlower().getArticle());
            job = rowTable.createCell(4);
            job.setCellStyle(cellStyle(false, true, false));
            job = rowTable.createCell(5);
            job.setCellStyle(cellStyle(false, true, true));
            job.setCellValue(flowersModel.getJobFlower().getPrice());

            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, false);
        }

        if ((objects.getRuffs() != null) && (!objects.getRuffs().isEmpty())) {
            size = objects.getRuffs().size();
            boolean titleRuffs = true;
            for (int j = 0; j < size; j++) {
                RuffModel ruffModel = ruffService.getRuffByArticle(((RuffModel) objects.getRuffs().get(j)).getArticle());
                float quantity = ((RuffModel) objects.getRuffs().get(j)).getQuantity();
                rez = ruffService.calculate(ruffModel.getArticle());

                rowTable = sheet.createRow(rowCount++);
                if (titleRuffs) {
                    Cell rff = rowTable.createCell(0);
                    rff.setCellValue("Ершы");
                    titleRuffs = false;
                }
                nodeBorder(rowTable, false);
                Cell rff = rowTable.createCell(1);
                rff.setCellValue(ruffModel.getArticle());
                rff.setCellStyle(cellStyle(true, true, true));

                rff = rowTable.createCell(2);
                rff.setCellValue(Float.parseFloat(commonService.realRub(rez * quantity)));

                if (quantity > 0.0F) {
                    rff = rowTable.createCell(6);
                    rff.setCellValue(quantity);
                }
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
                mtrl = rowTable.createCell(2);
                mtrl.setCellValue("Материалы");
                mtrl.setCellStyle(cellStyle(true, true, true));

                int sizeRM = ruffModel.getMaterial().size();
                for (int i = 0; i < sizeRM; i++) {
                    rowTable = sheet.createRow(rowCount++);
                    nodeBorder(rowTable, false);
                    String value = ((MaterialModel) ruffModel.getMaterial().get(i)).getArticle();
                    quantity = ((MaterialModel) ruffModel.getMaterial().get(i)).getQuantity();
                    rez = quantity * commonService.calculate(value);

                    mtrl = rowTable.createCell(3);
                    mtrl.setCellStyle(cellStyle(true, true, false));
                    mtrl.setCellValue(value);

                    mtrl = rowTable.createCell(4);
                    mtrl.setCellStyle(cellStyle(false, true, false));
                    mtrl.setCellValue(quantity);

                    mtrl = rowTable.createCell(5);
                    mtrl.setCellStyle(cellStyle(false, true, true));
                    mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
                }
                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
                job = rowTable.createCell(3);
                job.setCellStyle(cellStyle(true, true, false));
                job.setCellValue(ruffModel.getJobRuff().getArticle());
                job = rowTable.createCell(4);
                job.setCellStyle(cellStyle(false, true, false));
                job = rowTable.createCell(5);
                job.setCellStyle(cellStyle(false, true, true));
                job.setCellValue(ruffModel.getJobRuff().getPrice());

                rowTable = sheet.createRow(rowCount++);
                nodeBorder(rowTable, false);
            }
        }

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        mtrl = rowTable.createCell(1);
        mtrl.setCellValue("Материалы");
        mtrl.setCellStyle(cellStyle(true, true, false));
        mtrl = rowTable.createCell(2);
        mtrl.setCellStyle(cellStyle(false, true, true));

        int sizeM = objects.getMaterials().size();
        float quantity = 0.0F;
        for (int i = 0; i < sizeM; i++) {
            rowTable = sheet.createRow(rowCount++);
            nodeBorder(rowTable, false);
            String value = ((MaterialModel) objects.getMaterials().get(i)).getArticle();
            quantity = ((MaterialModel) objects.getMaterials().get(i)).getQuantity();
            rez = quantity * commonService.calculate(value);

            mtrl = rowTable.createCell(3);
            mtrl.setCellStyle(cellStyle(true, true, false));
            mtrl.setCellValue(value);

            mtrl = rowTable.createCell(4);
            mtrl.setCellStyle(cellStyle(false, true, false));
            mtrl.setCellValue(quantity);

            mtrl = rowTable.createCell(5);
            mtrl.setCellStyle(cellStyle(false, true, true));
            mtrl.setCellValue(Float.parseFloat(commonService.realRub(rez)));
        }

        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        rowTable = sheet.createRow(rowCount++);
        nodeBorder(rowTable, false);
        job = rowTable.createCell(1);
        job.setCellStyle(cellStyle(true, true, false));
        job.setCellValue("Работа");
        job = rowTable.createCell(2);
        job.setCellStyle(cellStyle(false, true, true));

        rowTable = sheet.createRow(rowCount++);
        job = rowTable.createCell(3);
        job.setCellStyle(cellStyle(true, true, false));
        job.setCellValue(objects.getJobProduct().getArticle());
        job = rowTable.createCell(4);
        job.setCellStyle(cellStyle(false, true, false));
        job = rowTable.createCell(5);
        job.setCellStyle(cellStyle(false, true, true));
        job.setCellValue(objects.getJobProduct().getPrice());

        rowCount++;

        System.out.println(new Date().getTime() - start.getTime());
        System.out.println("Finish");
    }*/
}
