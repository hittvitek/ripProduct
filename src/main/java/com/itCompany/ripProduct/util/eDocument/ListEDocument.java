package com.itCompany.ripProduct.util.eDocument;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.common.Common;
import com.itCompany.ripProduct.enums.Composite;
import com.itCompany.ripProduct.model.data.Rentabelnost;
import com.itCompany.ripProduct.model.po.Calculation;
import com.itCompany.ripProduct.model.po.CalculationFlower;
import com.itCompany.ripProduct.model.po.CalculationMaterial;
import com.itCompany.ripProduct.model.po.CalculationRuff;
import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.model.po.Ruff;
import com.itCompany.ripProduct.repository.dao.ReportDao;
import com.itCompany.ripProduct.service.CalculationService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class ListEDocument extends AbstractEDocument implements EDocument {

    @Autowired
    ReportDao reportDao;

    @Autowired
    CalculationService calculationService;

    @Override
    public Workbook getWorkbook(String excelFilePath) throws IOException {
        Workbook workbook = null;
        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }

    public void createXLSXTableSold(Workbook workbook, Sheet sheet, final java.sql.Date with, final java.sql.Date by) {
        rate(workbook, sheet);
        date(sheet, with, by);

        Row row = sheet.createRow(2);
        headerTableSold(sheet, row, workbook);

        int rowIndex = 3, rowNumber = 1, HEADER_TABLE_SOLD_REPORT = HEADER_TABLE_SOLD.length + 1;
        List<Rentabelnost> list = reportDao.rentabelnostNotRemoved(with, by);
        for (Rentabelnost objects : list) {

            Row rowTable = sheet.createRow(rowIndex);
            for (int i = 0; i <= HEADER_TABLE_SOLD_REPORT; i++) {
                rowTable.createCell(i);
            }
            rowTable.getCell(0).setCellValue(rowNumber++);
            rowTable.getCell(1).setCellValue(objects.getCalculationArticle());
            sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
            rowTable.getCell(2).setCellValue(objects.getCalculationPrice());
            rowTable.getCell(4).setCellValue(objects.getProductPrice());
            sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 5, 6));
            rowTable.getCell(5).setCellValue(objects.getRentabelnost());
            rowTable.getCell(7).setCellValue(objects.getQuantity());

            for (int j = 0; j <= HEADER_TABLE_SOLD_REPORT; j++) {
                if (j < 2) {
                    styleBorder(workbook).setAlignment(HorizontalAlignment.CENTER);
                } else if (rowTable.getCell(j) == null) {
                    rowTable.createCell(j);
                }

                rowTable.getCell(j).setCellStyle(styleBorder(workbook));
            }
            rowIndex++;
        }
    }

    @Transactional
    @Override
    public void createXLSXTableList(Workbook workbook, Sheet sheet, boolean printPrice,
            Map<Composite, Map<String, Object>> maps, long... ids) {
        rate(workbook, sheet);
        int rowIndex = 1;

//        Float HO01 = 1.27F, H001 = 1.32F;
//        ((Material)(maps.get(Composite.MATERIAL)).get("НО01")).setCost(HO01);
//        ((Material)(maps.get(Composite.MATERIAL)).get("Н001")).setCost(H001);
        for (long el : ids) {
            Calculation objects = calculationService.getCalculation(el);
            if (!objects.isRemoved() ) {

            Row row = sheet.createRow(rowIndex++);
            headerTableList(printPrice, sheet, row, workbook);

            row = sheet.createRow(rowIndex++);
            createCellXLSX(sheet, row, rowIndex, 0, objects.getArticle());
            createCellXLSX(sheet, row, rowIndex, 1, objects.getName());
            if (printPrice) {
                createCellXLSX(sheet, row, rowIndex, 4, Common.NUMBER_FORMAT_SIGN_THIRD_ROUND.format(objects.getPrice()));
            }

            row = sheet.createRow(rowIndex++);
            Fone fone = ((Fone)(maps.get(Composite.FONE)).get(objects.getFone().getArticle()));
                    //objects.getFone();
            createCellXLSX(sheet, row, rowIndex, 0, fone.getArticle());
            createCellXLSX(sheet, row, rowIndex, 1, fone.getName());
            if (printPrice) {
                createCellXLSX(sheet, row, rowIndex, 4, Common.NUMBER_FORMAT_SIGN_THIRD_ROUND.format(fone.getPrice()));
            }

            List<CalculationRuff> calculationRuffs = objects.getCalculationRuffs();
            if (Objects.nonNull(calculationRuffs) && (!calculationRuffs.isEmpty())) {
                for (CalculationRuff list : calculationRuffs) {
                    row = sheet.createRow(rowIndex);
                    float quantity = list.getQuantity();
                    Float price = ((Ruff)(maps.get(Composite.RUFF)).get(list.getRuff().getArticle())).getPrice();
                    createCellXLSX(sheet, row, rowIndex, 0, list.getRuff().getArticle());
                    createCellXLSX(sheet, row, rowIndex, 1, list.getRuff().getName());
                    createCellXLSX(sheet, row, rowIndex, 2, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(quantity));
                    if (printPrice) {
                        createCellXLSX(sheet, row, rowIndex, 3, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(price));
                        createCellXLSX(sheet, row, rowIndex, 4,
                            Common.NUMBER_FORMAT_SIGN_THIRD_ROUND.format(price * quantity));
                    }
                    rowIndex++;
                }
            }

            List<CalculationFlower> calculationFlowers = objects.getCalculationFlowers();
            if (Objects.nonNull(calculationFlowers) && (!calculationFlowers.isEmpty())) {
                for (CalculationFlower list : calculationFlowers) {
                    row = sheet.createRow(rowIndex);
                    float quantity = list.getQuantity();
                    Float price = ((Flower)(maps.get(Composite.FLOWER)).get(list.getFlower().getArticle())).getPrice();
                    createCellXLSX(sheet, row, rowIndex, 0, list.getFlower().getArticle());
                    createCellXLSX(sheet, row, rowIndex, 1, list.getFlower().getName());
                    createCellXLSX(sheet, row, rowIndex, 2, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(quantity));
                    if (printPrice) {
                        createCellXLSX(sheet, row, rowIndex, 3, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(price));
                        createCellXLSX(sheet, row, rowIndex, 4,
                            Common.NUMBER_FORMAT_SIGN_THIRD_ROUND.format(price * quantity));
                    }
                    rowIndex++;
                }
            }

            List<CalculationMaterial> calculationMaterials = objects.getCalculationMaterials();
            if (Objects.nonNull(calculationMaterials) && (!calculationMaterials.isEmpty())) {
                for (CalculationMaterial list : calculationMaterials) {
                    row = sheet.createRow(rowIndex);
                    Float quantity = (list.getQuantity() != null) ? list.getQuantity() : 0;
                    Float cost = ((Material)(maps.get(Composite.MATERIAL)).get(list.getMaterial().getArticle())).getCost();
                    Float price = (cost != null) ? cost : 0;
                    createCellXLSX(sheet, row, rowIndex, 0, list.getMaterial().getArticle());
                    createCellXLSX(sheet, row, rowIndex, 1, list.getMaterial().getName());
                    createCellXLSX(sheet, row, rowIndex, 2, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(quantity));
                    if (printPrice) {
                        createCellXLSX(sheet, row, rowIndex, 3, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(price));
                        createCellXLSX(sheet, row, rowIndex, 4,
                            Common.NUMBER_FORMAT_SIGN_THIRD_ROUND.format(price * quantity));
                    }
                    rowIndex++;
                }
            }
            if (Objects.nonNull(objects.getJobProduct())) {
                row = sheet.createRow(rowIndex);
                createCellXLSX(sheet, row, rowIndex, 0, objects.getJobProduct().getArticle());
                if (printPrice) {
                    createCellXLSX(sheet, row, rowIndex, 4, Common.NUMBER_FORMAT_SIGN_SECOND_ROUND.format(objects.getJobProduct().getPrice()));
                }
                rowIndex++;
            }
            rowIndex++;
            row = sheet.createRow(rowIndex);
        }
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        if (printPrice) {
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
        }
    }

    protected void createCellXLSX(Sheet sheet, Row row, int rowIndex, int cellIndex, Object obj) {
        Cell cell = row.createCell(cellIndex);
        if (obj instanceof String) {
            cell.setCellValue((String) obj);
        } else if (obj instanceof Number) {
            cell.setCellValue((Float) obj);
        }
    }

}
