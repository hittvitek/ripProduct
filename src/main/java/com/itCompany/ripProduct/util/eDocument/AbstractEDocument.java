package com.itCompany.ripProduct.util.eDocument;

import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.common.Common;
import com.itCompany.ripProduct.model.po.Currency;
import com.itCompany.ripProduct.service.CurrencyService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public abstract class AbstractEDocument {

    @Autowired
    CurrencyService currencyService;

    protected final static String[] HEADER_TABLE_SOLD = { "№", "Ар-л", "С/с", "Цена", "Рен-ть", "Кол-во" };
    protected final static String[] HEADER_TABLE_LIST = { "Артикул", "Название", "Кол-во", "Цена за ед.", "Сумма" };

    protected void rate(Workbook workbook, Sheet sheet) {
        StringBuilder stringBuilder = new StringBuilder();
        String comma = ", ", space = " ";
        List<Currency> currencyList = currencyService.getCurrencys();
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));
        sheet.createRow(0).createCell(0).setCellValue(
             stringBuilder.append(currencyList.get(0).getArticle()).append(space).append(( currencyList.get(0)).getPrice())
                 .append(comma).append((currencyList.get(4)).getArticle()).append(space).append((currencyList.get(4)).getPrice())
                 .append(comma).append((currencyList.get(2)).getArticle()).append(space).append((currencyList.get(2)).getPrice())
                 .append(comma).append((currencyList.get(1)).getArticle()).append(space).append((currencyList.get(1)).getPrice())
                 .append(comma).append((currencyList.get(3)).getArticle()).append(space).append((currencyList.get(3)).getPrice())
             .toString());
        sheet.getRow(0).getCell(0).setCellStyle(styleBold(workbook));
    }
    
    protected void date(Sheet sheet, final java.sql.Date with, final java.sql.Date by) {
        StringBuilder stringBuilder = new StringBuilder();
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 4));
        sheet.createRow(1).createCell(2).setCellValue(
            stringBuilder.append("С ")
            .append(Common.DD_MM_YYYY.format(with))
            .append(" По ")
            .append(Common.DD_MM_YYYY.format(by))
            .toString());
    }

    protected void headerTableSold(Sheet sheet, Row row, Workbook workbook) {
        for(int i = 0; i <= 7; i++) {
            row.createCell(i);
            row.getCell(i).setCellStyle(styleBorder(workbook));
        }
        row.getCell(0).setCellValue(HEADER_TABLE_SOLD[0]);
        row.getCell(1).setCellValue(HEADER_TABLE_SOLD[1]);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));
        row.getCell(2).setCellValue(HEADER_TABLE_SOLD[2]);
        row.getCell(4).setCellValue(HEADER_TABLE_SOLD[3]);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 5, 6));
        row.getCell(5).setCellValue(HEADER_TABLE_SOLD[4]);
        row.getCell(7).setCellValue(HEADER_TABLE_SOLD[5]);
    }

    protected void headerTableList(boolean printPrice, Sheet sheet, Row row, Workbook workbook) {
        byte columnCount = 3;
        if (printPrice) {
            columnCount = 5;
        }
        for(byte i = 0; i < columnCount; i++) {
            row.createCell(i);
            row.getCell(i).setCellStyle(styleBorder(workbook));
        }
        row.getCell(0).setCellValue(HEADER_TABLE_LIST[0]);
        row.getCell(1).setCellValue(HEADER_TABLE_LIST[1]);
        row.getCell(2).setCellValue(HEADER_TABLE_LIST[2]);
        if (printPrice) {
            row.getCell(3).setCellValue(HEADER_TABLE_LIST[3]);
            row.getCell(4).setCellValue(HEADER_TABLE_LIST[4]);
        }
    }

    protected CellStyle styleBold(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setUnderline((byte) 1);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        return style;
    }

    protected CellStyle styleBorder(Workbook workbook) {
        CellStyle styleBorder = workbook.createCellStyle();
        styleBorder.setBorderBottom(BorderStyle.DASH_DOT);
        styleBorder.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleBorder.setBorderLeft(BorderStyle.THIN);
        styleBorder.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        styleBorder.setBorderRight(BorderStyle.THIN);
        styleBorder.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleBorder.setBorderTop(BorderStyle.THIN);
        styleBorder.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return styleBorder;
    }

    protected CellStyle cellStyle(Workbook workbook, boolean left, boolean topBottom, boolean right) {
        CellStyle styleBorder = workbook.createCellStyle();
        if (left) {
            styleBorder.setBorderLeft(BorderStyle.THIN);
            styleBorder.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        }
        if (topBottom) {
            styleBorder.setBorderTop(BorderStyle.DASH_DOT);
            styleBorder.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleBorder.setBorderBottom(BorderStyle.THIN);
            styleBorder.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        }
        if (right) {
            styleBorder.setBorderRight(BorderStyle.THIN);
            styleBorder.setRightBorderColor(IndexedColors.BLACK.getIndex());
        }
        return styleBorder;
    }

    protected void nodeBorder(Workbook workbook, Row rowTable, boolean border) {
        Cell node = rowTable.createCell(1);
        node.setCellStyle(cellStyle(workbook, true, false, false));
        if (border) {
            node = rowTable.createCell(2);
            node.setCellStyle(cellStyle(workbook, true, false, false));
        }
    }

}
