package com.itCompany.ripProduct.util.eDocument;

import java.io.IOException;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.itCompany.ripProduct.enums.Composite;

public interface EDocument {

    Workbook getWorkbook(String excelFilePath) throws IOException;

    void createXLSXTableSold(Workbook workbook, Sheet sheet, final java.sql.Date with, final java.sql.Date by);

    void createXLSXTableList(Workbook workbook, Sheet sheet, boolean printPrice,
        Map<Composite, Map<String, Object>> maps, long... id);

}
