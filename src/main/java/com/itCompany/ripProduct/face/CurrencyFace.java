package com.itCompany.ripProduct.face;

import java.util.List;

import com.itCompany.ripProduct.model.dto.CurrencyDto;

public interface CurrencyFace {

    void save(final CurrencyDto modellDto);

    CurrencyDto getCurrencyDtoById(final Long id);

    CurrencyDto getCurrencyDtoByArticle(final String article);

    List<CurrencyDto> getAllCurrencyDto();

    void delete(final Long id);

}
