package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.model.dto.RuffDto;

public interface RuffFace {

    RuffDto getRuffDto(final Long id);

    RuffDto getRuffDtoByArticle(final String article);

    List<RuffDto> getAllRuffDtoList();

    Map<Long, String> getAllRuffDtoMap();

    void save(final RuffDto ruffDto);

}
