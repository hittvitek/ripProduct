package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.model.dto.FlowerDto;

public interface FlowerFace {

    FlowerDto getFlowerDto(final Long id);

    FlowerDto getFlowerDtoByArticle(final String article);

    FlowerDto getJsonFlowerDto(final String article);

    List<FlowerDto> getAllFlowerDtoList();

    Map<Long, String> getAllFlowerDtoMap();

    void save(final FlowerDto flowerDto);

}
