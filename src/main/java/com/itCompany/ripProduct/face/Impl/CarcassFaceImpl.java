package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.face.CarcassFace;
import com.itCompany.ripProduct.model.converter.CarcassConverter;
import com.itCompany.ripProduct.model.dto.CarcassDto;
import com.itCompany.ripProduct.model.po.Carcass;
import com.itCompany.ripProduct.service.CarcassMaterialService;
import com.itCompany.ripProduct.service.CarcassService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class CarcassFaceImpl implements CarcassFace {

    CarcassService carcassService;
    CarcassMaterialService carcassMaterialService;
    CarcassConverter carcassConverter;

    @Transactional
    @Override
    public void save(CarcassDto carcassDto) {
        Carcass persist = (Carcass) carcassConverter.convertToPersist(carcassDto);
        if (carcassDto.getId() != null) {
            persist.setId(carcassDto.getId());
        }
        carcassService.saveCarcass(persist);
        carcassMaterialService.saveCarcassMaterial(persist.getCarcassMaterials());
    }

    @Override
    public CarcassDto getCarcassDto(Long id) {
        Carcass persist = carcassService.getCarcass(id);
        return (CarcassDto) carcassConverter.convertToDto(persist);
    }

    @Override
    public CarcassDto getCarcassDtoByArticle(final String article) {
        Carcass persist = carcassService.getCarcassByArticle(article);
        return (CarcassDto) carcassConverter.convertToDto(persist);
    }

    @Override
    public CarcassDto getJsonCarcassDto(final String article) {
        return getCarcassDtoByArticle(article);
    }

    @Override
    public List<CarcassDto> getAllCarcassDtoList() {
        List<Object> carcass = carcassService.getCarcass().stream().filter(f -> !f.isRemoved()).map(carcassConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<CarcassDto>)(Object)carcass;
    }

    @Override
    public Map<Long, String> getAllCarcassDtoMap() {
        Map<Long, String> carcass = carcassService.getCarcass().stream().filter(f -> !f.isRemoved())
            .collect(Collectors.toMap(Carcass::getId, Carcass::getArticle));
        return (Map<Long, String>) (Object) carcass;
    }

}
