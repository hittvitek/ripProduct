package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.face.FoneFace;
import com.itCompany.ripProduct.model.converter.FoneConverter;
import com.itCompany.ripProduct.model.dto.FoneDto;
import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.service.FoneMaterialService;
import com.itCompany.ripProduct.service.FoneRuffService;
import com.itCompany.ripProduct.service.FoneService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class FoneFaceImpl implements FoneFace {

    FoneService foneService;
    FoneMaterialService foneMaterialService;
    FoneRuffService foneRuffService;
    FoneConverter foneConverter;

    @Transactional
    @Override
    public void save(FoneDto foneDto) {
        Fone persist = (Fone) foneConverter.convertToPersist(foneDto);
        foneService.saveFone(persist);
        if (persist.getFoneMaterials() != null && !persist.getFoneMaterials().isEmpty()) {
            foneMaterialService.saveFoneMaterial(persist.getFoneMaterials());
        }
        if (persist.getFoneRuffs() != null && !persist.getFoneRuffs().isEmpty()) {
            foneRuffService.saveFoneRuff(persist.getFoneRuffs());
        }
    }

    @Override
    public FoneDto getFoneDto(Long id) {
        Fone persist = foneService.getFone(id);
        return (FoneDto) foneConverter.convertToDto(persist);
    }

    @Override
    public FoneDto getFoneDtoByArticle(String article) {
        Fone persist = foneService.getFoneByArticle(article);
        return (FoneDto) foneConverter.convertToDto(persist);
    }

    @Override
    public List<FoneDto> getAllFoneDtoList() {
        List<Object> list = foneService.getFones()
            .stream().filter(f -> !f.isRemoved()).map(foneConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<FoneDto>)(Object)list;
    }

    @Override
    public Map<Long, String> getAllFoneDtoMap() {
        Map<Long, String> map = foneService.getFones().stream().filter(f -> !f.isRemoved())
            .collect(Collectors.toMap(Fone::getId, Fone::getArticle));
        return (Map<Long, String>) (Object) map;
    }

}
