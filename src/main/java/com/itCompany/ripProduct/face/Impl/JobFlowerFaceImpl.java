package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.JobFlowerFace;
import com.itCompany.ripProduct.model.converter.JobFlowerConverter;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobFlower;
import com.itCompany.ripProduct.service.JobFlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class JobFlowerFaceImpl implements JobFlowerFace {

    JobFlowerService jobFlowerService;

    @Override
    public void save(final JobDto modelDto) {
        JobFlower persist = (JobFlower) JobFlowerConverter.getInstanse().convertToPersist(modelDto);
        jobFlowerService.saveJobFlower(persist);
    }

    @Override
    public JobDto getJobDtoById(final Long id) {
        JobFlower persist = jobFlowerService.getJobFlower(id);
        return (JobDto) JobFlowerConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJobDtoByArticle(final String article) {
        JobFlower persist = jobFlowerService.getJobFlowerByArticle(article);
        return (JobDto) JobFlowerConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJsonJobFlower(final String article) {
        return getJobDtoByArticle(article);
    }

    @Override
    public List<JobDto> getAllJobFlowerDto() {
        List<Object> jobFlowers = jobFlowerService.getJobFlowers()
                .stream().map(JobFlowerConverter.getInstanse()::convertToDto)
                .collect(Collectors.toList());
        return (List<JobDto>)(Object)jobFlowers;
    }

}
