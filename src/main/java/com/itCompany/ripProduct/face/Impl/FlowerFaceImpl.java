package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.FlowerFace;
import com.itCompany.ripProduct.model.converter.FlowerConverter;
import com.itCompany.ripProduct.model.dto.FlowerDto;
import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.service.FlowerMaterialService;
import com.itCompany.ripProduct.service.FlowerService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class FlowerFaceImpl implements FlowerFace {

    FlowerService flowerService;
    FlowerMaterialService flowerMaterialService;
    FlowerConverter flowerConverter;

    @org.springframework.transaction.annotation.Transactional
    @Override
    public void save(FlowerDto flowerDto) {
        Flower persist = (Flower) flowerConverter.convertToPersist(flowerDto);
        if (flowerDto.getId() != null) {
            persist.setId(flowerDto.getId());
        }
        flowerService.saveFlower(persist);
        flowerMaterialService.saveFlowerMaterial(persist.getFlowerMaterials());
    }

    @Override
    public FlowerDto getFlowerDto(Long id) {
        Flower persist = flowerService.getFlower(id);
        return (FlowerDto) flowerConverter.convertToDto(persist);
    }

    @Override
    public FlowerDto getFlowerDtoByArticle(final String article) {
        Flower persist = flowerService.getFlowerByArticle(article);
        return (FlowerDto) flowerConverter.convertToDto(persist);
    }

    @Override
    public FlowerDto getJsonFlowerDto(final String article) {
        return getFlowerDtoByArticle(article);
    }

    @Override
    public List<FlowerDto> getAllFlowerDtoList() {
        List<Object> flowers = flowerService.getFlowers().stream().map(flowerConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<FlowerDto>) (Object) flowers;
    }

    @Override
    public Map<Long, String> getAllFlowerDtoMap() {
        Map<Long, String> flowers = flowerService.getFlowers().stream().filter(f -> !f.isRemoved())
            .collect(Collectors.toMap(Flower::getId, Flower::getArticle));
        return (Map<Long, String>) (Object) flowers;
    }

}
