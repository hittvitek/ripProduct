package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.face.RuffFace;
import com.itCompany.ripProduct.model.converter.RuffConverter;
import com.itCompany.ripProduct.model.dto.RuffDto;
import com.itCompany.ripProduct.model.po.Ruff;
import com.itCompany.ripProduct.service.RuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class RuffFaceImpl implements RuffFace {

    RuffService ruffService;
    RuffConverter ruffConverter;

    @Transactional
    @Override
    public void save(RuffDto ruffDto) {
        Ruff persist = (Ruff) ruffConverter.convertToPersist(ruffDto);
        if (ruffDto.getId() != null) {
            persist.setId(ruffDto.getId());
        }
        ruffService.saveRuff(persist);
    }

    @Override
    public RuffDto getRuffDto(Long id) {
        Ruff persist = ruffService.getRuff(id);
        return (RuffDto) ruffConverter.convertToDto(persist);
    }

    @Override
    public RuffDto getRuffDtoByArticle(final String article) {
        Ruff persist = ruffService.getRuffByArticle(article);
        return (RuffDto) ruffConverter.convertToDto(persist);
    }

    @Override
    public List<RuffDto> getAllRuffDtoList() {
        List<Object> list = ruffService.getRuffs()
            .stream().map(ruffConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<RuffDto>)(Object)list;
    }

    @Override
    public Map<Long, String> getAllRuffDtoMap() {
        Map<Long, String> map = ruffService.getRuffs().stream().filter(f -> !f.isRemoved())
            .collect(Collectors.toMap(Ruff::getId, Ruff::getArticle));
        return (Map<Long, String>) (Object) map;
    }

}
