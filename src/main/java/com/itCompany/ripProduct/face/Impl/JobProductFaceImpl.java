package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.JobProductFace;
import com.itCompany.ripProduct.model.converter.JobProductConverter;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobProduct;
import com.itCompany.ripProduct.service.JobProductService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class JobProductFaceImpl implements JobProductFace {

    JobProductService jobProductService;
    
    JobProductConverter jobProductConverter;

    @Override
    public void save(final JobDto modelDto) {
        JobProduct persist = (JobProduct) jobProductConverter.convertToPersist(modelDto);
        jobProductService.saveJobProduct(persist);
    }

    @Override
    public JobDto getJobDtoById(final Long id) {
        JobProduct persist = jobProductService.getJobProduct(id);
        return (JobDto) jobProductConverter.convertToDto(persist);
    }

    @Override
    public JobDto getJobDtoByArticle(final String article) {
        JobProduct persist = jobProductService.getJobProductByArticle(article);
        return (JobDto) jobProductConverter.convertToDto(persist);
    }

    @Override
    public JobDto getJsonJobProductDto(final String article) {
        return getJobDtoByArticle(article);
    }

    @Override
    public List<JobDto> getAllJobProductDto() {
        List<Object> jobProducts = jobProductService.getJobProducts()
            .stream().map(jobProductConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<JobDto>)(Object)jobProducts;
    }

}
