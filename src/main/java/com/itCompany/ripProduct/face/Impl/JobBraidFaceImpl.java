package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.JobBraidFace;
import com.itCompany.ripProduct.model.converter.JobBraidConverter;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobBraid;
import com.itCompany.ripProduct.service.JobBraidService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class JobBraidFaceImpl implements JobBraidFace {

    JobBraidService jobBraidService;

    @Override
    public void save(final JobDto modelDto) {
        JobBraid persist = (JobBraid) JobBraidConverter.getInstanse().convertToPersist(modelDto);
        jobBraidService.saveJobBraid(persist);
    }

    @Override
    public JobDto getJobDtoById(final Long id) {
        JobBraid persist = jobBraidService.getJobBraid(id);
        return (JobDto) JobBraidConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJobDtoByArticle(final String article) {
        JobBraid persist = jobBraidService.getJobBraidByArticle(article);
        return (JobDto) JobBraidConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJsonJobBraidDto(final String article) {
        return getJobDtoByArticle(article);
    }

    @Override
    public List<JobDto> getAllJobBraidDto() {
        List<Object> jobBraids = jobBraidService.getJobBraids()
            .stream().map(JobBraidConverter.getInstanse()::convertToDto)
            .collect(Collectors.toList());
        return (List<JobDto>)(Object)jobBraids;
    }

}
