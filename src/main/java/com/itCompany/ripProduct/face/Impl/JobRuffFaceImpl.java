package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.JobRuffFace;
import com.itCompany.ripProduct.model.converter.JobRuffConverter;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobRuff;
import com.itCompany.ripProduct.service.JobRuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class JobRuffFaceImpl implements JobRuffFace {

    JobRuffService jobRuffService;

    @Override
    public void save(final JobDto modelDto) {
        JobRuff persist = (JobRuff) JobRuffConverter.getInstanse().convertToPersist(modelDto);
        jobRuffService.saveJobRuff(persist);
    }

    @Override
    public JobDto getJobDtoById(final Long id) {
        JobRuff persist = jobRuffService.getJobRuff(id);
        return (JobDto) JobRuffConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJobDtoByArticle(final String article) {
        JobRuff persist = jobRuffService.getJobRuffByArticle(article);
        return (JobDto) JobRuffConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJsonJobRuff(final String article) {
        return getJobDtoByArticle(article);
    }

    @Override
    public List<JobDto> getAllJobRuffDto() {
        List<Object> jobRuffs = jobRuffService.getJobRuffs().stream().map(JobRuffConverter.getInstanse()::convertToDto).collect(Collectors.toList());
        return (List<JobDto>)(Object)jobRuffs;
    }

}
