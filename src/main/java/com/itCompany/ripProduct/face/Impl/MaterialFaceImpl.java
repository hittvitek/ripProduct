package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.face.MaterialFace;
import com.itCompany.ripProduct.model.converter.MaterialConverter;
import com.itCompany.ripProduct.model.dto.MaterialDto;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.service.MaterialService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class MaterialFaceImpl implements MaterialFace {

    MaterialService materialService;
    MaterialConverter materialConverter;

    @Transactional
    @Override
    public void save(final MaterialDto materialDto) {
        Material persist = (Material) materialConverter.convertToPersist(materialDto);
        if (materialDto.getId() != null) {
            persist.setId(materialDto.getId());
        }
        materialService.saveMaterial(persist);
    }

    @Override
    public MaterialDto getMaterialDtoById(Long id) {
        Material persist = materialService.getMaterial(id);
        return (MaterialDto) materialConverter.convertToDto(persist);
    }

    @Override
    public void deleteMaterial(Long id) {
        materialService.deleteMaterial(id);
    }

    @Override
    public List<MaterialDto> getAllMaterialDtoList() {
        List<Object> materials = materialService.getMaterials().stream().filter(f -> !f.isRemoved())
            .map(materialConverter::convertToDto).collect(Collectors.toList());
        return (List<MaterialDto>) (Object) materials;
    }

    @Override
    public Map<Long, String> getAllMaterialDtoMap() {
        Map<Long, String> materials = materialService.getMaterials().stream().filter(f -> !f.isRemoved())
            .collect(Collectors.toMap(Material::getId, Material::getArticle));
        return (Map<Long, String>) (Object) materials;
    }

    @Override
    public MaterialDto getMaterialDtoByArticle(final String article) {
        MaterialDto dto = new MaterialDto();
        Material material = materialService.getMaterialByArticle(article);
        dto.setId(material.getId());
        dto.setName(material.getName());
        dto.setCost(material.getCost());
        return dto;
    }

    @Override
    public void recalculate() {
        materialService.recalculate();
    }

}
