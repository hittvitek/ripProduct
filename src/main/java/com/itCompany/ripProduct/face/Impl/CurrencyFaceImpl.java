package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.CurrencyFace;
import com.itCompany.ripProduct.model.converter.CurrencyConverter;
import com.itCompany.ripProduct.model.dto.CurrencyDto;
import com.itCompany.ripProduct.model.po.Currency;
import com.itCompany.ripProduct.service.CurrencyService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class CurrencyFaceImpl implements CurrencyFace {

    CurrencyService currencyService;

    @Override
    public void save(final CurrencyDto modelDto) {
        Currency persist = (Currency) CurrencyConverter.getInstanse().convertToPersist(modelDto);
        currencyService.saveCurrency(persist);
    }

    @Override
    public CurrencyDto getCurrencyDtoById(final Long id) {
        Currency persist = currencyService.getCurrency(id);
        return (CurrencyDto) CurrencyConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public CurrencyDto getCurrencyDtoByArticle(final String article) {
        Currency persist = currencyService.getCurrencyByArticle(article);
        return (CurrencyDto) CurrencyConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public List<CurrencyDto> getAllCurrencyDto() {
        List<Object> currencys = currencyService.getCurrencys()
            .stream().map(CurrencyConverter.getInstanse()::convertToDto)
            .collect(Collectors.toList());
        return (List<CurrencyDto>)(Object)currencys;
    }

    @Override
    public void delete(Long id) {
        currencyService.deleteCurrency(id);
    }

}
