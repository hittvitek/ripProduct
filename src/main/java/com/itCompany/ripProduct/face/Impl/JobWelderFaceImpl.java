package com.itCompany.ripProduct.face.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.itCompany.ripProduct.face.JobWelderFace;
import com.itCompany.ripProduct.model.converter.JobWelderConverter;
import com.itCompany.ripProduct.model.dto.JobDto;
import com.itCompany.ripProduct.model.po.JobWelder;
import com.itCompany.ripProduct.service.JobWelderService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class JobWelderFaceImpl implements JobWelderFace {

    JobWelderService jobWelderService;

    @Override
    public void save(final JobDto modelDto) {
        JobWelder persist = (JobWelder) JobWelderConverter.getInstanse().convertToPersist(modelDto);
        if (modelDto.getId() != null) {
            persist.setId(modelDto.getId());
        }
        jobWelderService.saveJobWelder(persist);
    }

    @Override
    public JobDto getJobDtoById(final Long id) {
        JobWelder persist = jobWelderService.getJobWelder(id);
        return (JobDto) JobWelderConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJobDtoByArticle(final String article) {
        JobWelder persist = jobWelderService.getJobWelderByArticle(article);
        return (JobDto) JobWelderConverter.getInstanse().convertToDto(persist);
    }

    @Override
    public JobDto getJsonJobWelder(final String article) {
        return getJobDtoByArticle(article);
    }

    @Override
    public List<JobDto> getAllJobWelderDto() {
        List<Object> jobWelders = jobWelderService.getJobWelders()
                .stream().map(JobWelderConverter.getInstanse()::convertToDto)
                .collect(Collectors.toList());
        return (List<JobDto>)(Object)jobWelders;
    }

}
