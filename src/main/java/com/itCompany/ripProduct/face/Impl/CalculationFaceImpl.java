package com.itCompany.ripProduct.face.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itCompany.ripProduct.enums.Composite;
import com.itCompany.ripProduct.face.CalculationFace;
import com.itCompany.ripProduct.model.converter.CalculationConverter;
import com.itCompany.ripProduct.model.dto.CalculationDto;
import com.itCompany.ripProduct.model.po.Calculation;
import com.itCompany.ripProduct.model.po.Flower;
import com.itCompany.ripProduct.model.po.Fone;
import com.itCompany.ripProduct.model.po.Material;
import com.itCompany.ripProduct.model.po.Ruff;
import com.itCompany.ripProduct.service.CalculationFlowerService;
import com.itCompany.ripProduct.service.CalculationMaterialService;
import com.itCompany.ripProduct.service.CalculationRuffService;
import com.itCompany.ripProduct.service.CalculationService;
import com.itCompany.ripProduct.service.FlowerService;
import com.itCompany.ripProduct.service.FoneService;
import com.itCompany.ripProduct.service.MaterialService;
import com.itCompany.ripProduct.service.RuffService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Component
public class CalculationFaceImpl implements CalculationFace {

    MaterialService materialService;
    RuffService ruffService;
    FlowerService flowerService;
    FoneService foneService;
    CalculationService calculationService;
    CalculationFlowerService calculationFlowerService;
    CalculationRuffService calculationRuffService;
    CalculationMaterialService calculationMaterialService;

    CalculationConverter calculationConverter;

    @Transactional
    @Override
    public void save(CalculationDto calculationDto) {
        Calculation persist = (Calculation) calculationConverter.convertToPersist(calculationDto);
        if (calculationDto.getId() != null) {
            persist.setId(calculationDto.getId());
        }
        if (Objects.nonNull(persist)) {
            calculationService.saveCalculation(persist);
        }
        if (Objects.nonNull(persist.getCalculationMaterials())) {
            calculationMaterialService.saveCalculationMaterial(persist.getCalculationMaterials());
        }
        if (Objects.nonNull(persist.getCalculationFlowers())) {
            calculationFlowerService.saveCalculationFlower(persist.getCalculationFlowers());
        }
        if (Objects.nonNull(persist.getCalculationRuffs())) {
            calculationRuffService.saveCalculationRuff(persist.getCalculationRuffs());
        }
    }

    @Override
    public CalculationDto getCalculationDto(Long id) {
        Calculation persist = calculationService.getCalculation(id);
        return (CalculationDto) calculationConverter.convertToDto(persist);
    }

    @Override
    public List<CalculationDto> getAllCalculationDtoListView() {
        List<Object> list = calculationService.getCalculations().stream().map(calculationConverter::convertToDto)
            .collect(Collectors.toList());
        return (List<CalculationDto>) (Object) list;
    }

    @Override
    public Map<Composite, Map<String, Object>> getAllCalculationView() {
        Map<String, Object> flowers = flowerService.getFlowers().stream().filter(c -> !c.isRemoved())
            .collect(Collectors.toMap(Flower::getArticle, model -> model));
        Map<String, Object> ruffs = ruffService.getRuffs().stream().filter(c -> !c.isRemoved())
            .collect(Collectors.toMap(Ruff::getArticle, model -> model));
        Map<String, Object> fones = foneService.getFones().stream().filter(c -> !c.isRemoved())
            .collect(Collectors.toMap(Fone::getArticle, model -> model));
        Map<String, Object> materials = materialService.getMaterials().stream().filter(c -> !c.isRemoved())
            .collect(Collectors.toMap(Material::getArticle, model -> model));
        Map<String, Object> materials1 = materialService.getMaterials().stream().filter(c -> !c.isRemoved() && c.getCost() == null)
            .collect(Collectors.toMap(Material::getArticle, model -> model));
        materials1.forEach((k, v) -> System.out.println("key materials1 : " + k));
        Map<Composite, Map<String, Object>> composite = new HashMap<Composite, Map<String, Object>>();
        composite.put(Composite.MATERIAL, materials);
        composite.put(Composite.FLOWER, flowers);
        composite.put(Composite.FONE, fones);
        composite.put(Composite.RUFF, ruffs);
        return composite;
    }

}
