package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.model.dto.CarcassDto;

public interface CarcassFace {

    CarcassDto getCarcassDto(final Long id);

    CarcassDto getCarcassDtoByArticle(final String article);

    CarcassDto getJsonCarcassDto(final String article);

    List<CarcassDto> getAllCarcassDtoList();

    Map<Long, String> getAllCarcassDtoMap();

    void save(final CarcassDto carcassDto);

}
