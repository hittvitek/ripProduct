package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.model.dto.FoneDto;

public interface FoneFace {

    FoneDto getFoneDto(final Long id);

    FoneDto getFoneDtoByArticle(final String article);

    List<FoneDto> getAllFoneDtoList();

    Map<Long, String> getAllFoneDtoMap();

    void save(final FoneDto foneDto);

}
