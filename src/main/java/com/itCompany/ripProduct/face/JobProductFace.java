package com.itCompany.ripProduct.face;

import java.util.List;

import com.itCompany.ripProduct.model.dto.JobDto;

public interface JobProductFace {

    void save(final JobDto modellDto);

    JobDto getJobDtoById(final Long id);

    JobDto getJobDtoByArticle(final String article);

    JobDto getJsonJobProductDto(final String article);

    List<JobDto> getAllJobProductDto();

}
