package com.itCompany.ripProduct.face;

import java.util.List;

import com.itCompany.ripProduct.model.dto.JobDto;

public interface JobBraidFace {

    void save(final JobDto modellDto);

    JobDto getJobDtoById(final Long id);

    JobDto getJobDtoByArticle(final String article);

    JobDto getJsonJobBraidDto(final String article);

    List<JobDto> getAllJobBraidDto();

}
