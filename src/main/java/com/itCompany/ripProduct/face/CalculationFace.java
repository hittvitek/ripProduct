package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.enums.Composite;
import com.itCompany.ripProduct.model.dto.CalculationDto;

public interface CalculationFace {

    CalculationDto getCalculationDto(final Long id);

    List<CalculationDto> getAllCalculationDtoListView();

    Map<Composite, Map<String, Object>> getAllCalculationView();

    void save(final CalculationDto calculationDto);

}
