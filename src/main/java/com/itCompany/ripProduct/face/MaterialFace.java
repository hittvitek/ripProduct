package com.itCompany.ripProduct.face;

import java.util.List;
import java.util.Map;

import com.itCompany.ripProduct.model.dto.MaterialDto;

public interface MaterialFace {

    void save(final MaterialDto materialDto);

    MaterialDto getMaterialDtoById(final Long id);

    void deleteMaterial(final Long id);

    List<MaterialDto> getAllMaterialDtoList();

    Map<Long, String> getAllMaterialDtoMap();

    MaterialDto getMaterialDtoByArticle(final String article);

	void recalculate();

}
