package com.itCompany.ripProduct.face;

import java.util.List;

import com.itCompany.ripProduct.model.dto.JobDto;

public interface JobRuffFace {

    void save(final JobDto modellDto);

    JobDto getJobDtoById(final Long id);

    JobDto getJobDtoByArticle(final String article);

    JobDto getJsonJobRuff(final String article);

    List<JobDto> getAllJobRuffDto();

}
