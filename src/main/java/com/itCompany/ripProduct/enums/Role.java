package com.itCompany.ripProduct.enums;

public enum Role {
    ADMIN, USER, GUEST;
}
