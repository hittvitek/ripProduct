package com.itCompany.ripProduct.enums;

public enum State {
    ACTIVE, BANNED, DELETED;
}
