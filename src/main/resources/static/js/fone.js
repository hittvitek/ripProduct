$(document).ready(function() {

    window.materials = {
        id : 0,
        name : '',
        cost : 0,
    };

    window.ruffs = {
        id : 0,
        name : '',
        price : 0,
    };

    function isValue(value, def, is_return) {
        if ( $.type(value) == 'null'
            || $.type(value) == 'undefined'
            || $.trim(value) == ''
            || ($.type(value) == 'number' && !$.isNumeric(value))
            || ($.type(value) == 'array' && value.length == 0)
            || ($.type(value) == 'object' && $.isEmptyObject(value)) ) {
            return ($.type(def) != 'undefined') ? def : false;
        } else {
            return ($.type(is_return) == 'boolean' && is_return === true ? value : true);
        }
    }

    function round(num, decimalPlaces = 0) {
        var p = Math.pow(10, decimalPlaces);
        var m = (num * p) * (1 + Number.EPSILON);
        return Math.round(m) / p;
    }

    function foneResult() {
        var resultMaterial = 0;
        var summaryMaterial = 0;
        $.each($('#materialItems tr'), function(row, column) {
            resultMaterial = $(column).children('td').get(5);
            resultMaterial = $(resultMaterial).text();
            summaryMaterial += Number.parseFloat(round(resultMaterial, 2));
        });
        var resultRuff = 0;
        var summaryRuff = 0;
        $.each($('#ruffItems tr'), function(row, column) {
            resultRuff = $(column).children('td').get(5);
            resultRuff = $(resultRuff).text();
            summaryRuff += Number.parseFloat(round(resultRuff, 2));
        });
        var jobPrice = $('#jobPrice').val();
        var carcassPrice = $('#carcassPrice').val();
        var result = Number.parseFloat(jobPrice) + Number.parseFloat(summaryRuff)
            + Number.parseFloat(summaryMaterial) + Number.parseFloat(carcassPrice);
        $('#price').val('');
        $('#price').val(Number.parseFloat(round(result, 3)));
    };

    function init() {
       //-= begin ruff =-
       $('#ruff').on('keyup', '#ruffQuantity', function() {
            //debugger;
            var currentRow = $(this).closest("tr");
            var quantityRow = currentRow.children("td").get(3);
            var priceRow = currentRow.children("td").get(4);
            var resultRow = currentRow.children("td").get(5);
            var price = $(priceRow).text();
            var quantity = $(quantityRow).children("#ruffQuantity").val();
            var result = (price * quantity);
            $(resultRow).html(round(result, 2));
            foneResult();
       });

        $('#ruff').on('change', '#selectRuff', function() {
            var chengeRuff = $(this).closest("#ruffArticle");
            var currentRow = $(this).closest("tr");
            var articleRow = chengeRuff.children("#selectRuff");
            var article = $(articleRow).children('#selectRuff :selected').text();
            $.ajax({
                type: "GET",
                url: "/fone/ruff/" + article,
                cache: false,
            }).done(function(responce) {
                var idRow = currentRow.children("td").get(0);
                var nameRow = currentRow.children("td").get(2);
                var quantityRow = currentRow.children("td").get(3);
                var costRow = currentRow.children("td").get(4);
                var resultRow = currentRow.children("td").get(5);
                var quantity = $(quantityRow).children("#ruffQuantity").val();
                var result = ((responce.cost) * quantity);
                $(idRow).children('#ruffId').val(responce.id);
                $(nameRow).html(responce.name);
                $(costRow).html(responce.price);
                $(resultRow).html(round(result, 2));
                foneResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $('#ruff').on('click', '#ruffDelete', function() {
            if (confirm("Вы подтверждаете удаление?")) {
                var currentRow = $(this).closest('tr');
                var foneId = $('#id').val();
                var idRow = currentRow.children('td').get(0);
                var ruffId = $(idRow).children('#ruffId').val();
                //isValue(value, def, is_return)
                if ($.type(foneId) != 'null') {
                  $.ajax({
                      type: "GET",
                      url: "/fone/" + foneId + "/delete/ruff/" + ruffId,
                      cache: false,
                  }).done(function(responce) {
                      foneResult();
                  }).fail(function(responce) {
                      alert("error" + responce);
                  });
                }
                $(this).closest('tr').remove();
            }
        });

//        $('#ruff #ruffAdd').click( async function() {
        $('#ruff #ruffAdd').click( function() {
            var lastRow = $('#ruffItems').find('tr').last();
            var index = 0;
            if ($(lastRow).index() != -1)
                index = $(lastRow).index() + 1;
            var ruffItems = '<tr>';
            ruffItems += '<td width="0%" colspan="1" hidden="true">';
            ruffItems += '<input type="number" class="form-control" id="ruffId" name=ruffs['+ index +'].id>';
            ruffItems += '</td>';
            ruffItems += '<td width="24%" colspan="1" id="ruffArticle">';
            ruffItems += '<select id="selectRuff" name="selectRuff" class="form-control">';
            ruffItems += '</select>';
            ruffItems += '</td>';
            ruffItems += '<td width="36%" colspan="1" id="ruffName"/>';
            ruffItems += '<td width="15%" colspan="1">';
            ruffItems += '<input type="text" class="form-control" id="ruffQuantity" placeholder="ruff.quantity" name=ruffs['+ index +'].quantity>';
            ruffItems += '<span class="text-danger"></span>';
            ruffItems += '</td>';
            ruffItems += '<td width="10%" colspan="1" id="ruffCost">';
            ruffItems += '</td>';
            ruffItems += '<td width="10%" colspan="1" id="ruffResult"></td>';
            ruffItems += '<td width="5%" colspan="1" id="ruffActionDelete">';
            ruffItems += '<a class="btn btn-outline-danger" id="ruffDelete"><i class="fas fa-user-times ml-2"></i></a>';
            ruffItems += '</td>';
            $('#ruffItems').append(ruffItems);

            $.ajax({
                type: "GET",
                url: "/ruff/fill/",
                cache: false,
            }).done(function(responce) {
                var ruff = '<option value="0"></option>';
                var obj = JSON.parse(responce);
                for (key in obj) {
                    ruff += '<option value="' + key + '">' + obj[key] + '</option>';
                }
                var lastRow = $('#ruff').find('tr').last();
                var articleList = lastRow.find('#ruffArticle #selectRuff');
                $(articleList).html('');
                $(articleList).append(ruff);
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });
        //-= end ruff =-
        //-= begin material =-
        $('#material').on('keyup', '#materialQuantity', function() {
            //debugger;
            var currentRow = $(this).closest("tr");
            var quantityRow = currentRow.children("td").get(3);
            var priceRow = currentRow.children("td").get(4);
            var resultRow = currentRow.children("td").get(5);
            var price = $(priceRow).text();
            var quantity = $(quantityRow).children("#materialQuantity").val();
            var result = (price * quantity);
            $(resultRow).html(round(result, 2));
            foneResult();
       });

        $('#material').on('change', '#selectMaterial', function() {
            var chengeMaterial = $(this).closest("#materialArticle");
            var currentRow = $(this).closest("tr");
            var articleRow = chengeMaterial.children("#selectMaterial");
            var article = $(articleRow).children('#selectMaterial :selected').text();
            $.ajax({
                type: "GET",
                url: "/fone/material/" + article,
                cache: false,
            }).done(function(responce) {
                var idRow = currentRow.children("td").get(0);
                var nameRow = currentRow.children("td").get(2);
                var quantityRow = currentRow.children("td").get(3);
                var costRow = currentRow.children("td").get(4);
                var resultRow = currentRow.children("td").get(5);
                var quantity = $(quantityRow).children("#materialQuantity").val();
                var result = ((responce.cost) * quantity);
                $(idRow).children('#materialId').val(responce.id);
                $(nameRow).html(responce.name);
                $(costRow).html(responce.cost);
                $(resultRow).html(round(result, 2));
                foneResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $('#material').on('click', '#materialDelete', function() {
            if (confirm("Вы подтверждаете удаление?")) {
                var currentRow = $(this).closest('tr');
                var foneId = $('#id').val();
                var idRow = currentRow.children('td').get(0);
                var materialId = $(idRow).children('#materialId').val();
                //isValue(value, def, is_return)
                if ($.type(foneId) != 'null') {
                  $.ajax({
                      type: "GET",
                      url: "/fone/" + foneId + "/delete/material/" + materialId,
                      cache: false,
                  }).done(function(responce) {
                      foneResult();
                  }).fail(function(responce) {
                      alert("error" + responce);
                  });
                }
                $(this).closest('tr').remove();
            }
        });

//        $('#material #materialAdd').click( async function() {
        $('#material #materialAdd').click( function() {
            var lastRow = $('#materialItems').find('tr').last();
            var index = 0;
            if ($(lastRow).index() != -1)
                index = $(lastRow).index() + 1;
            var materialItems = '<tr>';
            materialItems += '<td width="0%" colspan="1" hidden="true">';
            materialItems += '<input type="number" class="form-control" id="materialId" name=materials['+ index +'].id>';
            materialItems += '</td>';
            materialItems += '<td width="24%" colspan="1" id="materialArticle">';
            materialItems += '<select id="selectMaterial" name="selectMaterial" class="form-control">';
            materialItems += '</select>';
            materialItems += '</td>';
            materialItems += '<td width="36%" colspan="1" id="materialName"/>';
            materialItems += '<td width="15%" colspan="1">';
            materialItems += '<input type="text" class="form-control" id="materialQuantity" placeholder="material.quantity" name=materials['+ index +'].quantity>';
            materialItems += '<span class="text-danger"></span>';
            materialItems += '</td>';
            materialItems += '<td width="10%" colspan="1" id="materialCost">';
            materialItems += '</td>';
            materialItems += '<td width="10%" colspan="1" id="materialResult"></td>';
            materialItems += '<td width="5%" colspan="1" id="materialActionDelete">';
            materialItems += '<a class="btn btn-outline-danger" id="materialDelete"><i class="fas fa-user-times ml-2"></i></a>';
            materialItems += '</td>';
            $('#materialItems').append(materialItems);

            $.ajax({
                type: "GET",
                url: "/material/fill/",
                cache: false,
            }).done(function(responce) {
                var material = '<option value="0"></option>';
                var obj = JSON.parse(responce);
                for (key in obj) {
                    material += '<option value="' + key + '">' + obj[key] + '</option>';
                }
                var lastRow = $('#material').find('tr').last();
                var articleList = lastRow.find('#materialArticle #selectMaterial');
                $(articleList).html('');
                $(articleList).append(material);
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $('#carcassArticle').change(function() {
            var article = $(this).children('#carcassArticle :selected').text();
            $.ajax({
                type: "GET",
                url: "/fone/carcass/" + article,
                cache: false,
            }).done(function(responce) {
                $('#carcassId').val(responce.id);
                $('#carcassName').val(responce.name);
                $('#carcassPrice').val(responce.price);
                foneResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $('#jobArticle').change(function() {
            var article = $(this).children('#jobArticle :selected').text();
            $.ajax({
                type: "GET",
                url: "/fone/job/" + article,
                cache: false,
            }).done(function(responce) {
                $('#jobId').val(responce.id);
                $('#jobName').val(responce.name);
                $('#jobPrice').val(responce.price);
                foneResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        foneResult();
    };

    init();

});
