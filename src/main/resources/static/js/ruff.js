$(document).ready(function() {

    function isValue(value, def, is_return) {
        if ( $.type(value) == 'null'
            || $.type(value) == 'undefined'
            || $.trim(value) == ''
            || ($.type(value) == 'number' && !$.isNumeric(value))
            || ($.type(value) == 'array' && value.length == 0)
            || ($.type(value) == 'object' && $.isEmptyObject(value)) ) {
            return ($.type(def) != 'undefined') ? def : false;
        } else {
            return ($.type(is_return) == 'boolean' && is_return === true ? value : true);
        }
    }

    function round(num, decimalPlaces = 0) {
        var p = Math.pow(10, decimalPlaces);
        var m = (num * p) * (1 + Number.EPSILON);
        return Math.round(m) / p;
    }

    function ruffResult() {
        var resultMaterial = 0;
        var summaryMaterial = 0;
//		$.each( [ "a", "b", "c" ], function( i, l ){
//			  alert( "Index #" + i + ": " + l );
//			});
		//var w = $('#result')[r].cells[4];
        $.each($('#materialItems tr'), function(row, column) {
            resultMaterial = $(column).children('td').get(5);
            resultMaterial = $(resultMaterial).text();
//            resultMaterial = $('#result').val(round(resultMaterial, 2));
            summaryMaterial += Number.parseFloat(round(resultMaterial, 2));
        });
        var jobRuffPrice = $('#jobRuffPrice').val();
        var result = Number.parseFloat(jobRuffPrice) + Number.parseFloat(summaryMaterial);
        $('#price').val('');
        $('#price').val(result);
    };

    function init() {
        $('#ruffMaterial').on('keyup', '#quantity', function() {
            //debugger;
            var currentRow = $(this).closest("tr");
            var quantityRow = currentRow.children("td").get(3);
            var priceRow = currentRow.children("td").get(4);
            var resultRow = currentRow.children("td").get(5);
            var price = $(priceRow).text();
            var quantity = $(quantityRow).children("#quantity").val();
            var result = (price * quantity);
            $(resultRow).html(round(result, 2));
            ruffResult();
       });

//        $(function() {
//            $("#ruffMaterial #material").on('click', function() {
//                alert( $(this).parent('tr').index() );
//            });
//        });

        $('#ruffMaterial').on('change', '#material', function() {
            var chengeMaterial = $(this).closest("#materialArticle");
            var currentRow = $(this).closest("tr");
            var articleRow = chengeMaterial.children("#material");
            var article = $(articleRow).children('#material :selected').text();
            $.ajax({
                type: "GET",
                url: "/ruff/material/" + article,
                cache: false,
            }).done(function(responce) {
                var idRow = currentRow.children("td").get(0);
                var nameRow = currentRow.children("td").get(2);
                var quantityRow = currentRow.children("td").get(3);
                var costRow = currentRow.children("td").get(4);
                var resultRow = currentRow.children("td").get(5);
                var quantity = $(quantityRow).children("#quantity").val();
                var result = ((responce.cost) * quantity);
                $(idRow).children('#materialId').val(responce.id);
                $(nameRow).html(responce.name);
                $(costRow).html(responce.cost);
                $(resultRow).html(round(result, 2));
                ruffResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $('#jobRuffArticle').change(function() {
            var article = $(this).children('#jobRuffArticle :selected').text();
            $.ajax({
                type: "GET",
                url: "/ruff/jobRuff/" + article,
                cache: false,
            }).done(function(responce) {
                $('#jobRuffId').val(responce.id);
                $('#jobRuffName').val(responce.name);
                $('#jobRuffPrice').val(responce.price);
                ruffResult();
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        $("#ruffMaterial").on('click', '#materialDelete', function() {
            if (confirm("Вы подтверждаете удаление?")) {
                var currentRow = $(this).closest('tr');
                var ruffId = $('#id').val();
                var idRow = currentRow.children('td').get(0);
                var materialId = $(idRow).children('#materialId').val();
                if ($.type(ruffId) != 'null') {
                  $.ajax({
                      type: "GET",
                      url: "/ruff/" + ruffId + "/delete/material/" + materialId,
                      cache: false,
                  }).done(function(responce) {
                      ruffResult();
                  }).fail(function(responce) {
                      alert("error" + responce);
                  });
                }
                $(this).closest('tr').remove();
            }
        });

        $("#ruffMaterial #materialAdd").click( async function() {
            var lastRow = $('#materialItems').find('tr').last();
            var index = 0;
            if ($(lastRow).index() != -1)
                index = $(lastRow).index() + 1;
            var materialItems = '<tr>';
            materialItems += '<td width="0%" colspan="1" hidden="true">';
            materialItems += '<input type="number" class="form-control" id="materialId" name=materials['+ index +'].id>';
            materialItems += '</td>';
            materialItems += '<td width="24%" colspan="1" id="materialArticle">';
            materialItems += '<select id="material" name="material" class="form-control">';
            materialItems += '</select>';
            materialItems += '</td>';
            materialItems += '<td width="36%" colspan="1" id="name"/>';
            materialItems += '<td width="15%" colspan="1" >';
            materialItems += '<input type="text" class="form-control" id="quantity" placeholder="quantity" name=materials['+ index +'].quantity>';
            materialItems += '<span class="text-danger"></span>';
            materialItems += '</td>';
            materialItems += '<td width="10%" colspan="1" id="cost">';
            materialItems += '</td>';
            materialItems += '<td width="10%" colspan="1" id="result"></td>';
            materialItems += '<td width="5%" colspan="1" id="materialActionDelete">';
            materialItems += '<a class="btn btn-outline-danger" id="materialDelete"><i class="fas fa-user-times ml-2"></i></a>';
            materialItems += '</td>';
            $('#materialItems').append(materialItems);

            $.ajax({
                type: "GET",
                url: "/material/fill/",
                cache: false,
            }).done(function(responce) {
                var material = "";
                var obj = JSON.parse(responce);
                for (key in obj) {
                    material += '<option value="' + key + '">' + obj[key] + '</option>';
                }
                var lastRow = $('#ruffMaterial').find('tr').last();
                var articleList = lastRow.find('#materialArticle #material');
                $(articleList).html('');
                $(articleList).append(material);
//                currentLastRow.children('#materialArticle #material').html('');
//                currentLastRow.children('#materialArticle #material').append(material);
//                $('#materialArticle #material').html('');
//                $('#materialArticle #material').append(material);
            }).fail(function(responce) {
                alert("error" + responce);
            });
        });

        ruffResult();
    };

    init();

});
