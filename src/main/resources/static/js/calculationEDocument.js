$(document).ready(function() {

    function init() {
        $("#printReportSold").click(function() {
            if (confirm("Выполнить отчет по продажам за период?")) {
                $.ajax({type: "GET",
                     url: "/calculation/reportXLSX/listSold",
                     cache: false,
                     data: { dateFrom: dateFrom.value,
                             dateTo: dateTo.value },// parameters
                           }
                ).done( function(responce) {
                    alert("success" + responce); 
                        }
                ).fail(function(responce) {
                    alert("error" + responce);
                });
            }
        });

//        $('#calculations #check').change(function() {
////            var arrId[];
//            var check = $('#calculations #check');
//            if (check) {
////                var checkList;
//                $.each($('#calculationItems tr'), function(row, column) {
////                    checkList = 
//                    	$(column).children('td #checked').get(1);
//                    $(checkList).checked;
//                    
////                    resultMaterial = $(column).children('td').get(5);
////                    resultMaterial = $(resultMaterial).text();
//                    
//                });
//            }
//        });

        $("#printReportListCalculation").click( /*async*/ function() {
        	var ids = []; //выходной массив
        	var tmp;
        	var currentVal;
        	$.each($('#calculationItems tr'), function(row, column) {
        		currentVal = column.cells[1].children;
        		if ($(currentVal).prop('checked')) {
        			tmp = $(column).children('td').get(0);
        			ids.push($(tmp).text());
        		}
            });
//        	var a = $('#calculationItems tr input:checked'); //выбираем все отмеченные checkbox
//        	var out=[]; //выходной массив
        	 
//        	for (var x=0; x<a.length;x++){ //перебераем все объекты
//        	        out.push(a[x].value); //добавляем значения в выходной массив
//        	        }
//        	console.log(out); //с массивом делаем что угодно.
            if (ids.length > 0 && confirm("Выполнить отчет по выбраным позициям в виде списка?")) {
                $.ajax({type: "POST",
                     url: "/calculation/reportXLSX/listCalculation",
                     cache: false,
                     data: {'ids[]': ids}// parameters
                })
                .done( function(responce) {alert("success" + responce);})
                .fail(function(responce) {alert("error" + responce);});
            } else {alert("Для отчета не выбраны строки из таблицы!")} 
        });
    };

    (function($){
    	$.fn.checkboxTable = function() {
    	target = this;
     
    	// Клик по checkbox в шапке таблицы.
    	$(target).on('click', 'thead :checkbox', function() {
    		var check = this;
    		$(this).parents('table').find('tbody :checkbox').each(function(){
    			if ($(check).is(':checked')) {
    				$(this).prop('checked', true);
    				$(this).parents('tr').addClass('selected');
    			} else {
    				$(this).prop('checked', false);
    				$(this).parents('tr').removeClass('selected');
    			}
    		});
    	});
     
    	// Клик по checkbox в строке таблицы.
    	$(target).on('click', 'tbody :checkbox', function() {
    		var parents = $(this).parents('table');
    			if ($(this).is(':checked')) {
    				$(this).parents('tr').addClass('selected');
    				$(parents).find('thead :checkbox').prop('checked', true);
    			} else {
    				$(this).parents('tr').removeClass('selected');
    				if ($(parents).find('tbody :checkbox:checked').length == 0) {
    					$(parents).find('thead :checkbox').prop('checked', false);
    				}
    			}
    		});
     
    		// Клик по строке таблицы
    		$(target).on('click', 'tbody tr', function(event) {
    			if (event.target.tagName == 'TH' || event.target.tagName == 'TD'){
    				$(this).find(':checkbox').trigger('click');
    			}
    		});
    	};
    })(jQuery);

    init();
    $('.table').checkboxTable();

    
    
//    xhr: function () {
//        var xhr = new XMLHttpRequest();
//        if (xhr.upload) {
//    	    xhr.upload.onprogress = function(e) {
//    	      if (e.lengthComputable) {
//    	        progressBar.max = e.total;
//    	        progressBar.value = e.loaded;
//    	        display.innerText = Math.floor((e.loaded / e.total) * 100) + '%';
//    	      }
//    	    }
//    	    xhr.upload.onloadstart = function(e) {
//    	      progressBar.value = 0;
//    	      display.innerText = '0%';
//    	    }
//    	    xhr.upload.onloadend = function(e) {
//    	      progressBar.value = e.loaded;
//    	      loadBtn.disabled = false;
//    	      loadBtn.innerHTML = 'Start uploading';
//    	    }
//    	  }
//        loadBtn.addEventListener("click", function(e) {
//        	  this.disabled = true;
//        	  this.innerHTML = "Uploading...";
//        	  upload(buildFormData());
//        });
//    }, false);
//    return xhr;
    
    
});
